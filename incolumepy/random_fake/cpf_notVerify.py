from rstr import rstr


def gen_cpf():
    while True:
        yield rstr('1234567890', 11)

def gen_cpf_formated():
    cpf = gen_cpf()
    while True:
        n = next(cpf)
        yield "{}.{}.{}-{}".format(n[:3], n[3:6], n[6:9], n[-2:])

def gen_fake_cpf(formated=True):
    '''
    Cria um Gerador numeros de CPF com 11 digitos nao verificados
    :param formated: True para formatados e False para somente numeros
    :return: string com numero de CPF
    '''
    while True:
        cpf = rstr('1234567890', 11)
        if formated:
            yield "{}.{}.{}-{}".format(cpf[:3], cpf[3:6], cpf[6:9], cpf[-2:])
        else:
            yield cpf



if __name__ == '__main__':
    ncpf = gen_cpf()
    fcpf = gen_cpf_formated()
    fakecpf = gen_fake_cpf(False)
    fakecpf0 = gen_fake_cpf(True)

    print(ncpf)
    print(next(ncpf))
    for i in range(10):
        print(next(ncpf))
        print(next(fcpf))

    print([next(ncpf) for x in range(5)])
    print([next(fcpf) for x in range(5)])
    print([next(fakecpf) for x in range(5)])
    print([next(fakecpf0) for x in range(5)])
