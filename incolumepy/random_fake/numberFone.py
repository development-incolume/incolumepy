from rstr import rstr


def gen_fonefixo():
    while True:
        yield '({0}) {1}{2}-{3}'.format(
            rstr('1234567890', 2),
            rstr('123456', 1),
            rstr('1234567890', 3),
            rstr('1234567890', 4))


def gen_fonemovel():
    num = '1234567890'
    while True:
        yield '({}{}) {}{}-{}'.format(
            rstr(num[:-1], 1),
            rstr(num, 1),
            rstr(num[6:-1], 1),
            rstr(num, 4),
            rstr(num, 4))


if __name__ == '__main__':
    nf = gen_fonefixo()
    nc = gen_fonemovel()
    print(type(nf))
    print(nf)
    print(next(nf))
    print(next(nc))

    for i in range(10):
        print(next(nc))
