'''

'''

def primo1():
    limite = 1000
    numero = 2
    c = 1
    p = 0
    print ("\n\nPrimos 1: ")
    while numero < 1000:
        i = numero -1
        while i > 1:
            if numero % i == 0: break
            i -= 1
            c += 1
        else:
            print (numero, end=',')
            p += 1
        numero += 1

    print ("\n\nForam encontrados %d números primos." %p)
    print ("Foram necessárias %d comparações." %c)


def primo2():
    limite = 1000
    c = 1
    p = 0
    print ("\n\nPrimos 2: ")
    for numero in range(2, limite+1):
        for i in range(2,numero):
            if numero % i == 0: break
            c += 1
        else:
            print (numero,end=',')
            p += 1

    print ("\n\nForam encontrados %d números primos." %p)
    print ("Foram necessárias %d comparações." %c)

def primo3():
    limite = 1000

    print ("\n\nPrimos: 3")
    c = 1
    p = 1
    primos = [2,]
    for numero in range(3,limite+1):
        for i in primos:
            if numero % i == 0: break
            c += 1
        else:
            primos.append(numero)
            print (numero,end=',')
            p += 1

    print ("\n\nForam encontrados %d números primos." %p)
    print ("Foram necessárias %d comparações." %c)

def primo4():
    from math import sqrt

    limite = 1000

    print ("\n\nPrimos: 4")
    c = 1
    p = 1
    primos = [2,]
    for numero in range(3,limite+1):
        ehprimo = 1
        for i in primos:
            if numero % i == 0:
                ehprimo = 0
                break
            if i > sqrt(numero):
                break
            c += 1
        if (ehprimo):
            primos.append(numero)
            #print (numero,end=',')
            p += 1
    print(primos)
    print ("\n\nForam encontrados %d números primos." %p)
    print ("Foram necessárias %d comparações." %c)

def primo5():
    from math import sqrt

    print ("\n\nPrimos 5")

    c, p, primos, limite = 1, 1, [2,], 1000

    for numero in range(3,limite+1,2): #pula de dois em dois
        ehprimo = 1
        for i in primos:
            c += 1 # mudei de lugar
            if numero % i == 0:
                ehprimo = 0
                break
            if i > sqrt(numero):
                break
        if (ehprimo):
            primos.append(numero)
            print (numero,end=',')
            p += 1

    print ("\n\nForam encontrados %d números primos." %p)
    print ("Foram necessárias %d comparações." %c)


def primes( n ):
    if n < 2:  return []
    if n == 2: return [2]
    nums = range(3, n+1, 2)
    nums_length = (n // 2) - 1 + (n % 2)
    idx = 0
    idx_sqrtn = (int(n**0.5) - 3) // 2
    while idx <= idx_sqrtn:
        nums_at_idx = (idx << 1) + 3
        for j in range( idx * (nums_at_idx + 3) + 3, nums_length, nums_at_idx ):
            nums[j] = 0
        idx += 1
        while idx <= idx_sqrtn:
            if nums[idx] != 0:
                break
            idx += 1
    return [2] + [x for x in nums if x != 0]

def primo6(limite):
    lista_de_primos = primes(limite)
    print("Primos 7:", " ".join(str(x) for x in lista_de_primos))
    print("\n\nForam encontrados %d números primos." % len(lista_de_primos))


def isprimo0(numero):
    for i in range(2, numero+1):
        if i != numero:
            i = numero % i
            if i == 0:
                print ('Não primo')
                break
        else:
            print ('Primo')
            break
    print ('Fim')

def isprimo(numero):
    for i in range(2, numero+1):
        if i != numero:
            i = numero % i
            if i == 0:
                return False
    return True

def primo7():
    pass


def Main():
    DEBUG = 1
    if DEBUG:
        primo1()
        primo2()
        primo3()
        primo4()
        primo5()
        print(primes(5))
        print(isprimo(13))
        print(isprimo(1001050454057737))


if __name__ == '__main__':
    Main()
