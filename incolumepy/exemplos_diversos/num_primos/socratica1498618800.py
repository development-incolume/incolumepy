import math
import time
from functools import lru_cache


def is_prime_v1(n):
    '''Return True if 'n' is a number prime. False otherwise.'''
    if n == 1:
        return False
    for d in range(2, n):
        if n % d == 0:
            return False
    return True


def is_prime_v2(n):
    if n == 1:
        return False
    max_divisor = math.floor(math.sqrt(n))
    for d in range(2, 1 + max_divisor):
        if n % d == 0:
            return False
    return True


def is_prime_v3(n):
    if n == 1:
        return False
    if n == 2:
        return True
    if n > 2 and n % 2 == 0:
        return False
    max_divisor = math.floor(math.sqrt(n))
    for d in range(3, 1 + max_divisor, 2):
        if n % d == 0:
            return False
    return True


@lru_cache(maxsize=None)
def is_prime_v4(n):
    if n == 1: return False
    if n == 2: return True
    if n > 2 and n % 2 == 0: return False
    max_divisor = math.floor(math.sqrt(n))
    for d in range(3, 1 + max_divisor, 2):
        if n % d == 0:
            return False
    return True


def calc_time(method, unit_test=100000):
    t0 = time.time()
    for n in range(unit_test):
        method(n)
    t1 = time.time()
    print('Tempo de execução: ', t1 - t0)


def test_is_prime_v1():
    for i in range(1, 21):
        print('{:2}: {}'.format(i, is_prime_v1(i)))


def time_is_prime_v1():
    calc_time(is_prime_v1)


def test_is_prime_v2():
    for i in range(1, 21):
        print('{:2}: {}'.format(i, is_prime_v2(i)))


def time_is_prime_v2():
    calc_time(is_prime_v2)


def test_is_prime_v3():
    for i in range(1, 21):
        print('{:2}: {}'.format(i, is_prime_v3(i)))


def time_is_prime_v3():
    calc_time(is_prime_v3)


def test_is_prime_v4():
    for i in range(1, 21):
        print('{:2}: {}'.format(i, is_prime_v4(i)))


def time_is_prime_v4():
    calc_time(is_prime_v4)


if __name__ == '__main__':
    test_is_prime_v1()
    time_is_prime_v1()
    print('=' * 20)
    test_is_prime_v2()
    time_is_prime_v2()
    print('=' * 20)
    test_is_prime_v3()
    time_is_prime_v3()
    print('=' * 20)
    test_is_prime_v4()
    time_is_prime_v4()
