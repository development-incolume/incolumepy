from math import sqrt


class Vetor:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return 'Vetor({}, {})'.format(self.x, self.y)

    def distancia(self, v2):
        dx = self.x - v2.x
        dy = self.y - v2.y

        return sqrt(dx ** 2 + dy ** 2)

    def __abs__(self):
        return self.distancia(Vetor(0, 0))

    def __add__(self, other):
        dx = self.x + other.x
        dy = self.y + other.y
        return Vetor(dx, dy)

    def __mul__(self, other):
        '''
        multiplicação de vetor*int com o vetor em primeiro
        :param other: int
        :return: Vetor
        '''
        try:
            return Vetor(self.x * float(other), self.y * float(other))
        except:
            raise

    def __rmul__(self, other):
        '''
        multiplicação de int*vetor, propriedade comutativa da multiplicação
        :param other: int
        :return: Vetor
        '''
        try:
            return Vetor(self.x * float(other), self.y * float(other))
        except:
            raise


if __name__ == '__main__':
    v = Vetor(3, 4)
    print(v)
    print(abs(v))
    v1 = Vetor(2, 4)

    print(v + v1)
    print(v * 3)
    print(3 * v)
    print('3' * v)
