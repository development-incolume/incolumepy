#!/usr/bin/env python
# -*- coding: utf-8 -*-
# type_example.py
import argparse

my_parser = argparse.ArgumentParser()
my_parser.add_argument('-a', action='store', type=float)

args = my_parser.parse_args()

print(vars(args))
