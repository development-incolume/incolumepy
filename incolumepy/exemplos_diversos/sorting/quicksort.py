def quicksort(lista):
    if len(lista) <= 1:
        return lista

    pivo = lista[0]
    iguais = [x for x in lista if x == pivo]
    menores = [x for x in lista if x < pivo]
    maiores = [x for x in lista if x > pivo]
    return quicksort(menores) + iguais + quicksort(maiores)


if __name__ == '__main__':
    print(quicksort([9, 3, 4, 5, 1, 0, 3, 2, 7, 8, 1, 7, 0]))
