from functools import reduce


def mult(lst1):
    prod = lst1[0]
    for i in range(1, len(lst1)):
        prod *= lst1[i]
    return prod


l = [4, 3, 2, 1]

print(mult(l))
print(reduce(lambda x, y: x * y, l))
