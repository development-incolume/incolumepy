'''
equivalencia ao switch
'''


def key_1_pressed():
    print('Key 1 Pressed')


def key_2_pressed():
    print('Key 2 Pressed')


def key_3_pressed():
    print('Key 3 Pressed')


def unknown_key_pressed():
    print('Unknown Key Pressed')


def menu01(keycode):
    if keycode == 1:
        key_1_pressed()
    elif keycode == 2:
        key_2_pressed()
    elif keycode == 3:
        key_3_pressed()
    else:
        unknown_key_pressed()


def menu_pythonic(keycode):
    functions = {1: key_1_pressed, 2: key_2_pressed, 3: key_3_pressed}
    functions.get(keycode, unknown_key_pressed)()


if __name__ == '__main__':
    menu01(int(input('[1,2,3]')))

    menu_pythonic(int(input('Digite 1 a 3: ')))
