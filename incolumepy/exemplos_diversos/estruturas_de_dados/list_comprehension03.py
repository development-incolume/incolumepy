'''
map
'''
import math

lista1 = [1, 4, 9, 16, 25]
lista2 = list(map(math.sqrt, lista1))

print(lista2)

b = list(map(chr,[66,53,0,94]))
print(b)

c = list(map(chr,[x for x in range(1,256)]))
print(c)

# Pythonico
d = [*map(lambda x: x * 2, [x for x in range(1,10)])]
print(d)

# Pythonico
e = [chr(x) for x in [66, 53, 0, 94]]
print(e)

# Pythonico
f = [*map(chr,[x for x in range(1,256)])]
print(f)

# Pythonico
g = [chr(x) for x in range(1,256)]
print(g)
