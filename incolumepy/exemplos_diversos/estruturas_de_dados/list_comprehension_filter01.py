from functools import reduce

l = [x for x in range(-10, 11)]
print(l)

print([*map(lambda x: x>0, l)])

print([*filter(lambda x: x>0, l)])

print([reduce(lambda x, y: x + y, [x for x in range(6)])])
