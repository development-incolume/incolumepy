import csv
from collections import defaultdict

class Listas():
    '''
    Sort defaultdict
    '''
    csv_file = '../../../data/cidades_brasil.csv'
    def uso020(self):
        '''
        Ordenaçao pelo Estado com mais cidades
        :return:
        '''
        cities_by_state = defaultdict(list)

        # transfoma CSV em tupla
        with open(self.csv_file) as filein:
            cidades = [tuple(line) for line in csv.reader(filein)]

        # conta as cidades de cada estado
        for city, state in cidades:
            cities_by_state[state].append(city)

        # ordena os estados pela ordem alfabetica
        for state, i in sorted(cities_by_state.items()):
            print(state, i)

        # ordena os estados pela ordem alfabetica
        for state, i in sorted(cities_by_state.items()):
            print (state +': ' + ', '.join(i))
        


if __name__ == '__main__':
    Listas().uso020()
