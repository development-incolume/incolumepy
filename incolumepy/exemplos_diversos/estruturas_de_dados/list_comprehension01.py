import math
'''
map
'''
l1 = [x for x in range(1,11)]
print(l1)


b = list(map(math.sqrt, l1))
print(b)

c = list(map(lambda n: n * 2, filter(lambda n: n%2 == 1, l1)))
print(c)


l2 = [x**2 for x in range(1,11)]
print(l2)

def qq(x): return x**2
l3 = list(map(qq ,[x for x in range(1,11)]))
print(l3)

l3 = list(map(qq ,l1))
print(l3)

q = list(map(lambda x: x**2 ,[x for x in range(1,11)]))
print(q)

q = list(map(lambda x: x**2, l1))
print(q)

final = list(map((lambda x: x ** 2),l1))
print(final)
