import csv
from collections import defaultdict

class Listas():
    '''
   Sort defaultdict
   '''
    csv_file = '../../../data/cidades_brasil.csv'
    def uso021(self):
        '''
       Ordenaçao pelo Estado com mais cidades
       :return:
       '''
        cities_by_state = defaultdict(str)

        # transfoma CSV em lista de tupla
        with open(self.csv_file) as filein:
            cidades = [tuple(line) for line in csv.reader(filein)]

        # ordena as cidades por estados
        for city, estado in sorted(cidades, key = lambda x: (x[1],x[0])):
            print(city, estado)

        # ordena as cidades por estados
        for i in sorted(cidades, key = lambda x: (x[1],x[0])):
            print(i)

if __name__ == '__main__':
    Listas().uso021()
