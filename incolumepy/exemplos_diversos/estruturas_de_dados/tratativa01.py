import random
import collections


n = []
for i in range(20):
    n.append(random.randrange(6))

print(n)


print([item for item, contagem in collections.Counter(n).items() if contagem > 1])

print(set([item for item in n if n.count(item) > 1]))


m = collections.defaultdict(int)

for i in n:
    m[i] += 1

print(dict(m))
print(sorted(m.items(), key=lambda x: (x[1], x[0]), reverse=True))
