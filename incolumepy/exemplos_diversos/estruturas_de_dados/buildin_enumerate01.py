#comprehension_buildin01
# coding: utf-8


seasons = ['Spring', 'Summer', 'Fall', 'Winter']
meses_num = [x for x in range(1,13)]
meses = ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez']

#enumerate
l = list(enumerate(seasons))
print(len(l), l)

#zip
l1 = list(zip(meses, meses_num))
print(l1)

#enumerate
l2 = list(enumerate(meses,start=1))
print(l2)

#map
print(list(map(lambda x: x in meses_num, meses)))

#filter
print(list(filter(lambda x: x in meses, meses)))
