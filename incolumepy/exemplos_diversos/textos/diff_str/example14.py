# http://www.programcreek.com/python/example/1084/difflib.Differ
import difflib


class D:
    def get_diff(self):
        """
        Computes a diff and annotate each line with a line number.
        """

        diff = []
        differ = difflib.Differ()

        lineno1 = 0
        lineno2 = 0

        for line in differ.compare(self.pastes[0].get_raw_code().splitlines(),
                                   self.pastes[1].get_raw_code().splitlines()):
            line_start = line[0:2]

            if line_start == "- ":
                lineno1 += 1
                diff.append([lineno1, "", line])

            elif line_start == "+ ":
                lineno2 += 1
                diff.append(["", lineno2, line])

            elif line_start == "? ":
                pass

            else:
                lineno1 += 1
                lineno2 += 1
                diff.append([lineno1, lineno2, line])

        return diff


if __name__ == '__main__':
    D.get_diff()
