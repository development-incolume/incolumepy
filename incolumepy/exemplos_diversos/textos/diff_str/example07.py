# https://docs.python.org/2/library/difflib.html
import sys
from difflib import Differ
from pprint import pprint

text1 = '''1. Beautiful is better than ugly.
2. Explicit is better than implicit.
3. Simple is better than complex.
4. Complex is better than complicated.
'''.splitlines(1)

pprint(len(text1))
pprint(text1[0][-1])
print()

text2 = '''1. Beautiful is better than ugly.
3.   Simple is better than complex.
4. Complicated is better than complex.
5. Flat is better than nested.
'''.splitlines(1)

d = Differ()
result = list(d.compare(text1, text2))
pprint(result)

print()
print(sys.stdout.writelines(result))
