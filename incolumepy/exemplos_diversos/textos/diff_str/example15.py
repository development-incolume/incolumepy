# http://www.programcreek.com/python/example/1084/difflib.Differ
import difflib


def _set_diffs(self):
    differ = difflib.Differ()
    self.lines = []
    lineno = 0
    for line in differ.compare(self.old.splitlines(True),
                               self.new.splitlines(True)):
        if line.startswith(' '):
            lineno += 1
        elif line.startswith('-'):
            lineno += 1
            self.lines.append(lineno)


if __name__ == '__main__':
    _set_diffs()
