# https://docs.python.org/2/library/difflib.html
from difflib import ndiff, restore

diff = ndiff('one\ntwo\nthree\n'.splitlines(1),
             'ore\ntree\nemu\n'.splitlines(1))

diff = list(diff)  # materialize the generated delta into a list
print(''.join(restore(diff, 1)))
print(''.join(restore(diff, 2)))
