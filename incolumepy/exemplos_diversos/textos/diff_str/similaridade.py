#http://artedosdados.blogspot.com.br/2014/08/sequencias-de-dna-e-similaridade-entre.html
import random
import difflib


def genDNA(length):
    while True:
        yield ''.join(random.choice('CGTA') for nucleotideo in range(length))



def getDiference(DNA1, DNA2):
    s = difflib.SequenceMatcher(None, DNA1, DNA2)
    print('{} {}'.format(DNA1, DNA2))
    # Retorna a quantidade de caracteres semelhantes
    print(s.find_longest_match(0, len(DNA1), 0, len(DNA2)))
    # Retorna o indice de similaridade
    print(difflib.SequenceMatcher(None, DNA1, DNA2).ratio())


if __name__ == '__main__':
    dna = genDNA(15)
    for i in range(10):
        next(dna)
    #print(next(dna))
    getDiference(next(dna), next(dna))
