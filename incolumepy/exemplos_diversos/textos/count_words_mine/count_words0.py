import os
import re
from collections import Counter


def count_words(filename=os.path.join(os.path.dirname(__file__), '..', 'hino_a_bandeira.txt')):
    '''
    collections.Counter + re
    :param filename:
    :return:
    '''
    # print(filename)
    with open(filename) as f:
        content = f.read()
    # print(content)

    # separa as palavras e elementos de lista e
    # converte caracteres para minusculos
    words = re.findall('\w+', content.lower())

    # print(words)
    dic = Counter(words)

    # retorna dicionário com as palavras ordenado pela quantidade.
    print(dic)
    return dic


if __name__ == '__main__':
    count_words()
