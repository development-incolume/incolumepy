'''
fontes do professor Fernando Massori
'''
from string import punctuation

arq = open('../hino_a_bandeira.txt')
texto = arq.read()
texto = texto.lower()

for c in punctuation:
    texto = texto.replace(c, ' ')

texto = texto.split()

dic = {}
for p in texto:
    if p not in dic:
        dic[p] = 1
    else:
        dic[p] += 1

print('Total de palavras: {}'.format(dic))
