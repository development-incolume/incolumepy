import string


def isLetter(letra):
    '''
    Valid letters and return True or False
    :param letra: str
    :return: boolean

    >>> isLetter('q')
    True
    >>> isLetter('a')
    True
    >>> isLetter('.')
    False
    >>> isLetter(' ')
    False
    '''
    return letra in string.ascii_letters


def letra():
    print(isLetter(input("Digite uma letra: ")))


if __name__ == '__main__':
    import doctest

    doctest.testmod()
