import errno
import os


def mkdir_p(path):
    try:
        os.makedirs(path)
        return True
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def mkdir_0(path, mode=0o775):
    try:
        os.makedirs(path, mode, exist_ok=True)
        return True
    except OSError as e:
        pass
    except:
        raise


def mkdir_1(path):
    os.makedirs(path, exist_ok=True)

if __name__ == '__main__':
    mkdir_p('/tmp/0/teste/teste/teste/teste/teste')
    mkdir_0('/tmp/1/teste/teste/teste/teste')
    mkdir_0('/tmp/2/teste/teste/teste/teste')
    mkdir_0('/tmp/3/teste/teste/teste/teste')
    mkdir_1('/tmp/a//teste/teste/teste/teste/teste')
