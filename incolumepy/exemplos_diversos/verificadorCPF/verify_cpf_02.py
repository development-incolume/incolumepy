class Util(object):
    # Classe util com metodo de validacao de cpf.
    # Autor: Shalon Serpa (serpanet@gmail.com)
    def validaCpf(self, cpf, d1=0, d2=0, i=0):
        while i < 10:
            d1, d2, i = (d1 + (int(cpf[i]) * (11 - i - 1))) % 11 if i < 9 else d1, (
            d2 + (int(cpf[i]) * (11 - i))) % 11, i + 1
        return (int(cpf[9]) == (11 - d1 if d1 > 1 else 0)) and (int(cpf[10]) == (11 - d2 if d2 > 1 else 0))


# exemplo de uso
if __name__ == '__main__':
    print(Util().validaCpf("12345678901"))
    print()

    array = ["12345678901", '11345125380', '31354110274', '12345678900']
    for i in array:
        print(i, Util().validaCpf(i))

    lcpf = [str(x) * 11 for x in range(10)]

    for i in lcpf:
        print(i, Util().validaCpf(i))
