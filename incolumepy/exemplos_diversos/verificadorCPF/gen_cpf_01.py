import re


class GenCPF:
    @staticmethod
    def check(cpf):
        try:
            int(cpf)
        except ValueError:
            cpf = ''.join(re.findall('\d', cpf))
        except:
            raise
        if len(str(cpf)) != 11 or str(cpf) in [str(x) * 11 for x in range(10)]:
            return None
        return cpf

    def teste1(self):
        cpf = '123.123.123-90'
        _translate = lambda cpf: ''.join(re.findall("\d", cpf))
        print(re.findall('\d', cpf))
        print(''.join(re.findall('\d', cpf)))

if __name__ == '__main__':
    print(GenCPF.check(12345678900))
    print(GenCPF.check('12345678900'))
    print(GenCPF.check('123.456.789-00'))
    print(GenCPF.check('a.456.789-00'))
    print(GenCPF.check('111.111.111-11'))
    print(GenCPF.check('00000000000'))
    print(GenCPF.check(99999999999))
    print(GenCPF.check('111.111.111-12'))
    print(GenCPF.check('110.111.111-11'))
