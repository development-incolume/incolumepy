import locale
import sys

try:
    loc = locale.getlocale()
    lang, m = locale.getlocale()
    print(loc, lang, m)
    print(locale.LC_ALL)

    # use German locale; name might vary with platform
    #locale.setlocale(locale.LC_ALL, ('de_DE', 'UTF-8'))
    #print(locale.getlocale())

    locale.setlocale(locale.LC_ALL, ('en_US', 'UTF-8'))
    print(locale.getlocale())

    print(locale.strcoll('f\xe4n', 'foo'))  # compare a string containing an umlaut
    print(locale.getlocale())

    locale.setlocale(locale.LC_ALL, '')   # use user's preferred locale
    print(locale.getlocale())

    locale.setlocale(locale.LC_ALL, 'C')  # use default (C) locale
    print(locale.getlocale())

    locale.setlocale(locale.LC_ALL, loc)  # restore saved locale
    print(locale.getlocale())
except:
    print(sys.exc_info())