'''
https://pt.stackoverflow.com/questions/161848/como-fazer-leitura-de-pdf
https://pt.stackoverflow.com/questions/161848/como-fazer-leitura-de-pdf/161855#161855
'''
from tempfile import mktemp

import slate


def pdf_handler(pdffile):
    output_name = mktemp() + ".txt"

    with open(pdffile, 'rb') as pdf_file, open(output_name, 'wt') as output:
        doc = slate.PDF(pdf_file)
        for page in doc:
            output.write(page + '\n')


def asd():
    print(dir(slate))


if __name__ == '__main__':
    # pdf_handler('/home/brito/Documentos/cAMILA _ANA.pdf')
    asd()
