# /bin/env python
# -*- encode: utf-8 -*-

__author__ = '@britodfbr'
"""https://www.journaldev.com/33281/pypdf2-python-library-for-pdf-files#pypdf2-examples"""

import PyPDF2
from pathlib import Path
from PIL import Image
file = Path('outorga_ljdfe.pdf').absolute()
assert file.exists(), f"Ops: {file.name}"


def extracting_metadata():
    with file.open('rb') as pdf_file:
        pdf_reader = PyPDF2.PdfFileReader(pdf_file)
        pages = pdf_reader.getNumPages() if pdf_reader.getNumPages() else None
        meta = pdf_reader.documentInfo if pdf_reader.documentInfo else None
        autor = pdf_reader.documentInfo["/Author"] if pdf_reader.documentInfo["/Author"] else None
        creador = pdf_reader.documentInfo["/Creator"] if pdf_reader.documentInfo["/Creator"] else None
        print(f'Number of Pages in PDF File is {pdf_reader.getNumPages()}\n'
              f'PDF Metadata is {pdf_reader.documentInfo}\n'
              f'PDF File Author is {pdf_reader.documentInfo["/Author"]}\n'
              f'PDF File Creator is {pdf_reader.documentInfo["/Creator"]}')


def extract_page():
    with file.open('rb') as pdf_file:
        pdf_reader = PyPDF2.PdfFileReader(pdf_file)

        # printing first page contents
        pdf_page = pdf_reader.getPage(0)
        print(pdf_page.extractText())

        # reading all the pages content one by one
        for page_num in range(pdf_reader.numPages):
            pdf_page = pdf_reader.getPage(page_num)
            print(pdf_page.extractText())


def rotate_file_pages():
    with file.open('rb') as pdf_file:
        pdf_reader = PyPDF2.PdfFileReader(pdf_file)
        pdf_writer = PyPDF2.PdfFileWriter()

        for page_num in range(pdf_reader.numPages):
            pdf_page = pdf_reader.getPage(page_num)
            pdf_page.rotateClockwise(90)  # rotateCounterClockwise()
            pdf_writer.addPage(pdf_page)

        with file.with_name('{}-tutorial-rotated{}'.format(file.stem, file.suffix)).open('wb') as pdf_file_rotated:
            pdf_writer.write(pdf_file_rotated)


def merge_files():
    pdf_merger = PyPDF2.PdfFileMerger()
    pdf_files_list = [file, file.with_name('{}-tutorial-rotated{}'.format(file.stem, file.suffix))]

    for pdf_file_name in pdf_files_list:
        with open(pdf_file_name, 'rb') as pdf_file:
            pdf_merger.append(pdf_file)

    with open('Python_tutorial_merged.pdf', 'wb') as pdf_file_merged:
        pdf_merger.write(pdf_file_merged)


def merge_files_better():
    import contextlib

    pdf_files_list = [file, file.with_name('{}-tutorial-rotated{}'.format(file.stem, file.suffix))]

    with contextlib.ExitStack() as stack:
        pdf_merger = PyPDF2.PdfFileMerger()
        files = [stack.enter_context(open(pdf, 'rb')) for pdf in pdf_files_list]
        for f in files:
            pdf_merger.append(f)
        with open('Python_tutorial_merged_contextlib.pdf', 'wb') as f:
            pdf_merger.write(f)


def split_pdf_file():
    with file.open('rb') as pdf_file:
        pdf_reader = PyPDF2.PdfFileReader(pdf_file)
        for i in range(pdf_reader.numPages):
            pdf_writer = PyPDF2.PdfFileWriter()
            pdf_writer.addPage(pdf_reader.getPage(i))
            output_file_name = f'Python_tutorial_{i}.pdf'
            with open(output_file_name, 'wb') as output_file:
                pdf_writer.write(output_file)


def extract_images():
    with file.open('rb') as pdf_file:
        pdf_reader = PyPDF2.PdfFileReader(pdf_file)

        # extracting images from the 1st page
        page0 = pdf_reader.getPage(0)

        if '/XObject' in page0['/Resources']:
            xObject = page0['/Resources']['/XObject'].getObject()

            for obj in xObject:
                if xObject[obj]['/Subtype'] == '/Image':
                    size = (xObject[obj]['/Width'], xObject[obj]['/Height'])
                    data = xObject[obj].getData()
                    if xObject[obj]['/ColorSpace'] == '/DeviceRGB':
                        mode = "RGB"
                    else:
                        mode = "P"

                    if '/Filter' in xObject[obj]:
                        if xObject[obj]['/Filter'] == '/FlateDecode':
                            img = Image.frombytes(mode, size, data)
                            img.save(obj[1:] + ".png")
                        elif xObject[obj]['/Filter'] == '/DCTDecode':
                            img = open(obj[1:] + ".jpg", "wb")
                            img.write(data)
                            img.close()
                        elif xObject[obj]['/Filter'] == '/JPXDecode':
                            img = open(obj[1:] + ".jp2", "wb")
                            img.write(data)
                            img.close()
                        elif xObject[obj]['/Filter'] == '/CCITTFaxDecode':
                            img = open(obj[1:] + ".tiff", "wb")
                            img.write(data)
                            img.close()
                    else:
                        img = Image.frombytes(mode, size, data)
                        img.save(obj[1:] + ".png")
        else:
            print("No image found.")


def run():
    #  extracting_metadata()
    extract_page()
    rotate_file_pages()
    merge_files()
    merge_files_better()
    split_pdf_file()
    extract_images()


if __name__ == '__main__':
    run()