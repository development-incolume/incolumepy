def tabuada_multiplicar(num):
    for i in range(1, 11):
        print("%2.f x %2.f = %3.f" % (num, i, num * i))
    print()


def tabuada_somar(num):
    for i in range(1, 11):
        print("{:2} + {:2} = {:3}".format(num, i, num * i))
    print()


def tab_completa():
    for i in range(1, 11):
        tabuada_multiplicar(i)


def tab_compehention(num):
    print([f'{num:^2}x{x:>2}={num*x:>3}' for x in range(1, 11)])


if __name__ == '__main__':
    tabuada_multiplicar(6)
    tabuada_somar(5)
    tab_completa()
    tab_compehention(3)
