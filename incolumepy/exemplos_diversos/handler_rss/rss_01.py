import feedparser
from pprint import pprint

class H_rss:

    def __init__(self, url = 'http://www.inovacaotecnologica.com.br/boletim/rss.xml'):
        self.url = url
        self.feed = feedparser.parse(self.url)


    @staticmethod
    def Main():
        #Instancia da classe
        hrss = H_rss()

        #atributo url
        print(hrss.url)

        #atributo feed
        pprint(hrss.feed)

        # as entradas do feed de noticias/posts
        print('-' * 20, '\n')
        pprint(hrss.feed.entries)

        # O primeiro registro do feed
        print('\n'*3, '§' * 20, '\n'*3)
        print(hrss.feed.entries[0])

        # Titulo do portal rss
        print('\n' * 3, '§' * 20, '\n' * 3)
        print(hrss.feed['feed']['title'])

        #url de acesso
        print(hrss.feed['feed']['link'])

        #subtitulo do portal rss
        print(hrss.feed.feed.subtitle)

        #quantia de feeds disponível
        print(len(hrss.feed))

        #titulo do feed 0
        print(hrss.feed['entries'][0]['title'])

        #titulo e link de cada post
        print('§' * 20, '\n')
        for post in hrss.feed.entries:
            print( "%s:  %s" % (post.title, post.link ))

        #Versão do rss
        print('§' * 20, '\n')
        print( hrss.feed.version)

        #cabeçalho do rss
        print()
        pprint(hrss.feed.headers)

        #extração do campo de cabeçalho
        print()
        print(hrss.feed.headers.get('Content-Type'))


if __name__ == '__main__':
    H_rss.Main()
