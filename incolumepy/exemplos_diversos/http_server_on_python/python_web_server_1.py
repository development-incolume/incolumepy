#!/usr/bin/env python
# -*- coding: utf-8 -*-
from http.server import SimpleHTTPRequestHandler
from socketserver import ThreadingTCPServer

PORT = 8201


class MyHandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(b"<center><h1>Servidor em manutencao!</h1></center>")
        return


httpd = ThreadingTCPServer(('', PORT), MyHandler)
try:
    print("servidor web rodando na porta ", PORT)
    httpd.serve_forever()

except KeyboardInterrupt:
    print("Voce pressionou ^C, encerrando...")
    httpd.socket.close()
