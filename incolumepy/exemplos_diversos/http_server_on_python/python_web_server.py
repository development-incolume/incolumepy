#!/usr/bin/env python
# -*- coding: utf-8 -*-
from http.server import SimpleHTTPRequestHandler
from socketserver import TCPServer

PORT = 8201

Handler = SimpleHTTPRequestHandler
httpd = TCPServer(("", PORT), Handler)

print("servidor web na porta ", PORT)
httpd.serve_forever()
