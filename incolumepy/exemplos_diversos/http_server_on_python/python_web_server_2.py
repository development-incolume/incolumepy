#!/usr/bin/env python
# -*- coding: utf-8 -*-
from http.server import SimpleHTTPRequestHandler
from socketserver import ThreadingTCPServer

PORT = 8201


class MyHandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        self.path = "/tmp/index.html"
        try:
            f_index = open(self.path, 'rb')
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(f_index.read())
            f_index.close()
        except IOError:
            self.send_error(404, "Arquivo nao encontrado: %s" % self.path)
        return


httpd = ThreadingTCPServer(('', PORT), MyHandler)
try:
    print("servidor web rodando na porta ", PORT)
    httpd.serve_forever()

except KeyboardInterrupt:
    print("Voce pressionou ^C, encerrando...")
    httpd.socket.close()
