'''
Design Pattern Sigleton
implementação #2
'''


class Singleton(object):
    instance = None

    class __Singleton:
        def __init__(self, *args, **kwargs):
            self.val = None

        def __str__(self):
            return '%s - %s' % (self.__repr__(), self.val)

    def __new__(self):  # __new__ always a classmethod
        if not Singleton.instance:
            Singleton.instance = Singleton.__Singleton()
        return Singleton.instance

    def __getattr__(self, name):
        return getattr(self.instance, name)

    def __setattr__(self, name):
        return setattr(self.instance, name)


def test_0():
    a = Singleton()
    b = Singleton()
    assert (a is b) == True
    assert (id(a) == id(b)) == True


if __name__ == '__main__':
    pass
    x = Singleton()
    y = Singleton()

    print(id(x), id(y))
    print(x is y)
