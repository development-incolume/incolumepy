'''
Design Pattern Sigleton
implementação #1
'''


# fail

class OnlyOne:
    instance = None

    class __OnlyOne:
        def __init__(self, arg):
            self.val = arg

        def __str__(self):
            return repr(self) + self.val

    def __init__(self, arg):
        '''
        clona os elementos entre as instancias
        :param arg:
        '''
        if not OnlyOne.instance:
            OnlyOne.instance = OnlyOne.__OnlyOne(arg)
        else:
            OnlyOne.instance.val = arg

    def __getattr__(self, name):
        return getattr(self.instance, name)


def test_0():
    a = OnlyOne('a')
    b = OnlyOne('b')

    assert (a is a) == True
    assert (b is b) == True
    assert (id(a) == id(a)) == True
    assert (id(b) == id(b)) == True

    assert (id(a) == id(b)) == False
    assert a.val == 'b'
    assert b.val == 'b'


if __name__ == '__main__':
    pass
    x = OnlyOne('sausage')
    y = OnlyOne('eggs')
    print(id(x), id(y))
    print(x is y)
