from singleton_decorate import Singleton


@Singleton
class Foo:
    def __init__(self):
        print('Foo created')


@Singleton
class Bar:
    def __init__(self, value):
        self.value = value

def main():
    try:
        f = Foo()  # Error, this isn't how you get the instance of a singleton
    except Exception as e:
        print(e)

    f = Foo.Instance()  # Good. Being explicit is in line with the Python Zen
    g = Foo.Instance()  # Returns already created instance

    print(id(f) == id(g))  # True
    print(f is g)  # True


if __name__ == '__main__':
    main()
