'''
Design Pattern Sigleton
implementação #7
'''


class SingletonDecorator:
    __instance = None

    def __init__(self, decorated):
        self.__decorated = decorated

    def __call__(self, *args, **kwds):
        try:
            return self.__instance
        except AttributeError:
            self.__instance = self.__decorated()
            return self.__instance


@SingletonDecorator
class Singleton:
    val = None
    pass


@SingletonDecorator
class Singletest:
    val = None

    def __init__(self, value):
        self.value = value

    pass


def test_0():
    a = Singletest('')
    b = Singletest('')
    x = Singletest('')
    x.value = 'a'
    assert (a is b) == True
    assert (id(a) == id(b)) == True
    assert x.val == 'a'
    assert a.val == 'a'
    assert b.val == 'a'


if __name__ == '__main__':
    x = Singleton()
    y = Singleton()
    z = Singleton()

    print(id(x), id(y), id(z))
    print(x is y is z)
