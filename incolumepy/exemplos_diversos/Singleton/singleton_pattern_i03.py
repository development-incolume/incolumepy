'''
Design Pattern Sigleton
implementação #3
'''


class Borg:
    __shared_state = {}

    def __init__(self):
        '''
        clona o __dict__ para todas as instancias
        '''
        self.__dict__ = self.__shared_state


class Singleton(Borg):
    def __init__(self, arg):
        Borg.__init__(self)
        self.val = arg

    def __str__(self): return self.val


def test_0():
    a = Singleton('a')
    b = Singleton('b')
    b.val = 'ok'
    assert (a is b) == False
    assert (id(a) == id(b)) == False
    assert (a.val == b.val) == True
    assert a.val == 'ok'
    assert b.val == 'ok'


if __name__ == '__main__':
    pass
    x = Singleton('a')
    y = Singleton('b')
    print(id(x), id(y))
    print(x is y)
