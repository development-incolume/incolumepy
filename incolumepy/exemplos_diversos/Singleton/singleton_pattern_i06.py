'''
Design Pattern Sigleton
implementação #6
'''


class SingletonMetaClass(type):
    def __init__(self, name, bases, dict):
        super(SingletonMetaClass, self).__init__(name, bases, dict)
        original_new = self.__new__

        def my_new(self, *args, **kwds):
            if self.instance == None:
                self.instance = original_new(self, *args, **kwds)
            return self.instance

        self.instance = None
        self.__new__ = staticmethod(my_new)


class Singleton(metaclass=SingletonMetaClass):
    def __init__(self, val):
        self.val = val

    def __str__(self):
        return 'self' + self.val


def test_0():
    a = Singleton('b')
    b = Singleton('a')
    assert (a == b) == True
    assert (id(a) is id(b)) == True


if __name__ == '__main__':
    pass
    x = Singleton('a')
    y = Singleton('b')
    z = Singleton('c')
    print(id(x), id(y), id(z))
    print(x is y is z)
