class Singleton(object):
    __instance = None

    def __new__(self, *args, **kwargs):
        if not self.__instance:
            self.__instance = super(Singleton, self).__new__(
                self, *args, **kwargs)
        return self.__instance


def test_0():
    s1 = Singleton()
    s2 = Singleton()
    assert id(s1) == id(s2)
    assert (s1 is s2) == True

if __name__ == '__main__':
    pass
