import fileinput
import re

import pytest

'''
http://pythontesting.net/python/regex-search-replace-examples/#in_python
'''


def create_file(filename):
    '''
    gera nomes para novos arquivos prefix999.sufix
    :param filename:
    :return:
    '''
    while True:
        try:
            with open(filename, 'r') as f:
                raise FileExistsError('File exists')
        except FileNotFoundError:
            with open(filename, 'w') as file:
                file.write('')
            return filename
        except FileExistsError:
            file = re.findall(r'([0-9]+)', filename)
            print(file)
            return file
        except Exception as e:
            print(e)


def foo_file():
    try:
        with open('file', 'w') as f:
            f.write('foo')
    except:
        raise

def replace():
    try:
        foo_file()
        for line in fileinput.input(files='file'):
            line = re.sub('foo', 'bar', line.rstrip())
            print(line)
        return True
    except Exception:
        return False



def replace1():
    try:
        for line in fileinput.input(files='file', inplace=1, backup='.bkp', ):
            line = re.sub('foo', 'bar', line.rstrip())
            print(line)
        return True
    except:
        return False


def replace2():
    try:
        with open('wiki.txt', 'w') as f:
            f.write('* [the label](#the_anchor)')

        for line in fileinput.input(files='wiki.txt'):
            line = re.sub(r'\* \[(.*)\]\(#(.*)\)', r'<h2 id="\2">\1</h2>', line.rstrip())
            print(line)
        return True
    except:
        return False


def test_0():
    with pytest.raises(FileNotFoundError) as err:
        create_file('bla.txt')
        err.match(r'[Errno 2] No such file or directory')


def test_1():
    with pytest.raises(FileExistsError) as err:
        create_file('file')



if __name__ == '__main__':
    pass
    # functions = [replace(), replace1(), replace2()]

    replace()
    replace1()
    replace2()
