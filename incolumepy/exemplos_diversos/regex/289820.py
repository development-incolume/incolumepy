import re


Texto = "54 oz (163 g)"

Resultado = re.search('\((\d+) g\)', Texto)
print(Resultado.string)
print(Resultado.group())
print(Resultado.group(1))


Resultado2 = re.search('(?!\()\d+(?=\s*[a-zA-Z]\))', Texto)
print()
print(Resultado2.string)
print(Resultado2.group())


Resultado3 = re.search('\w+ \((\d+) \w+\)', Texto)
print()
print(Resultado3.string)
print(Resultado3.group())
print(Resultado3.group(1))


Resultado4 = re.search('\((\d+) \w+\)', Texto)
print()
print(Resultado4.string)
print(Resultado4.group())
print(Resultado4.group(1))
