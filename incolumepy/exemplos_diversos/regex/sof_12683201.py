import re

s = "one two 3.4 5,6 seven.eight nine,ten"
parts = re.split('\s|(?<!\d)[,.](?!\d)', s)
print(parts)
# ['one', 'two', '3.4', '5,6', 'seven', 'eight', 'nine', 'ten']