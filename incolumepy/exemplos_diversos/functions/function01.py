def funct(a=''):
    return a


def isVogal(letra):
    vogais = 'aeiou'
    return letra.lower() in vogais

def vogal():
    print(isVogal(input("Digite uma letra: ")))


if __name__ == '__main__':
    print(funct('a'))
    print(isVogal('a'))
    print(isVogal('b'))
    print(isVogal('C'))
    print(isVogal('E'))
    vogal()