def fahrenheit2celsius(temperatura):
    return (5 / 9) * (temperatura - 32)


def celsius2fahrenheit(temperatura):
    return (9 / 5) * temperatura + 32


if __name__ == '__main__':
    print(fahrenheit2celsius(212))
    print(celsius2fahrenheit(100))
