POL_CEN_PESO = 2.54


def pol2cm(polegadas):
    try:
        polegadas = float(polegadas)
        return polegadas * POL_CEN_PESO
    except:
        raise


def cm2pol(centimetros):
    try:
        centimetros = float(centimetros)
        return centimetros / POL_CEN_PESO
    except:
        raise


if __name__ == '__main__':
    print(pol2cm(6))
    print(cm2pol('15.24'))
