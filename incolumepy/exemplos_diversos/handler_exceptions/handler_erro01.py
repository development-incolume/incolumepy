#https://www.vivaolinux.com.br/artigo/Basico-sobre-tratamento-de-excecoes-em-Python-34

from urllib.request import urlopen
import urllib.error

try:
    urlopen('http://www.webcheats.com.br')
except urllib.error.HTTPError as e:
    print('Erro HTTP, código do erro: {code}'.format(code = e.code))
except urllib.error.URLError:
    print('erro ao tentar abrir a URL')
