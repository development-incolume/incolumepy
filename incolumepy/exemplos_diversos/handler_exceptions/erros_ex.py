'''recebe 3 valores entre 10 e 20'''


def getValues():
    l = list()
    while len(l) < 3:
        try:
            x = int(input('Digite o numero: '))
            if not (10 < x <= 20):
                raise ValueError('Numero Inválido.')
            l.append(x)

        except ValueError:
            print('Numero Inválido.')
        except Exception as e:
            print(e)
    return l


print(getValues())
