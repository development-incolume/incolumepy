'''aceita numero apenas entre 10 20'''


def getNum(x):
    try:
        x = int(x)
        if not (10 < x < 20):
            raise ValueError
        return x
    except ValueError:
        print('Valor digitado inválido!')
        return False
    except:
        raise


if __name__ == '__main__':
    getNum(input('digite um numero entre 10 e 20: '))
