def e_handler():
    try:
        with open('none.txt') as file:
            file.read()
    except Exception as e:
        print(e)
        print(e.strerror)
        print(e.args)
        print(e.__class__)


if __name__ == '__main__':
    e_handler()
