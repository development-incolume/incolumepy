__author__ = '@britodfbr'

import logging
import time


#create logger
logging.basicConfig(filename='exceptions_exemplos.log', level=logging.DEBUG)
logger = logging.getLogger()


def read_file_timed(path):
    '''Return the content of the file at path and measure time required.'''
    start_time = time.time()
    try:
        f = open(path, mode='rb')
        data = f.read()
        return data
    except FileNotFoundError as e:
        logger.error(e)
        raise
    else:
        f.close()
    finally:
        stop_time = time.time()
        dt = stop_time - start_time
        logger.info("Time required for {file} = {time}".format(file=path, time=dt))


if __name__ == '__main__':
    data = read_file_timed(u'/home/brito/Vídeos/Reino Escondido.Dub.rmvb')
    data = read_file_timed('/tmp/img.png')