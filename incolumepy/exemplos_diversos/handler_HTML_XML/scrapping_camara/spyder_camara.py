'''
Spyder do texto da legislação na camara
'''
__author__ = '@britodfbr'
import platform


class Spyder:
    def __init__(self):
        pass

    def load_dirvers(self):
        if platform.system() == 'Windows':
            chromebin = r'C:\Users\ricardobn\AppData\Local\Programs\Python\Python36-32\selenium\webdriver\chromedriver.exe'
            firefoxbin = r'C:\Users\ricardobn\AppData\Local\Programs\Python\Python36-32\selenium\webdriver\geckodriver.exe'
            encode = 'iso8859-1'
        elif platform.system() == 'Linux':
            chromebin = '../../../drivers/chromedriver'
            firefoxbin = '../../../drivers/geckodriver'
            encode = 'utf-8'
        else:
            raise OSError('Sistema operacional não identificado.')

    @staticmethod
    def run():
        pass


if __name__ == '__main__':
    Spyder.run()
