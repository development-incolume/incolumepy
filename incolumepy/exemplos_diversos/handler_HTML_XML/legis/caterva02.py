from xml.etree import ElementTree

def implem01():
    with open('podcasts.opml', 'rt') as f:
        tree = ElementTree.parse(f)
    for node in tree.findall('.//outline'):
        url = node.attrib.get('xmlUrl')
        if url:
            print(url)

def implem02():
    #ElementTree_find_feeds_by_structure.py
    with open('podcasts.opml', 'rt') as f:
        tree = ElementTree.parse(f)
    for node in tree.findall('.//outline/outline'):
        url = node.attrib.get('xmlUrl')
        print(url)


if __name__ == '__main__':
    try:
        implem02()
    except:
        raise
