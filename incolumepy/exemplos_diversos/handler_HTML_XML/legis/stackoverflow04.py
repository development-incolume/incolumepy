'''https://stackoverflow.com/questions/14694482/converting-html-to-text-with-python'''
#!/usr/bin/env python

from urllib.request import urlopen
import html2text
from bs4 import BeautifulSoup

def html2xml():
    soup = BeautifulSoup(urlopen('http://www.planalto.gov.br/ccivil_03/LEIS/L9478.htm').read())
    txt = soup.find('div', {'class' : 'body'})
    print(html2text.html2text(txt))

if __name__ == '__main__':
    html2xml()
