#!/usr/bin/env python
#  -*- coding: utf-8 -*-
from legis_1b import Legis
#from incolumepy.utils.files import ll


def truncus03(file='../../../../../saj_projects/exemplos/sican_others/normativos04/D70916.html'):

    a = Legis()
    a.file = file
    soup = a.get_soup_from_file()
    print('a.get_soup_from_file .. ok')
    #print(soup)
    epigrafe = soup.select('p[class="epigrafe"]')[0].text.strip().replace('.','').split(',')
    print(epigrafe)
    print(a.date_conv(epigrafe[-1]))

def truncus04():
    a = Legis()
    a.file = '../../../../../saj_projects/exemplos/sican_others/normativos04/D70916.html'
    a.get_soup_from_file()
    print(a.date)
    print(a.set_date())
    print(a.date)

def truncus05():
    pass


def run():
    truncus05()


if __name__ == '__main__':
    run()
