#!/usr/bin/env python
#  -*- coding: utf-8 -*-
__author__ = "@britodfbr"
__copyright__ = "Copyright 2007, incolume.com.br"
__credits__ = ["Ricardo Brito do Nascimento"]
__license__ = "GPL"
__version__ = "1.0a"
__maintainer__ = "@britodfbr"
__email__ = "contato@incolume.com.br"
__status__ = "Production"


from bs4 import BeautifulSoup
from bs4 import Doctype
from bs4 import Comment
import os
from copy import copy
import platform
from _datetime import datetime
import locale



class Legis:
    def __init__(self, **kwargs):
        self.soup = None
        self.file = None
        self.urls = []
        self.date = None
        for item, value in kwargs.items():
            self.__dict__[item] = value


    def link_css(self, **kwargs):
        urls=["http://www.planalto.gov.br/ccivil_03/css/legis_3.css",
              "https://www.planalto.gov.br/ccivil_03/css/legis_3.css"]
        urls.append('css/legis_3.css')

        if 'url' in kwargs:
            urls.append(kwargs['url'])

        soup = BeautifulSoup('', 'html.parser')
        for item in urls:
            soup.append(soup.new_tag('link', type="text/css", rel="stylesheet", href=item))
        return soup


    def meta(self, **kwargs):
        itens = {'numero':None, 'tipo':'dec', 'ano':2018, 'situacao':"vigente ou revogado", 'origem':'Poder Executivo',
                 'chefegoverno':'', 'referenda':'','correlacao': '', 'veto':'', 'fonte':'', 'dataassinatura':'',
                 "generator_user":"@britodfbr", "Copyright":'PORTARIA Nº 1.492 DE 5/10/2011. http://www.planalto.gov.br/ccivil_03/Portaria/P1492-11-ccivil.htm'}
        soup = BeautifulSoup('', 'html.parser')


        for key, value in kwargs.items():
            itens[key] = value

        for key, value in itens.items():
            # metadados copyright
            tag = soup.new_tag('meta')
            tag['content'] = value
            tag['name'] = key
            soup.append(tag)

        return soup


    def nav(self):
        '''
        <nav>
        <ul>
        <li><a href="#">Vide Recurso extraordinário nº 522897</a></li>
        <li>
           <a class="show-action" href="#">Texto completo</a>
           <a class="hide-action" href="#view">Texto original</a>
        </li>
        <li><a class="textoimpressao" href="#textoimpressao">Texto para impressão</a></li>
        </ul>
        </nav>
        :return:
        '''

        soup = BeautifulSoup('', 'lxml')
        soup.append(soup.new_tag('nav'))
        soup.nav.append(soup.new_tag('ul'))

        a = soup.new_tag('a', href="#view")
        a.string = 'Texto compilado'
        a['class']="hide-action"

        a1 = soup.new_tag('a', href="#")
        a1.string = 'Texto original'
        a1['class']="show-action"

        li = soup.new_tag('li')
        li.append(a)
        li.append(a1)
        soup.nav.ul.append(li)

        a2 = soup.new_tag('a', href="#textoimpressao")
        a2['class']="textoimpressao"
        a2.string = 'Texto para impressão'

        a3 = soup.new_tag('a', href='#')
        a3.string = 'Vide Recurso extraordinário nº 522897'

        soup.nav.ul.append(soup.new_tag('li'))
        soup.nav.ul.li.next_sibling.append(a2)
        soup.nav.ul.insert(0, soup.new_tag('li'))
        soup.nav.ul.li.append(a3)

        return soup


    def baseurl(self, **kwargs):
        soup = BeautifulSoup('', 'lxml')
        if not kwargs['target']:
            kwargs['target'] = '_blank'
        if not kwargs['href']:
            raise ValueError('href nao definido')
        soup.append(soup.new_tag('base', target=kwargs['target'], href=kwargs['href']))
        return soup


    def header(self, **kwargs):
        '''  <header>
        <h1>
        Presidência da República
        </h1>
        <h2>
        Casa Civil
        </h2>
        <h3>
        Subchefia para Assuntos Jurídicos
        </h3>
        </header>'''
        if not kwargs:
            kwargs['h1'] = 'Presidência da República'
            kwargs['h2'] = 'Presidência da República'
            kwargs['h3'] = 'Presidência da República'

        soup = BeautifulSoup('', 'lxml')
        soup.append(soup.new_tag('header'))
        soup.header.append(soup.new_tag('h1'))
        soup.header.append(soup.new_tag('h2'))
        soup.header.append(soup.new_tag('h3'))
        soup.header.h1.string = 'Presidência da República'
        soup.header.h2.string = 'Casa Civil'
        soup.header.h3.string = 'Subchefia para Assuntos Jurídicos'
        return soup


    def doctype(self):
        tag = Doctype('html')

        return tag


    def comment(self, text):
        tag = Comment(text)
        return tag


    @property
    def file(self):
        return self._file


    @file.setter
    def file(self, value):
        if value:
            self._file = os.path.abspath(value)


    def get_soup_from_file(self, parser_index=0):
        parser = ["html.parser", 'lxml', 'lxml-xml', 'xml', 'html5lib']
        if not self._file:
            raise ValueError('self.file not defined')
        try:
            f = open(self._file).read()
        except:
            f = open(self._file, encoding='iso8859-1').read()

        self.soup = BeautifulSoup(f, parser[parser_index])
        return self.soup


    def replace_brastra(self, img=None, index=0):
        img_l=['http://www.planalto.gov.br/ccivil_03/img/Brastra.gif',
             'http://www.planalto.gov.br/ccivil_03/img/Brastra.png',
             'http://www.planalto.gov.br/ccivil_03/img/Brastra01.png',
             'http://www.planalto.gov.br/ccivil_03/img/brasaorep.png'
             ]
        logo = self.soup.select('img[src*="Brastra"]')
        if img:
            logo[0]['src'] = img
        else:
            logo[0]['src'] = img_l[index]
        return (True, logo)


    def date_conv(self, date):
        ''' convert date to timestamp
                :param date: ' de 8 de Janeiro de 1900'
                :return: 1900/1/8
        '''
        if platform.system() == 'Linux':
            locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
        else:
            locale.setlocale(locale.LC_ALL, 'pt_BR')

        try:
            return datetime.strptime(date, ' de %d de %B de %Y').strftime('%Y/%m/%d')
        except ValueError:
            return datetime.strptime(date, ' de %dº de %B de %Y').strftime('%Y/%m/%d')
            pass


    def set_date(self):
        epigrafe = self.soup.select('p[class="epigrafe"]')[0].text.strip().replace('.','').split(',')
        self.date = self.date_conv(epigrafe[-1])
        return self.date




def run():
    a = Legis()
    print(type(a.link_css()))
    print((a.link_css().prettify()))

    print('*'*20)
    print(type(a.meta(numero=1234)))
    print(a.meta(numero=1234).prettify())
    print('*'*20)
    print(a.header().prettify())
    print('*'*20)
    print(a.nav().prettify())
    print('*'*20)
    print(a.baseurl(href='http://www.planalto.gov.br/ccivil_03/', target='_blank'))
    print('*' * 20)

    print(a.doctype())
    print(type(a.comment('Comentario de teste.')))

    print('*' * 20)
    a.file = '../../../../../saj_projects/CCIVIL_03/decreto/1980-1989/1985-1987/D95578.htm'
    print(a.file)
    print('*' * 20)
    print(a.get_soup_from_file())
    print('*' * 20)
    print(a.replace_brastra('http://www.planalto.gov.br/ccivil_03/img/Brastra.png'))
    print(a.soup)

    print('*' * 20)
    a.file = '../../../../../saj_projects/CCIVIL_03/decreto/D3630.htm'
    (a.get_soup_from_file())
    print(a.replace_brastra())
    print(a.soup)

    print(a.date_conv('DECRETO Nº 9.366, DE 8 DE MAIO DE 2018'))

if __name__ == '__main__':
    pass
    run()
