#!/usr/bin/env python
#  -*- coding: utf-8 -*-
from legis import Legis


def run0():
    a = Legis()
    print('*' * 20)
    a.file = '../../../../../saj_projects/CCIVIL_03/decreto/1980-1989/1985-1987/D95578.htm'
    print(a.file)

def run1():
    a = Legis()

    print('*' * 20)
    print(a.get_soup_from_file())

def run2():
    a = Legis()
    print(a.get_soup_from_file())

    print('*' * 20)
    a.file = '../../../../../saj_projects/CCIVIL_03/decreto/1980-1989/1985-1987/D95578.htm'
    print(a.file)
    print('*' * 20)
    print(a.get_soup_from_file())
    print('*' * 20)
    print(a.replace_brastra('http://www.planalto.gov.br/ccivil_03/img/Brastra.png'))
    print(a.soup)

    print('*' * 20)
    a.file = '../../../../../saj_projects/CCIVIL_03/decreto/D3630.htm'
    (a.get_soup_from_file())
    print(a.replace_brastra())
    print(a.soup)


def run():
    a = Legis()

    print('*' * 20)
    a.file = '../../../../../saj_projects/CCIVIL_03/decreto/D3630.htm'
    print(a.get_soup_from_file())
    print(a.replace_brastra('http://www.planalto.gov.br/ccivil_03/img/Brastra.png'))
    print(a.soup)

def main():
    run2()
    run1()
    run0()
    run()

if __name__ == '__main__':
    main()