#!/usr/bin/env python
#  -*- coding: utf-8 -*-
from legis import Legis


def run():
    a = Legis()

    a.file = '../../../../../saj_projects/CCIVIL_03/decreto/D3630.htm'
    print('*' * 20)
    print(a.replace_brastra('http://www.planalto.gov.br/ccivil_03/img/Brastra.png'))
    print(a.soup)

def main():
    run()

if __name__ == '__main__':
    main()