import json
import re
from pprint import pprint

import requests
from bs4 import BeautifulSoup


def paises_lst(url='http://www.geonames.org/countries/', proxies=None):
    page = requests.get(url=url, proxies=proxies)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')
    header = None
    result = []
    for linha in table:
        if isinstance(linha, str):
            continue
        if not header:
            header = [campo.get_text(' ') for campo in linha]
            continue
        tupla = [campo.get_text(' ') for campo in linha]
        result.append({x: y for x, y in zip(header, tupla)})
    print(result)
    return result


def paises_str_json(url='http://www.geonames.org/countries/', proxies=None):
    page = requests.get(url=url, proxies=proxies)
    soap = BeautifulSoup(page.content, 'html.parser')
    header = soap.find('h3').get_text()
    table = soap.find(id='countries')
    headers = None
    result = {}
    for linha in table:
        if isinstance(linha, str):
            continue
        if not headers:
            headers = [campo.get_text(' ') for campo in linha]
            continue
        tupla = [campo.get_text(' ') for campo in linha]
        row = {x: y for x, y in zip(headers, tupla)}
        result[row['ISO-3166 alpha2']] = row
        result[row['ISO-3166 alpha3']] = row
        result[row['ISO-3166 numeric']] = row


        # print(row)
    result["_comment"] = "get at {}".format(page.url)
    print(result)
    return (result)


def paises_json0(url='http://www.geonames.org/countries/', proxies=None):
    page = requests.get(url=url, proxies=proxies)
    soup = BeautifulSoup(page.content, 'html.parser')
    table = soup.find(id='countries')
    result = {}
    filename = '{}.json'.format(soup.find('h3').get_text().replace(' ', ''))
    filename = '{}.json'.format(*(re.findall('\w+', soup.find('h3').get_text())))
    filename = '{}.json'.format(''.join(re.findall('\w+', soup.find('h3').get_text())))
    filename = '{}{}.json'.format(*soup.find('h3').get_text().split()).lower()
    headers = None
    for linha in table:
        if isinstance(linha, str):
            continue
        if not headers:
            headers = [campo.get_text(' ') for campo in linha]
            continue
        tupla = [campo.get_text(' ') for campo in linha]
        row = {x: y for x, y in zip(headers, tupla)}
        result[row['ISO-3166 numeric']] = row

    result["_comment"] = "get at {}".format(page.url)

    for i in result.items():
        print(i)

    with open(filename, 'w') as fileout:
        fileout.write(json.dumps(result))
        json.dump(result, fileout, sort_keys=True, indent=4, ensure_ascii=False)

    return True


def paises_json1(filename='paises.json', url='http://www.geonames.org/countries/', proxies=None):
    result = paises_str_json(url=url, proxies=proxies)
    with open(filename, 'w') as fileout:
        fileout.write(json.dumps(result))
        json.dump(result, fileout, sort_keys=True, indent=4, ensure_ascii=False)
    return True


def paises_json2(filename='paises.json', url='http://www.geonames.org/countries/', proxies=None):
    page = requests.get(url=url, proxies=proxies)
    soup = BeautifulSoup(page.content, 'html.parser')
    title = soup.find('title').get_text().lower()
    result = {}
    headers = None
    tupla = []
    header = ''.join(soup.find('h3').get_text().split()).lower()

    table = soup.find(id='countries')

    for line in table:
        if isinstance(line, str):
            continue
        if not headers:
            headers = [campo.get_text(' ') for campo in line]
            continue
        row = [campo.get_text(' ') for campo in line]
        tupla.append({x: y for x, y in zip(headers, row)})

        # print(row)
    result["_comment"] = "get at {}".format(page.url)
    result[header] = tupla
    # print(tupla)
    with open('{}.json'.format(title), 'w') as f:
        json.dump(result, f, sort_keys=True, indent=2, ensure_ascii=False)
    return True


def paises_json3(filename=None, url='http://www.geonames.org/countries/', proxies=None):
    try:
        page = requests.get(url=url, proxies=proxies)
        soup = BeautifulSoup(page.content, 'html.parser')
        title = soup.find('title').get_text().lower()
        header = ''.join(soup.find('h3').get_text().lower().split())
        if not filename:
            filename = '{}_{}.json'.format(title, header)
        table = soup.find(id='countries')
        headers = None
        dict_row = {}

        for line in table:
            if isinstance(line, str):
                continue
            if not headers:
                headers = [campo.get_text(' ') for campo in line]
            row = [campo.get_text('') for campo in line]
            tupla = {x: y for x, y in zip(headers, row)}
            # dict_row[tupla['ISO-3166 alpha2']] = tupla
            # dict_row[tupla['ISO-3166 alpha3']] = tupla
            # dict_row[tupla['ISO-3166 numeric']] = tupla
            dict_row[tupla['Country']] = tupla
        dict_row["_comment"] = "get at {}".format(page.url)

        with open(filename, 'w') as f:
            json.dump(dict_row, f, sort_keys=True, indent=2, ensure_ascii=False)

        print(dict_row)
        return True
    except:
        raise


def paises_json(filename=None, order=0,
                url='http://www.geonames.org/countries/', proxies=None, tmout=2):
    '''
    Generate a json file
    :param filename: default title_h3.json
    :param order: default Country, 1: ISO-3166 alpha2; 2: ISO-3166 alpha3; 3: ISO-3166 numeric
    :param url: default 'http://www.geonames.org/countries/'
    :param proxies: default None
    :param tmout: default 2
    :return: True if create a json file
    '''
    try:
        page = requests.get(url=url, proxies=proxies, timeout=tmout)
        soup = BeautifulSoup(page.content, 'html.parser')
        title = soup.find('title').get_text().lower()
        header = ''.join(soup.find('h3').get_text().lower().split())
        if not filename:
            filename = '{}_{}.json'.format(title, header)
        order = int(order)
        table = soup.find(id='countries')
        headers = None
        dict_row = {}

        for line in table:
            if isinstance(line, str):
                continue
            if not headers:
                headers = [campo.get_text(' ') for campo in line]
            row = [campo.get_text('') for campo in line]
            tupla = {x: y for x, y in zip(headers, row)}
            if order == 1:
                dict_row[tupla['ISO-3166 alpha2']] = tupla
            elif order == 2:
                dict_row[tupla['ISO-3166 alpha3']] = tupla
            elif order == 3:
                dict_row[tupla['ISO-3166 numeric']] = tupla
            else:
                dict_row[tupla['Country']] = tupla
        dict_row["_comment"] = "get at {}".format(page.url)

        with open(filename, 'w') as f:
            json.dump(dict_row, f, sort_keys=True, indent=2, ensure_ascii=False)

        print(dict_row)
        return True
    except:
        raise


# paises_json()
# #print(type(paises_json()), paises_json())
# pprint(paises_json())
pprint(paises_json())
paises_json(filename='paises.json')
paises_json(filename='paises1.json', order=1)
paises_json(filename='paises2.json', order=2)
paises_json(filename='paises3.json', order=3)
paises_json(filename='paises4.json', order=4)
