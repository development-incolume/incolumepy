proxies = {
    'http': 'http://10.1.101.101:8080',
    'https': 'http://10.1.101.101:8080',
    'ftp': 'http://10.1.101.101:8080',
}


def truncus01a():
    '''
    Web Scraping:
    https://www.youtube.com/watch?v=XQgXKtPSzUI
    '''
    from bs4 import BeautifulSoup as soup
    from urllib.request import urlopen as uReq

    my_url = 'https://www.newegg.com/Video-Cards-Video-Devices/Category/ID-38'
    fileoutput = 'prices.cvs'
    uClient = uReq(my_url)
    page_html = uClient.read()
    uClient.close()

    page_soup = soup(page_html, 'html.parser')
    print(page_soup.h1)

    containers = page_soup.find_all('div', {'class': 'item-container'})
    print(len(containers))
    container = containers[0]

    header = 'brand; product_name; shipping\n'
    f = open(fileoutput, 'w')
    f.write(header)

    for container in containers:
        brand = product_name = shipping = None
        try:
            brand = (container.div.div.a.img['title'])
        except AttributeError:
            brand = '***'
        except:
            raise
        title_container = container.findAll('a', {'class': 'item-title'})
        product_name = title_container[0].text
        shipping_container = container.findAll('li', {'class': 'price-ship'})
        shipping = shipping_container[0].text.strip()
        print("{}; {}; {}".format(brand, product_name, shipping))
        f.write("{}; {}; {}\n".format(brand, product_name, shipping))
    f.close()

def truncus01b():
    '''
    Web Scraping:
    https://www.youtube.com/watch?v=XQgXKtPSzUI
    '''
    from bs4 import BeautifulSoup
    from urllib.request import urlopen

    my_url = 'https://www.newegg.com/Video-Cards-Video-Devices/Category/ID-38'
    fileoutput = 'catalogo.cvs'
    page_html = urlopen(my_url).read()
    page_soup = BeautifulSoup(page_html, 'html.parser')
    #print(page_soup)
    containers = page_soup.find_all('div', {'class': 'item-container'})
    with open(fileoutput, 'w') as f:
        f.write('brand; product_name; shipping; price\n')
        for container in containers:
            title_container = container.findAll('a', {'class': 'item-title'})
            product_name = title_container[0].text
            brand = product_name.split()[0]
            shipping_container = container.findAll('li', {'class': 'price-ship'})
            shipping = shipping_container[0].text.strip()
            price_container = container.findAll('li', {'class': 'price-current'})
            price = price_container[0].text.replace('|', '').replace('–','').strip()
            #print(price)
            print("{}; {}; {}; {}".format(brand, product_name, shipping, price))
            f.write("{}; {}; {}; {}\n".format(brand, product_name, shipping, price))

def truncus01c():
    '''
    Web Scraping:
    https://www.youtube.com/watch?v=XQgXKtPSzUI
    '''
    from bs4 import BeautifulSoup
    import requests

    my_url = 'https://www.newegg.com/Video-Cards-Video-Devices/Category/ID-38'
    fileoutput = 'catalog.cvs'
    page_html = requests.get(my_url, proxies=proxies, timeout=5).content
    page_soup = BeautifulSoup(page_html, 'html.parser')
    #print(page_soup)
    containers = page_soup.find_all('div', {'class': 'item-container'})
    with open(fileoutput, 'w') as f:
        f.write('brand; product_name; shipping; price\n')
        for container in containers:
            title_container = container.findAll('a', {'class': 'item-title'})
            product_name = title_container[0].text
            brand = product_name.split()[0]
            shipping_container = container.findAll('li', {'class': 'price-ship'})
            shipping = shipping_container[0].text.strip()
            price_container = container.findAll('li', {'class': 'price-current'})
            price = price_container[0].text.replace('|', '').replace('–','').strip()
            #print(price)
            print("{}; {}; {}; {}".format(brand, product_name, shipping, price))
            f.write("{}; {}; {}; {}\n".format(brand, product_name, shipping, price))

if __name__ == '__main__':
#    truncus01a()
#    truncus01b()
    truncus01c()
