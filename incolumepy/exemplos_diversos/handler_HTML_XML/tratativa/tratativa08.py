import html2text

def f01():
    print(dir(html2text))


def f02():
    h = html2text.HTML2Text()
    h.ignore_links = True
    print (h.handle("<p>Hello, <a href='http://earth.google.com/'>world</a>!"))


def f03(filehtml='../../../data/ipsum.html'):
    '''
    tranform file html to file text
    :param filehtml: 
    :return: 
    '''
    h = html2text.HTML2Text()
    h.ignore_links = True
    print(h.handle(open(filehtml, 'r', encoding='utf-8').read()))
    pass

def f04(filehtml='../../../data/ipsum.html'):
    '''
    tranform file html to file text
    :param filehtml: 
    :return: 
    '''
    h = html2text.HTML2Text()
    h.ignore_links = False
    print(h.handle(open(filehtml, 'r', encoding='utf-8').read()))
    pass


if __name__ == '__main__':
    f03()
    f04()
    pass