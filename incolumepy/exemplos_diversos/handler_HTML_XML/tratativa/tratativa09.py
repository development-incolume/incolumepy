import requests
import html2text

def f01(filehtml='../../../data/ipsum.html'):
    '''
    tranform file html to file text
    :param filehtml: 
    :return: 
    '''
    try:
        h = html2text.HTML2Text()
        h.ignore_links = False
        print(h.handle(open(filehtml, 'r', encoding='utf-8').read()))
    except FileNotFoundError as e:
        print(e)
    except:
        raise
    pass

def f02(url):
    '''
    
    :param url: 
    :return: 
    '''
    h = html2text.HTML2Text()
    h.ignore_links = False
    content = h.handle(requests.get(url, timeout=2).text)
    print(content)

def f03(filehtml, filetxt):
    try:
        h = html2text.HTML2Text()
        h.ignore_links = False
        content = h.handle(open(filehtml, 'r', encoding='utf-8').read())
        with open(filetxt,'w', encoding='utf-8') as file:
            file.write(content)
    except FileNotFoundError as e:
        print(e)

def f04(url, filetxt):
    try:
        h = html2text.HTML2Text()
        h.ignore_links = False
        content = h.handle(requests.get(url).text)
        with open(filetxt, 'w', encoding='utf-8') as file:
            file.write(content)
    except FileNotFoundError as e:
        print(e)
    except:
        raise


if __name__ == '__main__':
    f01()
    f02('http://brito.blog.incolume.com.br')
    f03('../../../data/ipsum.html', 'ipsum.txt')
    f04('http://brito.blog.incolume.com.br', 'britoblog.txt')

    f01('../fhtml/D9255.htm')
    pass