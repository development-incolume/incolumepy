from austintaylor import downloadxml
import xml.etree.ElementTree as ET
import pandas as pd


class XML2DataFrame:

    def __init__(self, xml_data):
        self.root = ET.XML(xml_data)

    def parse_root(self, root):
        return [self.parse_element(child) for child in iter(root)]

    def parse_element(self, element, parsed=None):
        if parsed is None:
            parsed = dict()
        for key in element.keys():
            parsed[key] = element.attrib.get(key)
        if element.text:
            parsed[element.tag] = element.text
        for child in list(element):
            self.parse_element(child, parsed)
        return parsed

    def process_data(self):
        structure_data = self.parse_root(self.root)
        return pd.DataFrame(structure_data)

if __name__ == '__main__':
    xml2df = XML2DataFrame(downloadxml())
    xml_dataframe = xml2df.process_data()
    print(type(xml_dataframe))
    print(xml_dataframe.head())