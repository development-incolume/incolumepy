import requests

def downloadxml():
    dproxy = {'http': 'http://10.1.101.101:8080',
              'https': 'http://10.1.101.101:8080',
              'ftp': 'http://10.1.101.101:8080'
              }
    user_agent_url = 'http://www.user-agents.org/allagents.xml'
    xml_data = requests.get(user_agent_url, proxies = dproxy).content

    return (xml_data)


if __name__ == '__main__':
    print(downloadxml())