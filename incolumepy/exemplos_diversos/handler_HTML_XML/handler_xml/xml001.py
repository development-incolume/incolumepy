'''
https://docs.python.org/3/library/xml.etree.elementtree.html
'''
#coding: utf-8

import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup

def xml01(filexml='../../../data/country_data.xml'):
    tree = ET.parse(filexml)
    root = tree.getroot()
    print(root)

    print(root.tag)
    print(root.attrib)
    print(root.tag)
    print('-'*50)


def xml02(filexml='../../../data/country_data.xml'):
    tree = ET.parse(filexml)
    root = tree.getroot()

    for e in root:
        print(e.tag, e.attrib)
    print('-'*50)

def xml03(filexml='../../../data/country_data.xml'):
    tree = ET.parse(filexml)
    root = tree.getroot()

    for item in root.findall('country'):
        print(item.get('name'))

    for item in root.findall('neighbor'):
        print(item.get('name'))
    print('-'*50)

def xml04(filexml='../../../data/country_data.xml'):
    soup = BeautifulSoup(open(filexml), 'html.parser')
    print(soup.data.country.neighbor['name'])

    for i in soup.data:
        print(i)
    print('-'*50)

def xml05(filexml='../../../data/country_data.xml'):

    print('-'*50)


if __name__ == '__main__':
    #xml01()
    #xml02()
    #xml03()
    xml04()
    #xml05()
    pass
