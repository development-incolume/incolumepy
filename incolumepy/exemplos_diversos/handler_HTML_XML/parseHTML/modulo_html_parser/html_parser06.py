import os
import sys
from pprint import pprint as prettify

from selenium import webdriver

sys.path.append(os.path.abspath('../../'))

print(os.environ.get('PYTHONPATH'))
from legis import utils


def html_parser06a(url):
    driver = utils.select_plataforma('../../drivers')
    #    print(driver)
    chorme = webdriver.Chrome(driver['chromebin'])
    basename = utils.define_basename(url)
    print(basename)
    filename = utils.realfilename(basename, extout='xhtml', basedir='./xhtml')
    print(filename)
    chorme.get(url)
    chorme.execute_script()
    page = chorme.page_source
    prettify(page)

    pass


if __name__ == '__main__':
    pass
    nurl = utils.gen_url('../legis/relacao_url.txt')
    #    print(next(nurl))
    html_parser06a(next(nurl))
