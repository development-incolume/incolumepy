import os
import sys

from html_parser02 import MyHTMLParser as hp02_MyHTMLParser
from html_parser08 import MyHTMLParser as hp08_MyHTMLParser

sys.path.append(os.path.abspath('../../'))


# print(os.environ.get('PYTHONPATH'))


def html_parser07a(filein='./xhtml/L8112consol.htm'):
    with open(filein, 'r', encoding='iso8859-1') as f:
        page = f.read()
        parser = hp02_MyHTMLParser()
        parser.feed(page)


def html_parser07b(filein='./xhtml/L8112consol.htm'):
    os.makedirs('txt', mode=0o755, exist_ok=True)
    with open(filein, 'r', encoding='iso8859-1') as f, open('txt/L8112.txt', 'w') as file:
        page = f.read()
        parser = hp08_MyHTMLParser()
        parser.feed(page)
        file.write(parser.result)



if __name__ == '__main__':
    html_parser07a()
    html_parser07b()
