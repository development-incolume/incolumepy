from selenium import webdriver
import platform

if platform.system() == 'Windows':
    chromebin = r'C:\Users\ricardobn\AppData\Local\Programs\Python\Python36-32\selenium\webdriver\chromedriver.exe'
    firefoxbin = r'C:\Users\ricardobn\AppData\Local\Programs\Python\Python36-32\selenium\webdriver\geckodriver.exe'
    encode = 'iso8859-1'
elif platform.system() == 'Linux':
    chromebin = '../drivers/chromedriver'
    firefoxbin = '../drivers/geckodriver'
    encode = 'utf-8'
else:
    raise OSError('Sistema operacional não identificado.')

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('window-size=1920x1080')

driver = webdriver.Chrome(executable_path=chromebin, chrome_options=options)
driver.get("https://twitter.com/search?f=tweets&vertical=default&q=javascript")
