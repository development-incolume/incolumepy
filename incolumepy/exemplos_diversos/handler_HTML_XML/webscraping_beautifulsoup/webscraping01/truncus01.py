import inspect
import re

import requests
from bs4 import BeautifulSoup

url1='http://legis.senado.leg.br/legislacao/ListaTextoSigen.action?' \
     'norma=26343953&id=26343958&idBinario=26344177&mime=application/rtf'

def truncus01a():
    page = requests.get(url1).text
    #print(page)
    with open('D9266_sen00.html', 'w') as file:
        file.write(page)
    soup = BeautifulSoup(page, 'lxml')
    #print(soup.prettify())
    #print(soup.title.text.strip())
    # print(inspect.stack())
    # print(inspect.stack()[0][3])
    filename = "{}.html".format(inspect.stack()[0][3])
    with open(filename, 'w') as file:
        file.write(soup.prettify().strip())

    #teste = soup.findAll()
    #container_div = soup.find_all('div', class_="conteudoPrincipal")
    container = soup.find_all('div', class_="container")
    print(container)

    container = soup.find_all('div', id_="conteudoPrincipal")
    print(container)

    container = soup.find_all('div', {'id', 'conteudoPrincipal'})
    print(container)

    print(container)
    print('-' * 20)




def truncus01b():
    page = requests.get(url1).text
    #print(inspect.stack())
    #print(inspect.stack()[0][3])
    filename = "{}.html".format(inspect.stack()[0][3])
    #print(filename)
    soup = BeautifulSoup(page, 'html.parser')
    with open(filename, 'w') as file:
        file.write(soup.prettify())
    container = soup.find_all(id="conteudoPrincipal")
    #print(container)
    print(container[0].div.html)
    print(container[0].div.html.prettify())
    print('-' * 20)


def truncus01c():
    page = requests.get(url1).text
    filename = '{}.html'.format(inspect.stack()[0][3])
    soup = BeautifulSoup(page, 'html.parser').find_all(id='conteudoPrincipal')
    with open(filename, 'w') as file:
        file.write(soup[0].div.html.prettify())
    print('-' * 20)

def truncus01d():
    filename = '{}.html'.format(inspect.stack()[0][3])
    page = requests.get(url1)
    soup = BeautifulSoup(page.text, 'html5lib').find_all(id='conteudoPrincipal')
    container = soup[0].div.text
    print(container)
    with open(filename, 'w') as file:
        file.write(container)
        pass
    print('-' * 20)


def truncus01e():
    filename = '{}.html'.format(inspect.stack()[0][3])
    #print(filename)
    page = requests.get(url1)
    soup = BeautifulSoup(page.text, 'html.parser').find_all(id='conteudoPrincipal')
    container = soup[0].div.html
    print(container.prettify())
    with open(filename, 'w') as file:
        file.write(container.prettify())

    print('-' * 20)


def truncus01f():
    filename = '{}.html'.format(inspect.stack()[0][3])
    page = requests.get(url1)
    soup = BeautifulSoup(page.text, 'html.parser').find_all(id='conteudoPrincipal')
    print(soup)
    container = soup[0].div.html
    print(type(container))
    #html_tidy = re.sub(r' style="[^"]+"', '', container)
    #print(html_tidy)
    print('-' * 20)


def truncus01g():
    filename = '{}.html'.format(inspect.stack()[0][3])
    page = requests.get(url1)
    #print(page.text)
    page = re.sub(r' style="[^"]+"', '', page.text)
    soup = BeautifulSoup(page, 'html.parser').find_all(id='conteudoPrincipal')
    container = soup[0].div.html
    print(container.prettify())
    with open(filename, 'w') as file:
        file.write(container.prettify())
    print('-' * 20)


def truncus01h():
    filename = '{}.html'.format(inspect.stack()[0][3])
    page = re.sub(r' style="[^"]+"', '', requests.get(url1).text)
    page = re.sub(r'<span>', '', page)
    page = re.sub(r'</span>', '', page)
    soup = BeautifulSoup(page, 'html.parser').find_all(id='conteudoPrincipal')
    container = soup[0].div.html
    with open(filename, 'w') as file:
        file.write(container.prettify())
    print('-'*20)



def truncus01i():
    filename = '{}.html'.format(inspect.stack()[0][3])
    page = re.sub(r' style="[^"]+"', '', requests.get(url1).text)
    page = re.sub(r'<span>', '', page)
    page = re.sub(r'</span>', '', page)
    soup = BeautifulSoup(page, 'html.parser').find_all(id='conteudoPrincipal')
    caterva = soup[0].div.html.body

    container = caterva.find_all('div')[2]
    print(container)

    with open(filename, 'w') as file:
        file.write(container.prettify())
        pass
    print('-'*20)


def truncus01j():
    header = '<!DOCTYPE html>' \
             '<html lang="pt-br">' \
             '<head>' \
             '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>' \
             '<link rel="stylesheet" href="../../../saj_projects/view/legis_1.css">' \
             '</head>' \
             '<body>' \
             '<header> ' \
             '<h1>Presidência da República</h1> ' \
             '<h2>Casa Civil</h2> ' \
             '<h3>Subchefia para Assuntos Jurídicos</h3> ' \
             '</header>'
    footer='</body></html>'
    filename = '{}.html'.format(inspect.stack()[0][3])
    page = re.sub(r' style="[^"]+"', '', requests.get(url1).text)
    page = re.sub(r'<span>', '', page)
    page = re.sub(r'</span>', '', page)
    soup = BeautifulSoup(page, 'html.parser').find_all(id='conteudoPrincipal')
    caterva = soup[0].div.html.body

    container = caterva.find_all('div')[2]
    print(container)

    with open(filename, 'w') as file:
        file.write(BeautifulSoup(header, 'lxml').prettify())
        file.write(container.prettify())
        file.write(BeautifulSoup(footer, 'lxml').prettify())
        pass
    print('-' * 20)
    pass


def truncus01k():
    header = '<!DOCTYPE html>' \
             '<html lang="pt-br">' \
             '<head>' \
             '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>' \
             '<link rel="stylesheet" href="../../../saj_projects/view/legis_3.css">' \
             '</head>' \
             '<body>' \
             '<header> ' \
             '<h1>Presidência da República</h1> ' \
             '<h2>Casa Civil</h2> ' \
             '<h3>Subchefia para Assuntos Jurídicos</h3> ' \
             '</header>'
    footer='</p><p class="dou">Este texto não substitui o publicado no DOU de 17.1.2018</body></html>'
    filename = '{}.html'.format(inspect.stack()[0][3])
    page = re.sub(r' style="[^"]+"', '', requests.get(url1).text)
    page = re.sub(r'<span>', '', page)
    page = re.sub(r'</span>', '', page)
    page = re.sub(r'EpgrafeAlt1', 'epigrafe', page)
    page = re.sub(r'EmentaAlt2', 'ementa', page)
    page = re.sub(r'Assinatura1Alt7', 'presidente', page)
    page = re.sub(r'Assinatura2Alt8', 'ministro', page)
    soup = BeautifulSoup(page, 'html.parser').find_all(id='conteudoPrincipal')
    caterva = soup[0].div.html.body

    container = caterva.find_all('div')[2]
    print(container)

    with open(filename, 'w') as file:
        file.write(BeautifulSoup(header, 'lxml').prettify())
        file.write(container.prettify())
        file.write(BeautifulSoup(footer, 'lxml').prettify())
        pass
    print('-' * 20)
    pass

def truncus01l(url='http://legis.senado.leg.br/legislacao/'
                   'ListaTextoSigen.action?norma=483900&id=14310946&'
                   'idBinario=15795498&mime=application/rtf'):
    header = '<!DOCTYPE html>' \
             '<html lang="pt-br">' \
             '<head>' \
             '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>' \
             '<link rel="stylesheet" href="../../../saj_projects/view/legis_3.css">' \
             '</head>' \
             '<body>' \
             '<header> ' \
             '<h1>Presidência da República</h1> ' \
             '<h2>Casa Civil</h2> ' \
             '<h3>Subchefia para Assuntos Jurídicos</h3> ' \
             '</header>'
    footer='<p class="dou">Este texto não substitui o publicado no DOU de 17.1.2018</p></body></html>'
    filename = '{}.html'.format(inspect.stack()[0][3])
    page = re.sub(r' style="[^"]+"', '', requests.get(url).text)
    page = re.sub(r'<span>|</span>', '', page)
    page = re.sub(r'EpgrafeAlt1|Epgrafe', 'epigrafe', page)
    page = re.sub(r'EmentaAlt2|Ementa', 'ementa', page)
    page = re.sub(r'Assinatura1Alt7|Assinatura1', 'presidente', page)
    page = re.sub(r'Assinatura2Alt8|Assinatura2', 'ministro', page)
    soup = BeautifulSoup(page, 'html.parser').find_all(id='conteudoPrincipal')
    caterva = soup[0].div.html.body

    container = caterva.find_all('div')[2]
    print(container)

    with open(filename, 'w') as file:
        file.write(BeautifulSoup(header, 'lxml').prettify())
        file.write(container.prettify())
        file.write(BeautifulSoup(footer, 'lxml').prettify())
        pass
    print('-' * 20)
if __name__ == '__main__':
    #truncus01a()
    #truncus01b()
    #truncus01c()
    #truncus01d()
    #truncus01e()
    truncus01f()
    truncus01g()
    truncus01h()
    truncus01i()
    truncus01j()
    truncus01k()
    truncus01l()
    truncus01l('http://legis.senado.leg.br/legislacao/ListaTextoSigen.action?norma=570696&id=14253788&idBinario=15822433&mime=application/rtf')
    truncus01l(
        'http://legis.senado.leg.br/legislacao/ListaTextoSigen.action?norma=478186&id=14305769&idBinario=15808588&mime=application/rtf')
