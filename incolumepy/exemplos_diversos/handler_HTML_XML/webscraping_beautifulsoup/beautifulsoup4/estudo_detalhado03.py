__author__ = '@britodfbr'
'''https://www.crummy.com/software/BeautifulSoup/bs4/doc/'''

from bs4 import BeautifulSoup

soup = BeautifulSoup(open('../../fhtml/D9255.htm'), 'html.parser')
data = '''<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />
    <meta name='author' value='@britodfbr' />
    <meta name='date' value='20180311' />
    <meta name='timestamp' value='Dom Mar 11 10:12:17 -03 2018' />
    <meta name='epoch' value = '1520773937' />
   <title>
    The Dormouse's story
   </title>
  </head>
  <body>
   <p class="title">
    <b>
     The Dormouse's story
    </b>
   </p>
   <p class="story">
    Once upon a time there were three little sisters; and their names were
    <a class="sister" href="http://example.com/elsie" id="link1"> Elsie </a>,
    <a class="sister" href="http://example.com/lacie" id="link2"> Lacie </a> and
    <a class="sister" href="http://example.com/tillie" id="link3"> Tillie </a>; 
    and they lived at the bottom of a well.
   </p>
   <p class="story">
    ...
   </p>
   <footer>
   
   </footer>
  </body>
 </html>'''


def estudo_detalhado03a():
    '''grava o conteudo de dados no arquivo dormouse.xhtml'''
    with open('../../fhtml/dormouse.xhtml', 'w') as f:
        f.write(data)


def estudo_detalhado03b():
    '''
    imprime todos os links da pagina
    :return:
    '''
    soup = BeautifulSoup(data, 'html.parser')
    for i, link in enumerate(soup.find_all('a')):
        print(i, link.get('href'))


def estudo_detalhado03c():
    '''
    apenas id='link3' no documento html
    :return:
    '''
    soup = BeautifulSoup(data, 'html.parser')
    print(soup.find(id='link3'))


def estudo_detalhado03d():
    '''
    apenas o texto do documento html
    :return:
    '''
    soup = BeautifulSoup(data, 'html.parser')
    print(soup.get_text().strip())


if __name__ == '__main__':
    estudo_detalhado03a()
    estudo_detalhado03b()
    estudo_detalhado03c()
    estudo_detalhado03d()
