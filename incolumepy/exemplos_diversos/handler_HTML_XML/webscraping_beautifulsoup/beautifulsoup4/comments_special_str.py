from bs4 import BeautifulSoup, Comment, CData

def exemplo01():
    markup = "<b><!--Hey, buddy. Want to buy a used parser?--></b>"
    soup = BeautifulSoup(markup,'lxml')
    comment = soup.b.string
    print(type(comment))
    print(soup.b.prettify())
    return True


def exemplo02():
    markup = "<b><!--Hey, buddy. Want to buy a used parser?--></b>"
    soup = BeautifulSoup(markup,'lxml')
    comment = soup.b.string
    cdata = CData("A CDATA block")
    comment.replace_with(cdata)

    print(soup.b.prettify())
    return True


if __name__ == '__main__':
    exemplo01()
    exemplo02()




