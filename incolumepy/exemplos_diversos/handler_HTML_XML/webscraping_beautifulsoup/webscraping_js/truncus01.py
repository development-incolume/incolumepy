__Autor__ = 'britodfbr'
from bs4 import BeautifulSoup
from selenium import webdriver


def truncus01a(filename):
    '''
    Modo
    :param filename:
    :return:
    '''
    with open(filename) as file:
        container = BeautifulSoup(file.read(), 'html.parser')
    result = container.find('p', {'id': 'jstest'})
    print(result.get_text())
    print(result.text)
    print(result.content)


def truncus01b(url="http://localhost:8000/lock.html"):
    driver = webdriver.Chrome()
    driver.get(url)
    source = driver.page_source
    soup = BeautifulSoup(source, 'html.parser')


def truncus01c(url="http://localhost:8000/lock.html"):
    driver = webdriver.Firefox()
    driver.get(url)
    source = driver.page_source
    soup = BeautifulSoup(source, 'html.parser')


def truncus01d(url="http://localhost:8000/lock.html"):
    client = webdriver.PhantomJS()
    client.get(url)
    soup = BeautifulSoup(client.page_source)
'''
def truncus01e(url=''):
    session = dryscrape.Session()
    session.visit(url)
    response = session.body()
    soup = BeautifulSoup(response)
    soup.find(id="intro-text")
    # Result:
    #< p id = "intro-text" > Yay! Supports javascript < / p >
'''
if __name__ == '__main__':
    truncus01a('lock.html')
    #truncus01b()
    #truncus01c('http://localhost:63342/00-incolumepy/incolumepy/testes/webscraping_beautifulsoup/webscraping_js/lock.html?_ijt=g9ae8hbk6qamf86sfnlo3njbg8')
    truncus01c()