from bs4 import BeautifulSoup

html_data = '''
<html>
    <head><title>Uma página qualquer</title></head>
    <body>
        <div id="content">
            <div class="content-title">
                Aqui vai um conteúdo 
                <a class="link" href="http://www.google.com/">qualquer</a>, 
                um texto sem fundamento algum, escrito sem a mínima 
                intenção de despertar qualquer interesse por parte
                do <a class="link-old"> leitor</a>.
            </div>
            <div class="content-author">
                Escrito por um autor qualquer, que não tinha 
                algo melhor para escrever.
            </div>
            <div class="content-data">
                16/03/2013
            </div>
        </div>
        <div class="footer">
            Nenhum direito reservado.
        </div>
    </body>
</html>'''

soup = BeautifulSoup(html_data, 'html.parser')

# <title>Uma página qualquer</title>
print(soup.title)

# Uma página qualquer
print(soup.title.text)

# imprime a div
print(soup.body.div)

# imprime o conteudo do contents
print(soup.body.contents)

print('\nGerador com os elementos child contidos em body\n', '-' * 20)
for x in soup.body.childGenerator():
    print(x, '\n')

print('\n\nAcesso ao recurso pai')
a = soup.body.div.a
print(a)
print(a.parent)

print('\n\n Buscar todos os elementos de determinado tipo')
print(soup.findAll('a'))

print('\n\nBuscar elemento pelo identificador')
print(soup.find(id="content"))

print('\n\nBuscar elementos em uma classe específica')
print(soup.find(attrs={'class': 'footer'}))
print(soup.find(attrs={'class': 'content-data'}))
