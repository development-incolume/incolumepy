from bs4 import BeautifulSoup

conteudo = '''
<tr bgcolor="FFF8DC">
    <td valign="top">25/06/2014 20:37</td>
    <td valign="top">25/06/2014</td>
    <td>
        <a href="Javascript:AbreArquivo('430489');">BROOKFIELD INCORPORAÇÕES S.A.</a>
        <br>
        Disponibilização do Laudo de Avaliação da pretendida oferta pública para a aquisição das
        ações de emissão da Companhia em circulação no mercado
    </td>
</tr>
'''

soup = BeautifulSoup(conteudo, 'html.parser')

print(soup.select('a[href]'))
print(soup.select('a')[0]['href'])
print(soup.select('a[href]')[0].contents)
print(soup('a'))
print(soup.a['href'])
print(soup.select('a')[0]['href'])
print(soup.select('a[href*="AbreArquivo"]')[0]['href'])
print(soup.select('a[href$="Javascript"]'))
# Solução:
print(soup.select('a[href*="AbreArquivo"]')[0]['href'].split("'")[1])
