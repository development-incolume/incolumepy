from bs4 import BeautifulSoup
from os.path import isfile, basename
from inspect import stack

def html2xml01a(filein='../fhtml/D9255.htm'):
    with open(filein) as file:
        content = file.read()
    soup = BeautifulSoup(content, 'html.parser').find('html')
    #print(content)
    print(soup[0])
    count = 0
    while True:
        try:
            if count <= 0:
                filename = '../fxml/{}_{}.xml'.format(basename(filein).split('.')[0], stack()[0][3], count)
            else:
                filename = '../fxml/{}_{}_{:0>2}.xml'.format(basename(filein).split('.')[0], stack()[0][3], count)
            if isfile(filename):
                raise IOError('Arquivo existente: {}'.format(filename))
            break
        except IOError as e:
            print(e)
        except:
            raise
        finally:
            count += 1
    with open(filename, 'w') as file:
#        file.write(content)
        file.write(soup)
        pass

def html2xml01b(filein='../fhtml/D9255.htm'):
    with open(filein) as file:
        content = file.read()
    soup = BeautifulSoup(content, 'html.parser').find('html')
#    print(content)
    print(soup.meta)
#    print(soup)
    count = 0
    while True:
        try:
            if count <= 0:
                filename = '../fxml/{}_{}.xml'.format(basename(filein).split('.')[0], stack()[0][3], count)
            else:
                filename = '../fxml/{}_{}_{:0>2}.xml'.format(basename(filein).split('.')[0], stack()[0][3], count)
            if isfile(filename):
                raise IOError('Arquivo existente: {}'.format(filename))
            break
        except IOError as e:
            print(e)
        except:
            raise
        finally:
            count += 1


if __name__ == '__main__':
#    html2xml01a()
    html2xml01b()
