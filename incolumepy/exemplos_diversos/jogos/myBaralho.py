DEBUG = 0
class Baralho():
    naipes = 'paus copas espadas ouros'.split()
    valores = [str(x) for x in range(1,11)]
    valores[0] = 'A'
    valores.append('V')
    valores.append('Q')
    valores.append('K')

    cartas = []

    def __init__(self):
        if DEBUG: print(self.valores, self.naipes)
        self.cartas=[(v,n) for v in self.valores for n in self.naipes]
        print(len(self.cartas), self.cartas)

    def __str__(self):
        return '<%s de %s>' % (self.valores[self.posicao], self.naipes[self.naipe])

    @classmethod
    def characteres(self):
        for i in range(257):
            print(i, chr(i), end=";")

    @classmethod
    def l_ascii2char(self):
        return [(i, chr(i)) for i in range(257)]


if __name__ == '__main__':
    Baralho()
    #Baralho.characteres()
    print()
    #print(Baralho.l_ascii2char())

    pass