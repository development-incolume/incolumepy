import collections
from random import shuffle


class Baralho:
    Carta = collections.namedtuple('Carta', 'valor naipe')  # class dinamic
    naipes = 'Paus Ouros Copas Espadas'.split()
    valores = 'A 2 3 4 5 6 7 8 9 10 J Q K'.split()

    def __init__(self):
        self.cartas = [self.Carta(v, n) for n in self.naipes for v in self.valores]

    def __len__(self):
        return len(self.cartas)

    def __getitem__(self, item):
        return self.cartas[item]

    def embaralhar(self):
        shuffle(self.cartas)
        return self.cartas

    def ordenar(self, tipo):
        return {
            'naipe': sorted(self.cartas, key=lambda x: (x[1], x[0])),
            'valor': sorted(self.cartas),
            'original': [self.Carta(v, n) for n in self.naipes for v in self.valores]
        }.get(tipo)


if __name__ == '__main__':
    b = Baralho()
    print(len(b))
    print(b.cartas)
    print(b[3])
    print(b.embaralhar())
    print(b.ordenar('naipe'))
    print(b.ordenar('original'))
