'''https://www.youtube.com/watch?v=DbWKAP-48dY'''

import os
import time

try:
    user1 = ''
    point = 5
    erros = 0
    def over(point, erros):
        if not point:
            raise SystemExit
        elif 5<= erros <= 10:
            print('há muitos erros!')
        else:
            raise SystemExit

    while True and user1 != '24':
        user1 = input('Quantas horas tem o dia? ').lower()
        if user1 != '24':
            point -= 1
            erros += 1
        print('Pontuação: %03d, Erros: %03i' % (point, erros))
        over(point, erros)
    #time.sleep(5)
except KeyboardInterrupt as e:
    print('^C')
except:
    raise