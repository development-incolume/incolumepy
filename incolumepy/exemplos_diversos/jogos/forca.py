palavra_secreta = [x.upper() for x in 'MACEIO']
letras_descobertas = ['-' for x in palavra_secreta]

print('\n Jogo da Forca \n')
print('palavra com {:2} letras'.format(len(letras_descobertas)))
print(letras_descobertas)

prosseguir = True

while prosseguir:
    letra = input("\nDigite a letra: ").upper()

    if letra in palavra_secreta:
        letras_descobertas[palavra_secreta.index(letra)] = letra

    print(letras_descobertas)

    if '-' not in letras_descobertas:
        prosseguir = False
print('\n\nParabéns!!!!\n')

if __name__ == '__main__':
    pass
