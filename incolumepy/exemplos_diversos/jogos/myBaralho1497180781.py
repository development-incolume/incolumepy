class Carta():
    naipes = 'Paus Ouros Copas Espadas'.split()
    valores = [str(x) for x in range(1, 11)]
    valores[0] = 'A'
    valores.append('J')
    valores.append('Q')
    valores.append('K')

    def __init__(self, valor, naipe):
        self.naipe = naipe
        self.valor = valor

    def __repr__(self):
        return '<%s de %s>' % (self.valor, self.naipe)

    def __cmp__(self, other):
        # verificar os valores
        if self.valores.index(self.valor) > self.valores.index(other.valor): return 1
        if self.valores.index(self.valor) < self.valores.index(other.valor): return -1

        # se as cartas têm o mesmo naipe...
        if self.naipes.index(self.naipe) > self.naipes.index(other.naipe): return 1
        if self.naipes.index(self.naipe) < self.naipes.index(other.naipe): return -1

        # as posições são iguais... é um empate
        return 0

    def __lt__(self, other):
        if self.__cmp__(other) == -1:
            return True
        return False

    def __eq__(self, other):
        if self.__cmp__(other) == 0:
            return True
        return False

class Baralho():
    def __init__(self):
        self.cartas = [Carta(v, n) for v in Carta.valores for n in Carta.naipes]


if __name__ == '__main__':
    print(Carta.naipes)
    print(Carta.valores)
    c1 = Carta('3', 'Ouros')
    c2 = Carta('4', 'Ouros')
    c3 = Carta('A', 'Ouros')
    c4 = Carta('K', 'Ouros')
    c5 = Carta('K', 'Copas')
    c6 = Carta('K', 'Espadas')
    c7 = Carta('K', 'Paus')
    c8 = Carta('4', 'Paus')

    a = c1
    b = c3
    print(a, b, a.__cmp__(b))

    a = c1
    b = c5
    print(a, b, a.__cmp__(b))

    a = c1
    b = c8
    print(a, b, a.__cmp__(b))

    lc1 = []
    lc1.append(c1)
    lc1.append(c2)
    lc1.append(c3)
    lc1.append(c4)
    lc1.append(c5)
    lc1.append(c6)
    lc1.append(c7)
    lc1.append(c8)
    print('lc1', lc1)
    print(lc1[0].__lt__(lc1[1]))
    print(lc1[0].__lt__(lc1[2]))
    lc1.sort()
    print(lc1)

    print('-'*20)
    b = Baralho()
    sorted(b.cartas)
    print(b.cartas)

