import json
import locale
import logging
import logging.handlers
import os
import platform
import re
from collections import namedtuple
from copy import copy
from datetime import datetime
from incolumepy.utils.files import realfilename
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait

Atos = namedtuple('Ato', 'nome numero ano date origem situacao relacionada link_elem_txt elem_txt')


class Scrapy_camara:
    def __init__(self, ano=1900, origem="executivo",
                 driver='../handler_HTML_XML/drivers/geckodriver', filelog=None, **kwargs):
        self.firefoxbin = os.path.abspath(driver)
        self.firefox = webdriver.Firefox(executable_path=self.firefoxbin)
        self.ano = ano
        self.origem = origem
        self.l_atos = []
        self.collection_atos = []
        self.collection_json = {}
        self.filelog = filelog
        self.filejson = '{}.json'.format(os.path.basename(__file__).split('.')[0])

    @property
    def origem(self):
        return self._origem

    @origem.setter
    def origem(self, value):
        if re.match('executivo', value, re.I):
            self._origem = 'Poder Executivo'
        elif re.match('legislativo', value, re.I):
            self._origem = 'Poder Legislativo'
        else:
            raise ValueError(value)

    def query_camara(self):
        '''
        busca de leis
        :return: return and write a json file
        '''
        self.queryid = self.firefox.window_handles
        self.firefox.get('http://www2.camara.leg.br/atividade-legislativa/legislacao/pesquisa/avancada')
        logging.debug(self.firefox.current_url)
        container = self.firefox.find_element_by_id('expressao')
        container.send_keys('leis')
        container = self.firefox.find_element_by_id('ano')
        container.send_keys(self.ano)
        self.firefox.find_element_by_id('apelido').click()
        self.firefox.find_element_by_id('Ilei').click()
        self.firefox.find_element_by_id('Ileicom').click()
        self.firefox.find_element_by_id('Ideclei').click()
        self.firefox.find_element_by_id('Idecret').click()

        # É preciso passar o elemento para a classe
        origem = Select(self.firefox.find_element_by_name('origensF'))
        # Selecionar a opção pelo texto
        #origem.select_by_visible_text('(Todas)')
        #origem.select_by_visible_text('Poder Legislativo')
        origem.select_by_visible_text(self.origem)

        # É preciso passar o elemento para a classe
        situacao = Select(self.firefox.find_element_by_id('situacao'))
        # Selecionar a opção pelo valor
        situacao.select_by_value('')
        container.send_keys(Keys.ENTER)
        logging.debug('consultas selecionadas')

        print('title: {}'.format(self.firefox.title))
        logging.debug('title: {}'.format(self.firefox.title))
        logging.debug('url: '.format(self.firefox.current_url))
        logging.debug('id: {}'.format(self.queryid))

        # self.firefox.forward()
        print('confirmaçao id: {}'.format(self.firefox.window_handles))

        body = self.firefox.find_element_by_tag_name("body")
        body.send_keys(Keys.CONTROL + 't')

        wait = WebDriverWait(self.firefox, 10)
        wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "Próxima")))

        print(self.firefox.title)
        # assert "busca" in self.firefox.title
        atos = self.firefox.find_elements_by_class_name('titulo')
        #self.atos2list(atos)
        while True:
            try:
                self.firefox.find_element_by_link_text('Próxima').click()
                #self.atos2list(self.firefox.find_elements_by_class_name('titulo'))
            except:
                break

        assert "No results found." not in self.firefox.page_source
        # self.firefox.close()

    def date_conv(self, date):
        '''
                convert date to timestamp
                :param date: ' de 8 de Janeiro de 1900'
                :return: 1900/1/8
        '''
        if platform.system() == 'Linux':
            locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
        else:
            locale.setlocale(locale.LC_ALL, 'pt_BR')

        try:
            return datetime.strptime(date, ' de %d de %B de %Y').strftime('%Y/%m/%d')
        except ValueError:
            return datetime.strptime(date, ' de %dº de %B de %Y').strftime('%Y/%m/%d')

    @staticmethod
    def run():
        a = Scrapy_camara()
        #a.ano = 1948
        #a.filejson='selenium_multaccess03a.json'
        try:
            logging.debug('starting...')
            a.query_camara()
        except (AttributeError) as e:
            logging.error(e)
            raise
            print(e)
        finally:
            a.firefox.quit()
            pass


if __name__ == '__main__':
    pass
    Scrapy_camara.run()
