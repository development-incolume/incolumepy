import os

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait

def esperar_pelo_elemento(firefox):
  return firefox.find_element_by_id('<elemento_id>')

def get_elemento():
    '''
    Nós pedimos para o firefox esperar por 5 segundos até que o resultado
    da função esperar_pelo_elemento seja True. Caso passe  esse  tempo  e
    ele não encontre o elemento, então será gerada uma exception.
    :return:
    '''
    firefoxbin = os.path.abspath('../handler_HTML_XML/drivers/geckodriver')
    firefox = webdriver.Firefox(executable_path=firefoxbin)
    firefox.get('http://google.com.br/')

    elemento = WebDriverWait(firefox, 5).until(esperar_pelo_elemento)

if __name__ == '__main__':
    get_elemento()
