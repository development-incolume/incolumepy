import sys
import re
import os
import locale
import logging
import platform
import logging.handlers
from datetime import datetime
from selenium import webdriver
from collections import namedtuple
from incolumepy.utils.files import realfilename
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait

Atos = namedtuple('Ato', 'nome numero ano date origem situacao relacionada link_elem_txt elem_txt')

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class Scrapy_camara:
    def __init__(self, origem="executivo",
                 driver='../handler_HTML_XML/drivers/geckodriver', **kwargs):
        self.firefoxbin = os.path.abspath(driver)
        self.firefox = webdriver.Firefox(executable_path=self.firefoxbin)
        self.origem = origem
        self.expressao = ''
        self.d_atos = {}
        self.id_clicked = ['nrbLegS', 'apelido', 'Ilei', 'Ileicom', 'Ideclei', 'Idecret']
        for item, value in kwargs.items():
            self.__dict__[item] = value
            logger.debug('atributo {}={}'.format(item, value))
#        logger.info('Objeto instanciado..')
        logger.debug('Objeto instanciado..')

    @property
    def origem(self):
        return self._origem

    @origem.setter
    def origem(self, value):
        if re.match('executivo', value, re.I):
            self._origem = 'Poder Executivo'
        elif re.match('legislativo', value, re.I):
            self._origem = 'Poder Legislativo'
        else:
            self._origem = '(Todas)'


    def query_camara(self):
        '''
        busca de atos legislativos na camara.gov.br
        :return: informaçoes das leis
        '''
        self.queryid = self.firefox.window_handles
        self.firefox.get('http://www2.camara.leg.br/atividade-legislativa/legislacao/pesquisa/avancada')
        logger.debug('url corrente: {}'.format(self.firefox.current_url))
        logger.debug('seleçao dos Termos de consultas')
        container = self.firefox.find_element_by_id('expressao')
        container.send_keys(self.expressao)
        container = self.firefox.find_element_by_id('ano')
        container.send_keys(self.ano)
#        if 'apelido' in self.clicked:
#            self.firefox.find_element_by_id('apelido').click()
#        if 'Ilei' in self.clicked:
#            self.firefox.find_element_by_id('Ilei').click()
#        if 'Ileicom' in self.clicked:
#            self.firefox.find_element_by_id('Ileicom').click()
#        if 'Ideclei' in self.clicked:
#            self.firefox.find_element_by_id('Ideclei').click()
#        if 'Idecret' in self.clicked:
#            self.firefox.find_element_by_id('Idecret').click()

        for item in self.id_clicked:
            self.firefox.find_element_by_id(item).click()

        # É preciso passar o elemento para a classe
        origem = Select(self.firefox.find_element_by_name('origensF'))
        # Selecionar a opção pelo texto
        origem.select_by_visible_text(self.origem)

        # É preciso passar o elemento para a classe
        situacao = Select(self.firefox.find_element_by_id('situacao'))
        # Selecionar a opção pelo valor
        situacao.select_by_value('')
        container.send_keys(Keys.ENTER)
        logger.debug('Termos de consultas selecionadas')
        logger.debug('title: {}'.format(self.firefox.title))
        logger.debug('url: '.format(self.firefox.current_url))
        logger.debug('id: {}'.format(self.queryid))

        logger.debug('confirmaçao id: {}'.format(self.firefox.window_handles))

        logger.debug(self.firefox.title)
        # assert "busca" in self.firefox.title
        while True:
            body = self.firefox.find_element_by_tag_name("body")
            body.send_keys(Keys.CONTROL + 't')

            wait = WebDriverWait(self.firefox, 10)
            wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "Próxima")))
            try:
                atos = self.firefox.find_elements_by_class_name('titulo')
                for ato in atos:
                    logger.debug('adding title: {}'.format(ato.text))
                    self.d_atos['title'] = ato.text
                    logger.debug('adding link: {}'.format(ato.find_element_by_name('a').get_attribute('href')))
                    self.d_atos['link'] = ato.find_element_by_tag_name('a').get_attribute('href')
                self.firefox.find_element_by_link_text('Próxima').click()
            except:
                logger.debug('Fim das paginas da consulta..')
                break
        logger.debug('query_camara>', self.d_atos)
        assert "No results found." not in self.firefox.page_source

    def date_conv(self, date):
        '''
                convert date to timestamp
                :param date: ' "de 8 de Janeiro de 1900" ou "de 1º de abril de 1900"'
                :return: 1900/1/8
        '''
        if platform.system() == 'Linux':
            locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
        else:
            locale.setlocale(locale.LC_ALL, 'pt_BR')

        try:
            return datetime.strptime(date, ' de %d de %B de %Y').strftime('%Y/%m/%d')
        except ValueError:
            return datetime.strptime(date, ' de %dº de %B de %Y').strftime('%Y/%m/%d')

    @staticmethod
    def run():
        logger.debug('Starting..')
        a = Scrapy_camara(ano=1889)
        #a.ano = 1948
        try:
            logger.debug('starting exec ...')
            a.query_camara()
        except (AttributeError) as e:
            logger.error(e)
            raise
        finally:
            a.firefox.quit()
            logger.debug('Finish..')


if __name__ == '__main__':
    pass
    Scrapy_camara.run()
