import random
import string

class Senhas:

    qt_char = min_char = 8

    def setqtchars(self, i):
        if i > self.min_char:
            self.qt_char = int(i)
        else:
            self.qt_char = self.min_char

    def get_pwd(self):
        pwd = ''
        for i in range(self.qt_char):
            pwd += random.choice(string.digits+string.ascii_letters)
        return pwd

    def gen_pass(self, qt_char):
        if qt_char < self.min_char:
            qt_char = self.min_char
            print("A senha nao pode conter menos de {} caracteres.".format(self.min_char))
            print("A senha nao pode conter menos de {:02d} caracteres.".format(self.min_char))
            print("A senha nao pode conter menos de {:>03f} caracteres.".format(self.min_char))
        pwd = ''
        for i in range(qt_char):
            pwd += random.choice(string.digits+string.ascii_letters+string.punctuation)
        return pwd

if __name__ == '__main__':
    s = Senhas()
    s.setqtchars(15)
    print(s.get_pwd())
    s.setqtchars(1)
    print(s.get_pwd())

    print(s.gen_pass(7))
