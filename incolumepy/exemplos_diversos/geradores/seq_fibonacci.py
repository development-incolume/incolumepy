# coding=utf-8
# Python3


class Sequencia:
    class Fibonacci:
        def __init__(self):
            self.seq = [0, 1]

        def __next__(self):
            self.seq.append(sum(self.seq))
            del self.seq[0]
            return self.seq[-1]

    def __iter__(self):
        return Sequencia.Fibonacci()


if __name__ == "__main__":
    fib = iter(Sequencia())
    print(fib)
    print(fib.__next__())

    for i in range(10):
        print(fib.__next__())

    for i in range(100):
        print(next(fib), end=', ')

    for i in range(100):
        print(next(fib))

    for i in range(1000):
        print(next(fib), end=', ')
        print()
        print(fib.__next__())
