class ProgressaoGeometrica:
    pg = []

    def __init__(self, numInicial, razao):
        self.razao = razao
        self.pg.append(numInicial)

    def __iter__(self):
        return self

    def __next__(self):
        self.pg.append(self.pg[-1] * self.razao)
        return self.pg.pop(0)


if __name__ == '__main__':
    pg = ProgressaoGeometrica(-4, 1 / 2)

    for i in range(100):
        print(next(pg))
