# http://www.portugal-a-programar.pt/forums/topic/55341-resolvido-password-aleat%C3%B3ria/
import random

char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
num_caract = int(input("Número de caracteres: "))

def password():
    if (num_caract < 0):
        return "Erro: número negativo"
    elif (num_caract == 0):
        return "Erro: Tem que ter pelo menos 1 caracter."
    else:
        passwd = ""
        while len(passwd) != num_caract:
            passwd = passwd + random.choice(char)
    return "Password: %s" % passwd


if __name__ == '__main__':
    print(password())
