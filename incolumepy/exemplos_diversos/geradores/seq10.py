def deznumeros():
    print ("Parte A")
    for i in range(10):
        print ("Parte B (num %d)" % i)
        yield i
        print ("Parte C (num %d)" % i)
    print ("Parte D")



if __name__ == '__main__':
    sequencia10 = deznumeros()

    print(sequencia10)
    print(dir(sequencia10))
    print(type(sequencia10))

    print(next(sequencia10))
    print(sequencia10.__next__())

    #for i in sequencia10:
    #    print(i)

