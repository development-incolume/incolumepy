import matplotlib.pyplot as plt


def exemplo_01a():
    plt.plot([10, 5, 3, 4, 6, 8])
    plt.title("Muito Fácil")
    plt.show()


def exemplo_01b():
    import numpy as np
    import matplotlib.pyplot as plt

    data1 = [10, 5, 2, 4, 6, 8]
    data2 = [1, 2, 4, 8, 7, 4]
    x = 10 * np.array(range(len(data1)))

    plt.plot(x, data1, 'go')  # green bolinha
    plt.plot(x, data1, 'k:', color='orange')  # linha pontilha orange

    plt.plot(x, data2, 'r^')  # red triangulo
    plt.plot(x, data2, 'k--', color='blue')  # linha tracejada azul

    plt.axis([-10, 60, 0, 11])
    plt.title("Mais incrementado")

    plt.grid(True)
    plt.xlabel("eixo horizontal")
    plt.ylabel("que legal")
    plt.show()


def exemplo_01c():
    import numpy.random as npr
    import matplotlib.pyplot as plt

    x = npr.randn(10000)
    print(x)
    plt.hist(x, 100)
    plt.show()
    pass


if __name__ == '__main__':
    # exemplo_01b()
    exemplo_01c()
