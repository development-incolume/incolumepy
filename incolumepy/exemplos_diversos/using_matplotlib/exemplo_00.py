'''
https://paulovasconcellos.com.br/15-comandos-de-matplotlib-que-talvez-voc%C3%AA-n%C3%A3o-conhe%C3%A7a-17cf88a75119
'''

import matplotlib.pyplot as plt

x = [i for i in range(1, 11)]
y = [i + 10 for i in x]


def plotagem01():
    # plt.style.use()
    plt.rcParams['figure.figsize'] = (11, 7)

    # salvar a imagem
    plt.savefig('nome_da_imagem.png')
    # salvar sem transparencia
    plt.savefig('nome_da_imagem.png', transparent=True)

    # forma 1
    # fig, ax = plt.subplots()
    # ax.plot(x, y)

    # forma 2
    plt.title('SEU TÍTULO LINDO')

    plt.plot(x, y)
    plt.show()


def plotagem02():
    plt.plot(x, y)  # Criando o gráfico
    plt.title('SEU TÍTULO LINDO')  # adicionando o título

    plt.xlabel('NOME DO EIXO X')
    plt.ylabel('NOME DO EIXO Y')

    plt.show()


def plotagem03():
    plt.plot(x, y)  # Criando o gráfico
    plt.title('SEU TÍTULO LINDO')  # adicionando o título

    plt.xlabel('NOME DO EIXO X')
    plt.ylabel('NOME DO EIXO Y')
    # plt.pie(x, y)
    plt.bar(x, y)
    plt.show()


def plotagem04():
    plt.plot(x, y)  # Criando o gráfico
    plt.title('SEU TÍTULO LINDO')  # adicionando o título

    plt.xlabel('NOME DO EIXO X')
    plt.ylabel('NOME DO EIXO Y')

    plt.plot(x, y, color='green')
    plt.scatter(x, y, color='red')
    # plt.legend(loc='best')  # colocando a legenda no melhor lugar
    plt.show()


def plotagem05():
    plt.plot(x, y)  # Criando o gráfico
    plt.title('SEU TÍTULO LINDO')  # adicionando o título

    plt.xlabel('NOME DO EIXO X')
    plt.ylabel('NOME DO EIXO Y')

    plt.plot(x, y, 'b--')
    plt.scatter(x, y, marker="*", color='red')
    plt.show()


if __name__ == '__main__':
    print(x)
    print(y)
    # plotagem01()
    # plotagem02()
    # plotagem03()
    # plotagem04()
    plotagem05()
