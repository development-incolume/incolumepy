import pprint
import json


json_str='''{"resultado":true, "mensagem":"ok", "Contador":2144,\
                "Dados":[{"Id":1, "Data_inscricao":"2017-01-01",\
                          "Texto":"Loremipsum", "Numeracao":26, "Tempo":"25s"},\
                         {"Id":3, "Data_inscricao":"2010-05-02",\
                          "Texto":"LoremipsumLorem", "Numeracao":656, "Tempo":"96s"}]}'''

json_str0 = '''    {<input "id": &quot;Resultados&quot;, "type":&quot;hidden&quot;, "value":&quot;{

    &quot;result&quot;:true,&quot;message&quot;:&quot;ok&quot;,&quot;Contador&quot;:2282,&quot;Dados&quot;:
    [
    {&quot;Id&quot;:1,
    &quot;Data_inscricao&quot;:&quot;2017-01-01&quot;,
    &quot;Texto&quot;:&quot;Loremipsum&quot;,
    &quot;Numeracao&quot;:26,
    &quot;Tempo&quot;:&quot;25s&quot;}, 
    {&quot;Id&quot;:3,
    &quot;Data_inscricao&quot;:&quot;2010-05-02&quot;,
    &quot;Texto&quot;:&quot;LoremipsumLorem&quot;,
    &quot;Numeracao&quot;:656,
    &quot;Tempo&quot;:9}
    ]

    }&quot;/>}'''
def other():
    s = json.loads(json_str)
    pprint.pprint(s)

def other0():
    s = json.loads(json_str0.replace('&quot;', '"').replace('<input ', '').replace('/>','').strip())
    print(s)

def d210837aa(str_json):
    data = json.loads(str_json.strip())
    print(data)
    for i in data.keys():
        print(i)

    for i in data.items():
        print(i)

    for i in data.values():
        print(i)

def d210837ab(str_json):
    data = json.loads(str_json.strip())
    for element in data['Dados']:
        print(element)

def d210837ac(str_json, *, keyword='Numeracao', value=26):
    data = json.loads(str_json.strip())
    for elements in data['Dados']:
        for element in elements.keys():
            if element == keyword:
                if elements[element] == value:
                    return 'Data de Inscrição: {}'.format(elements['Data_inscricao'])

if __name__ == '__main__':

    other()
    #other0()
    d210837aa(json_str)
    print('-'*20)
    d210837ab(json_str)
    print('-' * 20)
    print(d210837ac(json_str))
    print(d210837ac(json_str, keyword='Id', value=3))