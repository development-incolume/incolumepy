import json

str_json='''[  
   {  
      "Id":1,
      "Data_inscricao":"2017-01-01",
      "Texto":"Loremipsum",
      "Numeracao":26,
      "Tempo":"25s"
   },
   {  
      "Id":3,
      "Data_inscricao":"2010-05-02",
      "Texto":"LoremipsumLorem",
      "Numeracao":656,
      "Tempo":9
   }
]'''

data = json.loads(str_json)

def d210837(jsonfile='file.json'):
    with open(jsonfile) as file:
        data = json.loads(file.read())
    return data

def search_insc(number = 656):
    for d in data:
        for value in d.values():
            if value == number:
                return 'A data de inscrição é: {}'.format(d['Data_inscricao'])

def search_insc1(number = 656):
    # Restringindo para que a busca se limite à chave 'Numeração'
    for d in data:
        for key in d.keys():
            if key == 'Numeracao':
                if d[key] == number:
                    return 'A data de inscrição é: %s' % d['Data_inscricao']

def search_insc2(*, keyword='Numeracao', value=26):
    for d in data:
        for key in d.keys():
            if key == keyword:
                if d[key] == value:
                    return 'A data de inscrição é: %s' % d['Data_inscricao']

if __name__ == '__main__':
    data1 = d210837()
    print((data))
    print(data1)
    print('-')

    for i in data:
        print(i)

    print(search_insc(656))
    print(search_insc1(26))
    print(search_insc2(keyword='Numeracao', value=656))
    print(search_insc2(keyword='Id', value=3))
    print(search_insc2(keyword='Tempo', value='25s'))
