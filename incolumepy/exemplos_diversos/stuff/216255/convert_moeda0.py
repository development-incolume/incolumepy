class Convert:

    def __init__(self):
        self.usd = 3.3
        self.brl = 1


    def to_dollar(self, real=0):
        return real/self.__usd

    @property
    def usd(self):
        return self.__usd

    @usd.setter
    def usd(self, usd):
        self.__usd = usd

    @property
    def brl(self):
        return self.__brl

    @brl.setter
    def brl(self, brl):
        self.__brl = brl

def exemples():
    c = Convert()
    reais = 16.67

    print(c.__dict__)

    # Utilizando a @ property us:

    print('valor atual do dollar: ', c.usd)

    # Utilizando o metodo to_dollar para conversão:

    print('Conversão de', reais, 'reais em dollar: ', c.to_dollar(reais))

    # Utilizando o us.setter:
    c.usd = 3.10
    print('Valor Atualizado do dollar: ', c.usd)

    # Nova conversão após a atualização com o setter:

    print('Conversão de', reais, 'reais em dollar: ', c.to_dollar(reais))


if __name__ == '__main__':
    exemples()
