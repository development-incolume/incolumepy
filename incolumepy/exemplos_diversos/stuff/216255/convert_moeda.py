class Convert:

    def __init__(self):
        self.us = 3.3

    def to_dollar(self, real=0):
        return real/self.__us

    @property
    def us(self):
        return self.__us

    @us.setter
    def us(self, us):
        self.__us = us

def exemples():
    c = Convert()
    reais = 16.67

    # Utilizando a @ property us:

    print('valor atual do dollar: ', c.us)

    # Utilizando o metodo to_dollar para conversão:

    print('Conversão de', reais, 'reais em dollar: ', c.to_dollar(reais))

    # Utilizando o us.setter:
    c.us = 3.10
    print('Valor Atualizado do dollar: ', c.us)

    # Nova conversão após a atualização com o setter:

    print('Conversão de', reais, 'reais em dollar: ', c.to_dollar(reais))


if __name__ == '__main__':
    exemples()
