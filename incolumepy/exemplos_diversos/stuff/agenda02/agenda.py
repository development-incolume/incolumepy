agenda = list()
keys = ['nome', 'telefone', 'celular', 'email']

def add_contato(nome, telefone, celular, email):
    global agenda
    agenda.append([nome, telefone, celular, email])


def search_contato(**kwargs):
    if len(kwargs) > 1:
        raise KeyError('Selecione apenas um campo: %s' % keys)
    if ''.join(list(kwargs.keys())).lower() not in keys:
        raise KeyError('Campo de busca "< %s >" invalido!' % ''.join(kwargs.keys()).lower())
    for campo, valor in kwargs.items():
        for index, registro in enumerate(agenda):
            if registro[0] == campo.lower():
                return index
        print(campo, valor)





def list_agenda():
    if agenda:
        for i in agenda:
            print('Nome: %s, Telefone: %s, Celular: %s, E-mail: %s' % tuple(i))
    else:
        print('Sem itens na agenda!!')


def menu():
    itens = ['sair', 'inserir', 'listar', 'pesquisar', 'editar', 'excluir']
    while True:
        print('===== MENU =====')
        for i, item in enumerate(itens):
            print("%s - %s" % (i, item))
        op = int(input('Selecione uma opção: '))
        if not op:
            exit(0)
        elif op == 1:
            add_contato(input('Nome: '), input('Telefone: '), input('celular: '), input('Email: '))
        elif op == 2:
            list_agenda()
        elif op == 3:
            #search_contato(nome='12', telefone='')
            #search_contato(nomes='12')
            #search_contato(Telefone='12')
            search_contato(EMAIL='12')
        elif op == 4:
            print('4')
        elif op == 5:
            print(5)
        else:
            menu()


if __name__ == '__main__':
    menu()
