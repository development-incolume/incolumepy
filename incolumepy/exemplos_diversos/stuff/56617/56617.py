import urllib.request
import urllib.parse
from platform import python_version, version


if python_version() < '3.0':
    from BeautifulSoup import BeautifulSoup
else:
    from bs4 import BeautifulSoup

def funcao(url, ano):
    parametros = urllib.parse.urlencode({'ano': ano, 'entrar': 'Buscar'})
    print(type(parametros))
    print((parametros))
    print(bytes(parametros))
    html = urllib.request.urlopen(url, bytes(parametros)).read()
    return html

url  = "http://venus.maringa.pr.gov.br/arquivos/orgao_oficial/paginar_orgao_oficial_ano.php"
ano  = 2015
html = funcao(url, ano)
soup = BeautifulSoup(html)

for link in soup.findAll('a'):
    print(link.get('href'))