def validarIP(ip):
    list = ip.split('.')
    for i in list:
        if int(i) > 255:
            return False
    return True


def main():
    l = ['255.255.255.255', '192.168.0.12', '1.1.1.1', '256.1.2.3',
         '172.16.0.292', '10.0.0.1', '111.111.111.111']
    for i in l:
        print('%15s: %s' % (i, validarIP(i)))


if __name__ == '__main__':
    main()
