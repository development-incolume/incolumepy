# Latitude: -15.7801, Longitude: -47.9292
import csv
import random

from rstr import rstr

from incolumepy.geradores.timestamp import gen_timestamp, gen_random_timestamp

'''


'''
header_format = 'id, ssid, bssid, dateTime, latitude, longitude'


def makeLinha0():
    num = '1234567890'
    return '%s, %s, %s, %s, %06.4f, %06.4f' % (
        rstr(num[:-1], 2),
        rstr(num, 3),
        rstr(num, 4),
        next(gen_random_timestamp()),
        (random.randint(1, 50) + random.random()),
        (random.randint(1, 50) + random.random())
    )


def makeLinha():
    num = '1234567890'
    return '{}, {}, {}, {}, {:06.4f}, {:06.4f}'.format(
        rstr(num[:-1], 2),
        rstr(num, 3),
        rstr(num, 4),
        next(gen_timestamp()),
        (random.randint(1, 50) + random.random()),
        (random.randint(1, 50) + random.random())
    )


def makeCVSFile0(filename, qlinhas=100):
    data = []
    data.append(header_format.split(','))
    for i in range(qlinhas):
        data.append(makeLinha().split(','))

    with open(filename, 'w') as file:
        writer = csv.writer(file, delimiter=',',
                            quotechar=',', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(data)


def makeCVSFile1(filename, qlinhas=100):
    data = []
    data.append(header_format.split(','))
    for i in range(qlinhas):
        data.append(makeLinha().split(','))

    with open(filename, 'w') as file:
        writer = csv.writer(file, quoting=csv.QUOTE_MINIMAL)
        writer.writerow(data)


def makeCVSFile2(filename, qlinhas=100):
    data = []
    data.append(header_format.split(','))
    for i in range(qlinhas):
        data.append(makeLinha().split(','))

    with open(filename, 'w') as file:
        writer = csv.writer(file)
        writer.writerow(data)


def makeCVSFile3(filename, qlinhas=100):
    '''
    Funcionando como esperado
    :param filename:
    :param qlinhas:
    :return:
    '''
    data = []
    data.append(header_format.split(','))
    for i in range(qlinhas):
        data.append(makeLinha().split(','))

    with open(filename, 'w') as file:
        writer = csv.writer(file)
        for line in data:
            writer.writerow(line)


def makeCVSFile(filename, qlinhas=100):
    with open(filename, 'w') as file:
        writer = csv.writer(file)
        for i in range(qlinhas):
            if i == 0:
                writer.writerow(header_format.split(','))
            writer.writerow(makeLinha().split(','))


def main():
    for i in range(1, 10):
        print(makeLinha())
        makeCVSFile('./cvs/file{:02d}.csv'.format(i))


if __name__ == '__main__':
    main()
    makeCVSFile('./cvs/file1.cvs')
