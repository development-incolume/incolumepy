from os import listdir
from os.path import isfile, join

# Diretorio
diretorio = "./cvs"

# Recupera lista de ficheiros CSV em um diretorio
ficheiros = [f for f in listdir(diretorio) if (isfile(join(diretorio, f)) and f.endswith('.csv'))]
print('a>>', ficheiros)
print(listdir(diretorio))
ficheiros = [join(diretorio, f) for f in listdir(diretorio) if (f.endswith('.csv'))]

# Abre ficheiro de saida...
saida = open("saida.csv", "a")

# Para cada ficheiro...
for f in ficheiros:

    # Abra o ficheiro
    csv = open(f)

    # Ignora o header do CSV
    next(csv)

    # Calcula student...
    student = 1

    # Para cada uma das demais linhas no ficheiro...
    for linha in csv:
        linha = linha.rstrip() + ';' + str(student) + '\n';
        saida.write(linha)

    # Fecha ficheiro CSV de entrada
    csv.close()

# Fecha ficheiro CSV de saida
saida.close()
