def trocar(vals, posX, posY):
    temp = vals[posX]
    vals[posX] = vals[posY]
    vals[posY] = temp
    return None


def rearranjar_tupla(tupla):
    return (-tupla[2], tupla[1], tupla[0])


def ordenar(valores):
    tamanho = len(valores) - 1
    troquei = True
    while troquei:
        troquei = False
        for i in range(tamanho):
            if rearranjar_tupla(valores[i]) > rearranjar_tupla(valores[i + 1]):
                trocar(valores, i, i + 1)
                troquei = True
    tamanho -= 1
    return valores


lista= [('Ana', 30, 6.69), ('João', 25, 6.11), ('Pedro', 30, 6.69),    ('Maria', 28, 5.45), ('Thiago', 40, 5.45), ('Raquel', 26, 10.0), ('Alberto', 26, 10.0), ('Rachel', 26, 10.0)]

x=ordenar(lista)
print(x)
