from bs4 import BeautifulSoup
import requests
import datetime

url = "https://dumps.wikimedia.org/other/pageviews/2018/2018-04/"
proxies = {
    'http': 'http://10.1.101.101:8080',
    'https': 'http://10.1.101.101:8080',
    'ftp': 'http://10.1.101.101:8080',

}

page_html = requests.get(url, proxies=proxies).content

soup = BeautifulSoup(page_html, "lxml")
#print(soup)
print(datetime.datetime.now().strftime('%Y%m%d'))
links = [(url+link['href'])
          for link in soup.find_all('a')
          if "pageviews-" in link['href']
          and datetime.datetime.now().strftime('%Y%m%d-%H0000') in link['href']
          ]

for link in links: print(link)

