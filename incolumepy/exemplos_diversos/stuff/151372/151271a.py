class Foo:
    '''
    classdocs
    '''
    _id = None
    name = None

    def __init__(self, *attrs):
        if attrs:
            for key, value in attrs.items():
                setattr(self, key, value)

    def __getattr__(self, key):
        if (hasattr(self, key)):
            return key
        else:
            return None

    def __setattr__(self, k, v):
        if hasattr(self, k):
            super().__setattr__(k, v)
            return True
        else:
            return False


if __name__ == '__main__':
    import doctest

    doctest.testmod()
