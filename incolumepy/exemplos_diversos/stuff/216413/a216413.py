from collections import defaultdict
'''
Identificar elementos repetidos em lista com Python
'''

def a216413():
    # Define a lista de volares:
    lista = [3, 2, 4, 7, 3, 8, 2, 3, 8, 1]

    # Define o objeto que armazenará os índices de cada elemento:
    keys = defaultdict(list);

    # Percorre todos os elementos da lista:
    for key, value in enumerate(lista):

        # Adiciona o índice do valor na lista de índices:
        keys[value].append(key)

    # Exibe o resultado:
    for value in keys:
        if len(keys[value]) > 1:
            print(value, keys[value])

def b216413(lista = [3, 2, 4, 7, 3, 8, 2, 3, 8, 1]):
    keys = defaultdict(list)
    for key, value in enumerate(lista):
        keys[value].append(key)
    #print(keys.items())

    # imprimir todos
    for i, j in keys.items():
        print(i, j)

if __name__ == '__main__':
    a216413()
    print()
    b216413()