def a217042():
    '''

    :return:
    '''
    frase = 'Oi, eu sou Goku e estou indo para a minha casa'

    remover_palavras = ['a', 'e']
    lista_frase = frase.split()

    result = [palavra for palavra in lista_frase if palavra.lower() not in remover_palavras]

    retorno = ' '.join(result)
    print(retorno)
    return retorno


if __name__ == '__main__':
    a217042()
