def metodo01():
    numeros = []
    maior = -9999999999
    for i in range(20):
        numeros.append(int(input("Número %sº: " % (i + 1))))
        if numeros[i] > maior:
            maior = numeros[i]
    return maior


def metodo02():
    numeros = [int(input("Número %sº: " % (i + 1))) for i in range(20)]
    return max(numeros)


def metodo03():
    return max([int(input("Número %sº: " % (i + 1))) for i in range(20)])


if __name__ == '__main__':
    # print(metodo01())
    print(metodo02())
