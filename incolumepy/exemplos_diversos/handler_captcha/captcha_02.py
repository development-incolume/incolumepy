from captcha_solver import CaptchaSolver
from bs4 import BeautifulSoup
import requests

url = 'http://anp.gov.br/preco/prc/Resumo_Por_Estado_Index.asp'
proxies ={
    'http': 'http://10.1.101.101:8080',
    'https': 'http://10.1.101.101:8080',
    'ftp': 'http://10.1.101.101:8080',
}
source = requests.get(url, proxies=proxies)
print(source)

soup = BeautifulSoup(source.content, 'html.parser')
print(soup)

captcha_img = soup.select('img[src*="imagem.asp"]')
print(captcha_img)

#solver = CaptchaSolver('browser')
#raw_data = open('captcha.png', 'rb').read()
#print(solver.solve_captcha(raw_data))