from captcha_solver import CaptchaSolver
from selenium import webdriver
import os


url = 'http://anp.gov.br/preco/prc/Resumo_Por_Estado_Index.asp'


driver='../handler_HTML_XML/drivers/geckodriver'
firefoxbin = os.path.abspath(driver)
firefox = webdriver.Firefox(executable_path=firefoxbin)

firefox.get(url)
print(firefox.context())

solver = CaptchaSolver('browser')
raw_data = open('captcha.png', 'rb').read()
print(solver.solve_captcha(raw_data))