from incolumepy.random_fake.cpf_notVerify import gen_cpf
from incolumepy.testes.OObj.herança.funcionario import Funcionario
from incolumepy.testes.OObj.herança.pessoa import Pessoa


class Criança(Funcionario):
    def __init__(self, nomecompleto, cpf, domain):
        super().__init__(nomecompleto, cpf, domain)


class Cidadão(Pessoa):
    def __init__(self, nomecompleto):
        super(Cidadão, self).__init__(nomecompleto)
        super().__init__(nomecompleto)


if __name__ == '__main__':
    b = Cidadão('Ada Catarina Santos Brito')
    c = Criança('Ada Catarina Santos Brito', next(gen_cpf()), 'incolume.com.br')
    print(b)
    print(c)
    print(type(b))
    print(type(c))
