# https://www.youtube.com/watch?v=sPiWg5jSoZI

class Structure:
    __slots__ = []

    def __init__(self, *args):
        for name, val in zip(self.__slots__, args):
            setattr(self, name, val)


class Point(Structure):
    ''' class Point, metaprogramação com slots'''
    __slots__ = ['x', 'y', 'z']

    def __repr__(self):
        return '({}, {}, {})'.format(*self.__slots__)

    def __str__(self):
        return 'x: {}, y: {}, z: {}'.format(self.x, self.y, self.z)


if __name__ == '__main__':
    p = Point(1, 2, 3)
    print(p.__doc__)
    print(p)
    print('{!r}'.format(p))
    print('{!s}'.format(p))
