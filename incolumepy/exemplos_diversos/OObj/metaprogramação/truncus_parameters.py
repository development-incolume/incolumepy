from inspect import Parameter, Signature

_fields = ['name', 'shares', 'price', ]
params = [Parameter(fname, Parameter.POSITIONAL_OR_KEYWORD) for fname in _fields]

print(params)

sig = Signature(params)
print(sig)


def catrefa(*args, **kwargs):
    bound = sig.bind(*args, **kwargs)
    for name, val in bound.arguments.items():
        print(name, val)


catrefa(1, 2, 3)
catrefa(1, price=4.1, shares=2)
