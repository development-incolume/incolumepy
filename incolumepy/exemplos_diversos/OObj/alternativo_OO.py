# !/bin/env python
'''

'''

import sys
class Alternativo:
    """ There is no switch or case in Python ... because you can
    do better by using its OO capabilities and a dictionary. """
    takeaction = dict()
    def __init__(self):
        self.set_actions()

    def doblue(self):
        return ("Céu e o oceano são azuis")

    def dogreen(self):
        return ("Vegetação é verde")

    def doyellow(self):
        return ("Ouro é amarelo")

    def redflag(self):
        return ("Vermelho é a cor do Fogo")

    # Uses the get method to allow us to specify a default
    def errhandler(self, msg="Esta entrada não foi reconhecida!"):
        return (msg)

    # set up a dictionary of actions
    def set_actions(self):
        self.takeaction["azul"] = self.doblue
        self.takeaction['verde'] = self.dogreen
        self.takeaction['amarelo'] = self.doyellow
        self.takeaction['vermelho'] = self.redflag
        self.takeaction['exit'] = sys.exit
    #Não funcionou ainda com restrição de acesso.
    #def __getattr__(self, item):
    #    if item in self.__dict__:
    #        return self.__dict__[item]
    #    else:
    #        raise AttributeError('not %s' % item)

    #def __setattr__(self, key, value):
    #    if key in self.__dict__:
    #        __dict__[key] = value
    #    else:
    #        raise ArithmeticError('Restrict', key)

    def Main(self):
        while True:
            colour = input("Escolha entre as opções \nazul, \nverde, \namarelo, \nvermelho ou \nexit: ")
            print('%s\n' % self.takeaction.get(colour, self.errhandler)())


if __name__ == '__main__':
    DEBUG = 0
    if DEBUG: a = Alternativo().Main
    b = Alternativo()
    print(b.__dict__)
    try:
        b.cor = print
    except:
        print(sys.exc_info())
    Alternativo().Main()
    pass
