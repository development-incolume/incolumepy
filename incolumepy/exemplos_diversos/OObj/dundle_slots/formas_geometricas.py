class FormaGeometrica:
    __slots__ = ()

    def get_area(self):
        raise NotImplementedError(NotImplemented)

    def get_perimetro(self):
        raise NotImplementedError
