# -*- coding: utf-8 -*-
import math
from unittest import TestCase

from incolumepy.testes.OObj.dundle_slots.circulo import Circulo
from incolumepy.testes.OObj.dundle_slots.formas_geometricas import FormaGeometrica
from incolumepy.testes.OObj.dundle_slots.quadrado import Quadrado
from incolumepy.testes.OObj.dundle_slots.retangulo import Retangulo


# O nome da classe deve iniciar com a palavra Test
class TestFormaGeometrica(TestCase):
    # Serve para incializar variavei que usaremos
    # globalmente nos testes
    def setUp(self):
        TestCase.setUp(self)
        self.fig = FormaGeometrica()

    def test_slots(self):
        self.assertIsNotNone(self.fig.__slots__)
        self.assertTupleEqual((), self.fig.__slots__)
        with self.assertRaises(AttributeError):
            str(self.fig.__dict__)

    # Retorna uma NotImplementedError
    # O nome do metodo deve comecar com test
    def test_get_area(self):
        self.assertRaises(NotImplementedError, self.fig.get_area)

    # Retorna uma NotImplementedError
    # O nome do metodo deve comecar com test
    def test_get_perimetro(self):
        self.assertRaises(NotImplementedError, self.fig.get_perimetro)


class TestCirculo(TestCase):
    def setUp(self):
        TestCase.setUp(self)
        self.fig = Circulo()

    def test_herança(self):
        self.assertIsInstance(self.fig, FormaGeometrica)

    def test_slots(self):
        with self.assertRaises(AttributeError):
            str(self.fig.__dict__)
        self.assertIsNotNone(self.fig.__slots__)

    def test_slots_(self):
        self.assertIn('raio', self.fig.__slots__)

    def test_encapsulamento(self):
        with self.assertRaises(AttributeError):
            self.fig.value = 0
        self.assertIsNot('value', self.fig.__slots__)

    def test_get_area(self):
        # Utilizamos a formula diretamente por conveniencia
        # já que math.pi e double e sendo assim, possui
        # muitas casas decimais
        self.fig.raio = 2
        area = math.pi * self.fig.raio ** 2
        self.assertEqual(self.fig.get_area(), area)

        self.fig.raio = 7.0
        area = math.pi * self.fig.raio ** 2
        self.assertEqual(self.fig.get_area(), area)

    def test_get_perimetro(self):
        self.fig.raio = 2
        perimetro = 2 * math.pi * self.fig.raio
        self.assertEqual(self.fig.get_perimetro(), perimetro)

        self.fig.raio = 7.0
        perimetro = 2 * math.pi * self.fig.raio
        self.assertEqual(self.fig.get_perimetro(), perimetro)


class TestQuadrado(TestCase):
    def setUp(self):
        TestCase.setUp(self)
        self.fig = Quadrado()

    def test_herança(self):
        self.assertTrue(isinstance(self.fig, FormaGeometrica))
        self.assertIsInstance(self.fig, FormaGeometrica)

    def test_slots(self):
        with self.assertRaises(AttributeError):
            str(self.fig.__dict__)
        self.assertIsNotNone(self.fig.__slots__)

    def test_slots_(self):
        self.assertIn('lado', self.fig.__slots__)

    def test_encapsulamento(self):
        with self.assertRaises(AttributeError):
            self.fig.value = 0
        self.assertIsNot('value', self.fig.__slots__)

    def test_get_area(self):
        # Verificamos se o resultado é o esperado
        # de acordo com a formula de area do quadrado
        self.fig.lado = 2
        self.assertEqual(self.fig.get_area(), 4)
        self.fig.lado = 7.0
        self.assertEqual(self.fig.get_area(), 49.0)

    def test_get_perimetro(self):
        self.fig.lado = 2
        self.assertEqual(self.fig.get_perimetro(), 8)
        self.fig.lado = 7.0
        self.assertEqual(self.fig.get_perimetro(), 28.0)


class TestRetangulo(TestCase):
    def setUp(self):
        TestCase.setUp(self)
        self.fig = Retangulo()

    def test_herança(self):
        self.assertTrue(isinstance(self.fig, FormaGeometrica))
        self.assertIsInstance(self.fig, FormaGeometrica)

    def test_slots(self):
        with self.assertRaises(AttributeError):
            str(self.fig.__dict__)
        self.assertIsNotNone(self.fig.__slots__)

    def test_slots_(self):
        self.assertIn('base', self.fig.__slots__)
        self.assertIn('altura', self.fig.__slots__)

    def test_encapsulamento(self):
        with self.assertRaises(AttributeError):
            self.fig.value = 0
        self.assertIsNot('value', self.fig.__slots__)

    def test_get_area_int(self):
        # Verificamos se o resultado é o esperado
        # de acordo com a formula de area do quadrado
        self.fig.base = 2
        self.altura = 2
        self.assertEqual(self.fig.get_area(), 4.0)

    def test_get_area_float(self):
        self.fig.base = 7.0
        self.fig.altura = 2.0
        self.assertEqual(self.fig.get_area(), 14.0)

    def test_get_area_str(self):
        self.fig.base = '7.0'
        self.fig.altura = '2.0'
        self.assertEqual(self.fig.get_area(), 14.0)

    def test_get_perimetro_int(self):
        self.fig.base = 2
        self.altura = 2
        self.assertEqual(self.fig.get_perimetro(), 10)

    def test_get_perimetro_int(self):
        self.fig.base = 7.0
        self.fig.altura = 2.0
        self.assertEqual(self.fig.get_perimetro(), 18.0)

    def test_get_perimetro_str(self):
        self.fig.base = '7.0'
        self.fig.altura = '2.0'
        self.assertEqual(self.fig.get_perimetro(), 18.0)
