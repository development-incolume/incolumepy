import math

from incolumepy.testes.OObj.dundle_slots.formas_geometricas import FormaGeometrica


class Circulo(FormaGeometrica):
    __slots__ = ("__raio")

    def __init__(self):
        super()
        self.__raio = None

    def get_perimetro(self):
        return 2 * math.pi * self.raio

    def get_area(self):
        return math.pi * self.raio ** 2

    @property
    def raio(self):
        return self.__raio

    @raio.setter
    def raio(self, x):
        self.__raio = x


if __name__ == '__main__':
    c = Circulo()
    c.raio = 2  # ok
    c.lado = 2  # AttributeError
