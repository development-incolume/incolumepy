#Python3
import abc

class IMamifero(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def __str__(self):
        raise NotImplementedError('users must define __str__ to use this base class')

    @abc.abstractclassmethod
    def andar(self, x):
        raise NotImplementedError('users must define __str__ to use this base class')


if __name__ == '__main__':
    m = IMamifero()
