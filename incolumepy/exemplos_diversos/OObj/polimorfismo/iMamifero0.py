#Python2
import abc

class Mamifero(object):
    '''Interface Mamifero'''
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __str__(self):
        raise NotImplementedError('users must define __str__ to use this base class')

    @abc.abstractclassmethod
    def __repr__(self):
        raise NotImplementedError("Class %s doesn't implement __repr__" % (self.__class__.__name__))

if __name__ == '__main__':
    #m = Mamifero('Ricardo', 84, 1.78)
    m = Mamifero()
    #print(m)

