from incolumepy.testes.OObj.polimorfismo.iMamifero import IMamifero

class Mamifero(IMamifero):
    def __init__(self, nome, peso, altura):
        super(Mamifero, self).__init__()
        self.nome = nome
        self.peso = peso
        self. altura = altura

    def __str__(self):
        return '(%(nome)s, %(peso)s, %(altura)s)' % (self.__dict__)

    def andar(self, x):
        self.x += 1


if __name__ == '__main__':
    m = Mamifero('rato', .05, .05)
    print(m)