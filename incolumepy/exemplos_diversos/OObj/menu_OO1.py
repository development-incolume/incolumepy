import sys
from time import sleep


class MenuOO:
    itens_menu = ['Sair', 'Adição', 'Subtração',
                  'Multiplicação', 'Divisão', 'Resto',
                  'Exponenciação', 'Radiciação']
    takeaction = dict()
    sair_options = ['0', 'sair', 's', 'exit', 'e']

    def __init__(self):
        self.set_actions()

    def aritmetica(self, op, x, y):
        try:
            x, y = int(x), int(y)

            return {
                '+': x + y,
                '-': x - y,
                '*': x * y,
                '/': x // y,
                '%': x % y,
                '^': x ** y,
                'v': x
            }.get(op)
        except:
            print(sys.exc_info())

    def set_actions(self):
        self.takeaction['0'] = sys.exit
        self.takeaction['1'] = self.aritmetica('+', input('x: '), input('y: '))
        self.takeaction['2'] = self.aritmetica('-', input('x: '), input('y: '))
        self.takeaction['3'] = self.aritmetica('*', input('x: '), input('y: '))
        self.takeaction['4'] = self.aritmetica('/', input('x: '), input('y: '))
        self.takeaction['5'] = self.aritmetica('%', input('x: '), input('y: '))
        self.takeaction['6'] = self.aritmetica('^', input('x: '), input('y: '))
        self.takeaction['7'] = self.aritmetica('v', input('x: '), input('y: '))

    def menu(self):
        while True:
            option = ''
            print('======= Menu Calculadora ========')
            for i, item in enumerate(self.itens_menu):
                print('{:3} - {}'.format(i, item))
            option = input("Escolha entre as opções acima: ")
            print('>>>>', option)
            print('>>>>', type(option))
            print('%s\n' % self.takeaction.get(option,
                                               "\n>>>> Esta entrada não foi reconhecida!"))
            sleep(2)

    @classmethod
    def main(cls):
        menuoo = MenuOO()
        menuoo.menu()


if __name__ == '__main__':
    MenuOO.main()
