import jsonpickle

from incolumepy.testes.OObj.convert_jsonpickle.foo import Foo


def func00():
    # Transformando objeto em jsonpickle
    foo = Foo()
    jfoo = jsonpickle.encode(foo)
    # Recriando o objeto python a partir da string JSON:
    foo2 = jsonpickle.decode(jfoo)
    # Executando o objeto (a classe) recriado:
    print(foo2.test())


def func01():
    foo = Foo()
    jfoo = jsonpickle.encode(foo)
    print(type(jfoo), jfoo)
    with open('obj2json.json', 'w') as jsonfile:
        jsonfile.write(jfoo)


def func02():
    with open('obj2json.json') as jsonfile:
        jfoo = jsonfile.read()
    foo2 = jsonpickle.decode(jfoo)

    # Executando o objeto (a classe) recriado:
    print(foo2.test())


def class2json(obj, filename):
    try:
        with open(filename, 'w') as file:
            file.write(jsonpickle.encode(obj))
        return True
    except:
        return False


def json2class(filename):
    try:
        with open(filename) as file:
            return jsonpickle.decode(file.read())
    except:
        return False


if __name__ == '__main__':
    func01()
    func02()
