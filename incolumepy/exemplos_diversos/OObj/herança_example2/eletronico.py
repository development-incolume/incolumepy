from incolumepy.testes.OObj.herança_example2.iEletronico import IEletronico


class Eletronico(IEletronico):
    def __init__(self, name, model):
        super(Eletronico, self).__init__(name)
        self.model = model

    def status(self):
        return '%(name)s[%(model)s]: %(stat)s' % (self.__dict__)

    def __str__(self):
        return '%(name)s[%(model)s]' % (self.__dict__)


if __name__ == '__main__':
    e = Eletronico('aspirador', 'asp-123')
    print(e)
    print(e.ligaDesliga())
    print(e.ligado_stat())
    print(e.ligaDesliga())
    print(e.status())
