from collections import namedtuple

from incolumepy.testes.OObj.herança_example2.eletronico import Eletronico
from incolumepy.testes.OObj.herança_example2.som import Som
from incolumepy.testes.OObj.herança_example2.tv import Tv
from incolumepy.testes.OObj.polimorfismo.canino import Canino
from incolumepy.testes.OObj.polimorfismo.mamifero import Mamifero


class Pertences:
    def __init__(self):
        self.pertences = list()

    def addPertences(self, *args):
        try:
            for i in args:
                if not (isinstance(i, Eletronico) or isinstance(i, Mamifero)):
                    raise AssertionError('Somente Objetos da Class Eletronico e Mamifero')
                self.pertences.append(i)
            return True
        except AssertionError as e:
            raise TypeError(e)
        except:
            raise
        return False


if __name__ == '__main__':
    Person = namedtuple('Person', 'name')
    bob = Person('Eliana')

    pertences = Pertences()
    lesse = Canino('Lesse', 3, .8)
    tv1 = Tv('lg', 32)
    tv2 = Tv('lg', 50)
    som = Som('Sony')
    pertences.addPertences(tv1, tv2)
    pertences.addPertences(lesse)
    pertences.addPertences(bob)
    print(pertences.__dict__)
