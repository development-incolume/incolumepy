import abc


class IPessoa(object, metaclass=abc.ABCMeta):
    def __init__(self, nomecompleto):
        self.nomecompleto = nomecompleto
        self.nome = nomecompleto.split(' ')[0]
        self.sobrenome = nomecompleto.split(' ')[-1]

    def fullname(self):
        return '%(nome)s %(sobrenome)s' % self.__dict__

    @abc.abstractclassmethod
    def __str__(self):
        raise NotImplementedError(NotImplemented)


if __name__ == '__main__':
    p = IPessoa()
