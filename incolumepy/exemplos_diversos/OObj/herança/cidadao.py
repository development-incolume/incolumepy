import os
import sys

sys.path.append(os.path.dirname(__file__))

from pessoa import Pessoa


class Cidadao(Pessoa):
    pass


if __name__ == '__main__':
    c = Cidadao('Ana Gabriela dos Santos Brito')
    print(c.__dict__)
