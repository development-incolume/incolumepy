class Retangulo:
    def __init__(self, altura, largura):
        self.altura = altura
        self.largura = largura
        self.area = altura * largura


class Quadrado(Retangulo):
    def __init__(self, largura):
        super(Quadrado, self).__init__(largura, largura)


if __name__ == '__main__':
    q = Quadrado(3)
    print(q.area)
