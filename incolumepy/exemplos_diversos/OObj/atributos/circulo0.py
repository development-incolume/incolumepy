from incolumepy.geometria.figura_geometrica import FiguraGeometrica


class Circulo(FiguraGeometrica):
    __dict__ = {'__raio': None}

    def __init__(self, **kwargs):
        super()
        if 'raio' in kwargs:
            setattr(self, 'raio', kwargs['raio'])

    def __setattr__(self, key, value):
        if key != 'raio':
            raise AttributeError('Atributo {} não permitido.'.format(key))
        super().__setattr__(key, value)


if __name__ == '__main__':
    b = Circulo()
    print(b.__dict__)
    b.raio = 9

    print(b.__dict__)
    b.lado = 9
