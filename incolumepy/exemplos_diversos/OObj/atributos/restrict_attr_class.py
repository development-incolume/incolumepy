class One:
    '''
    >>> o = One()
    >>> o.__dict__
    {'y': 0, 'x': 0}
    >>> o.x = 1
    >>> o.y = 2
    >>> o.x
    1
    >>> o.y
    2
    >>> o.z = 1
    Traceback (most recent call last):
      File "/home/brito/Documentos/pycharm-community-2017.1.4/helpers/pycharm/docrunner.py", line 140, in __run
        compileflags, 1), test.globs)
      File "<doctest One[6]>", line 1, in <module>
        o.z = 1
      File "/home/brito/projetos/00-incolumepy/incolumepy/testes/OObj/atributos/restrict_attr_class.py", line 22, in __setattr__
        raise AttributeError('Attributo {} não permitido'.format(key))
    AttributeError: Attributo z não permitido
    '''
    __dict__ = {'x': 0, 'y': 0}

    def __init__(self):
        self.x, self.y = 0, 0

    def __setattr__(self, key, value):
        if key not in self.__dict__:
            raise AttributeError('Attributo {} não permitido'.format(key))
        self.__dict__[key] = value

    def __getattr__(self, item):
        return self.__dict__[item]


if __name__ == '__main__':
    import doctest

    doctest.testmod()
