import math


class Circulo():
    '''
    >>> c = Circulo()
    >>> c.__dict__
    {'raio': None}
    >>> c.raio = 2
    >>> c.lado = 2
    Traceback (most recent call last):
      File "/usr/lib/python3.5/doctest.py", line 1321, in __run
        compileflags, 1), test.globs)
      File "<doctest __main__.Circulo[3]>", line 1, in <module>
        c.lado = 2
      File "/home/brito/projetos/00-incolumepy/incolumepy/testes/OObj/atributos/restrict_attr_class0.py", line 22, in __setattr__
        raise AttributeError("Não pode criar atributo '{}' para esta classe".format(key))
    AttributeError: Não pode criar atributo 'lado' para esta classe
    '''
    __dict__ = {'raio': None}

    def get_perimetro(self):
        return 2 * math.pi * self.raio

    def get_area(self):
        return math.pi * self.raio ** 2

    def __setattr__(self, key, value):
        if key not in self.__dict__:
            raise AttributeError("Não pode criar atributo '{}' para esta classe".format(key))
        self.__dict__[key] = value

    def __getattr__(self, item):
        return self.__dict__[item]


if __name__ == '__main__':
    import doctest

    doctest.testmod()
