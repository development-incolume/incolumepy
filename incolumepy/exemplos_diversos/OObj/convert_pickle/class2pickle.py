import pickle


class Foo():
    def test(self):
        return 'bar'


def func():
    foo = Foo()
    with open('foo.pickle', 'wb') as f:
        pickle.dump(foo, f, pickle.HIGHEST_PROTOCOL)

    # Lendo do disco para a memoria (Desserializando):

    with open('foo.pickle', 'rb') as f:
        foo2 = pickle.load(f)

    # Executando a classe desserializada:

    foo2.test()
    'bar'


func()
