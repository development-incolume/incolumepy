from contextlib import ContextDecorator


class mycontext(ContextDecorator):
    def __enter__(self):
        print('Starting')
        return self

    def __exit__(self, *exc):
        print('Finishing\n')
        return False


@mycontext()
def funct():
    print('The bit in the middle')


funct()
# Starting
# The bit in the middle
# Finishing

with mycontext():
    print('The bit in the middle')
# Starting
# The bit in the middle
# Finishing
