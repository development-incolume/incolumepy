import time
from functools import wraps

def time_it(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print('{} gastou {:5.4f} ms'.format(func.__name__, 1000 * (end-start)))
        return result
    return wrapper


def rgb_blue():
    '''rgb blue function'''
    return 'blue'


rgb_blue = time_it(rgb_blue)


# @time_it
def rgb_red():
    ''' rgb red function '''
    return 'red'


@time_it
def calc_square(numbers):
    result = []
    for number in numbers:
        result.append(number*number)
    return result


@time_it
def calc_cube(numbers):
    result = []
    for number in numbers:
        result.append(number*number*number)
    return result


if __name__ == '__main__':
    array = range(1, 100000)
    out_square = calc_square(array)
    out_cube = calc_cube(array)
    print(rgb_blue())
    print(rgb_red())
    print(rgb_red().__doc__)
