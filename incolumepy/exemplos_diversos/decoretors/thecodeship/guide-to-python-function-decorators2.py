def p_decorate(func):
   def func_wrapper(name):
       return "<p>{0}</p>".format(func(name))
   return func_wrapper

def strong_decorate(func):
    def func_wrapper(name):
        return "<strong>{0}</strong>".format(func(name))
    return func_wrapper

def div_decorate(func):
    def func_wrapper(name):
        return "<div>{0}</div>".format(func(name))
    return func_wrapper


def get_text(name):
   return "lorem ipsum, {0} dolor sit amet".format(name)

get_text = div_decorate(p_decorate(strong_decorate(get_text)))

@div_decorate
@p_decorate
@strong_decorate
def get_text0(name):
   return "lorem ipsum, {0} dolor sit amet".format(name)

def tag_decorete(func):
    def func_wrapper(name):
        return '<{1}>{0}</{1}>'.format(func(name), tag)
    return func_wrapper

@tag_decorete('p')
def get_text1(nome):
    return "lorem ipsum, {0} dolor sit amet".format(nome)


if __name__ == '__main__':
    print(get_text('Ricardo'))
    print(get_text0("John"))
    print(get_text1("Ricardo Brito"))
