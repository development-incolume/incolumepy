from functools import wraps


def tags(tag_name):
    def tags_decorator(func):
        @wraps(func)
        def func_wrapper(name):
            return "<{0}>{1}</{0}>".format(tag_name, func(name))

        return func_wrapper

    return tags_decorator


@tags("p")
def get_text(name):
    """returns some text"""
    return "Hello " + name


if __name__ == '__main__':
    assert get_text.__name__ == "get_text", "Ops: falhou a verificação de nome"  # get_text
    assert get_text.__doc__ == 'returns some text', "Ops: falhou na captura da docstring"  # returns some text
    assert get_text.__module__ == '__main__', "Ops: falhou na verificação do módulo"  # __main__
