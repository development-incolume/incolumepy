
def greet01(name):
    """Assign functions to variables"""
    return "hello "+name

def execute01():
    greet_someone = greet01
    return (greet_someone("John"))
    # Outputs: hello John



def greet02(name):
    '''Define functions inside other functions'''
    def get_message():
        return "Hello "

    result = get_message()+name
    return result

def execute02():
    print(greet02("John"))
    # Outputs: Hello John



def greet03(name):
    '''Functions can be passed as parameters to other functions'''
    return "Hello " + name

def call_func(func):
    other_name = "John"
    return func(other_name)

def execute03():
    return (call_func(greet03))
    # Outputs: Hello John



def compose_greet_func04():
    '''
Functions can return other functions
In other words, functions generating other functions.
    '''
    def get_message():
        return "Hello there!"

    return get_message


def execute04():
    greet = compose_greet_func04()
    print(greet())
    # Outputs: Hello there!



def compose_greet_func05(name):
    '''
    Inner functions have access to the enclosing scope

    More commonly known as a closure. A very powerful pattern that we will come across while building decorators.
    Another thing to note, Python only allows read access to the outer scope and not assignment.
    Notice how we modified the example above to read a "name" argument from the enclosing scope of the inner
    function and return the new function.
    :param name:
    :return:
    '''
    def get_message():
        return "Hello there "+name+"!"

    return get_message

def execute05():
    greet = compose_greet_func05("John")
    print(greet())
    # Outputs: Hello there John!


def get_text(name):
   return "lorem ipsum, {0} dolor sit amet".format(name)


def p_decorate(func):
   def func_wrapper(name):
       return "<p>{0}</p>".format(func(name))
   return func_wrapper


def execute05():
    my_get_text = p_decorate(get_text)
    return (my_get_text("John"))
    # <p>Outputs lorem ipsum, John dolor sit amet</p>


def execute06(get_text=get_text):
    get_text = p_decorate(get_text)

    print(get_text("John"))
    # Outputs lorem ipsum, John dolor sit amet


def p_decorate07(func):
   def func_wrapper(name):
       return "<p>{0}</p>".format(func(name))
   return func_wrapper

@p_decorate07
def get_text(name):
   return "lorem ipsum, {0} dolor sit amet".format(name)

def execute07():
    print(get_text("John"))
    # Outputs <p>lorem ipsum, John dolor sit amet</p>


def p_decorate08(func):
   def func_wrapper(name):
       return "<p>{0}</p>".format(func(name))
   return func_wrapper

def strong_decorate08(func):
    def func_wrapper(name):
        return "<strong>{0}</strong>".format(func(name))
    return func_wrapper

def div_decorate08(func):
    def func_wrapper(name):
        return "<div>{0}</div>".format(func(name))
    return func_wrapper


def execute08():
    get_text = div_decorate08(p_decorate08(strong_decorate08(get_text)))
    print(get_text("John"))

def main0() :
    for i in ['execute{:0>2}'.format(x) for x in range(1, 3)]:
        print(f'''==========\n{i}\n{eval(i).__doc__}\n----------\n{eval(i)()}''')


def main():
    for i in ['execute{:0>2}'.format(x) for x in range(1, 10)]:
        print('='*20)
        print(i)
        try:
            if eval(i).__doc__:
                print(eval(i).__doc__)
        except NameError as e:
            print('-'*20)
            print(e)
        else:
            print('-'*20)
            print(eval(i)())
        finally:
            print()
if __name__ == '__main__':
    main()
