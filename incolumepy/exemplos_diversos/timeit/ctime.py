def f1():
    return list(range(10))

def f2():
    return [x for x in range(10)]

def f3():
    lst = []
    i = 0
    while i < 10:
        lst.append(i)
        i += 1
    return lst

#O cálculo:

from timeit import timeit

v1 = timeit('f1()', 'from __main__ import f1', number=100)
v2 = timeit('f2()', 'from __main__ import f2', number=100)
v3 = timeit('f3()', 'from __main__ import f3', number=100)

print(v1, v2, v3)
