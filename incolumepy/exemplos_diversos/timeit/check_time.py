from timeit import timeit

v1 = timeit('[chr(x) for x in range(1,256)]', '', number=100)
print(v1)

v2 = timeit('[*map(chr,[x for x in range(1,256)])]', '', number=100)
print(v2)

v3 = timeit('list(map(chr,[x for x in range(1,256)]))', '', number=100)
print(v3)
