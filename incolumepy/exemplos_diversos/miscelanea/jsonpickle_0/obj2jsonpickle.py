from incolumepy.testes.OObj.convert_jsonpickle.class2json import class2json
from incolumepy.testes.OObj.dundle_slots.circulo import Circulo

circulo = Circulo()
circulo.raio = 10
print(circulo.get_area())
class2json(circulo, 'circulo.json')
