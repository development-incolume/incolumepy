#--*-- Coding: utf8 --*--
'''
only Windows
'''
from distutils.core import setup
import py2exe

setup(
    windows=[{'script': 'strange.py'}],
    options={
        'py2exe':
        {
            'includes': ['BeautifulSoup'],
        }
    }
)