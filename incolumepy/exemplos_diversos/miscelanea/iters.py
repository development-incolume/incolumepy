#-*- coding: utf-8 -*-

import platform

if platform.python_version() >= '3.0':
    xrange = range
    raw_input = input

class Squares:
    '''
    Exemplo do livro digital 'aprendendo Python', p.331
    '''
    def __init__(self, start, stop):
        self.value = start -1
        self.stop = stop
        self.next = self.__next__

    def __iter__(self):
        return self

    def __next__(self):
        if self.value == self.stop:
            raise StopIteration
        self.value += 1
        return self.value **2


class Newsquares(Squares):
    '''
    '''
    def __init__(self, start, stop):
        self.value = start -1
        self.start = start
        self.stop = stop
        self.lista = [self.square(x) for x in xrange(start,stop)]
        self.next = self.__next__

    def __iter__(self):
        return self

    def __next__(self):
        self.value += 1
        try:
            return self.lista[self.value]
        except IndexError:
            print('Não há mais elementos')
        finally:
            self.value = self.start -1
            exit(0)
            
    def square(self,n=1):
        return n ** 2

    def __str__(self):
        return str(self.__repr__())

    def __repr__(self):
        return self.lista
    

if __name__ == '__main__':
    l = []
    for i in Squares(1,5):
        l += [i]
    print ('#1: ',l)

    x = Squares(1,10)
    print ('#1.1: ',type(x))
    print()
    l = []
    for i in x:
        l.append(i)

    print ('#2: ',l),

    print ('\nx: ', x)

    for i in x:
        print('#3: ', i)

    n = Newsquares(1,6)
    print(type(n))
    print(n.lista)
    print ('#1: ', n.square())
    print ('#2: ', n.square(2))
    print ('#3: ', n.square(3))
    print ('Next #1: ',n.next())
    print (n.__next__())
    print (n.next())
    print (n.next())
    print (n.next())
    print (n.next())
    for i in n:
        print (n.next(), sep=',')

