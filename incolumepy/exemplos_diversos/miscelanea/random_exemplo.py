'''
exemplos disponíves em:
https://docs.python.org/3/library/random.html
'''

import random
import bisect
import itertools

print(random.random())                      # Random float x, 0.0 <= x < 1.0
print(random.uniform(1, 10))                # Random float x, 1.0 <= x < 10.0
print(random.randrange(10))                 # Integer from 0 to 9
print(random.randrange(0, 101, 2))          # Even integer from 0 to 100
print(random.choice('abcdefghij'))          # Single random element

items = [1, 2, 3, 4, 5, 6, 7]
random.shuffle(items)               # mistura aleatoria dos itens
print(items)

print(random.sample([1, 2, 3, 4, 5],  3))   # Three samples without replacement
print(random.sample(items,  2))   # seleciona 2 itens aleatorios

weighted_choices = [('Red', 3), ('Blue', 2), ('Yellow', 1), ('Green', 4)]

for key, value in weighted_choices:
    print(key, value)

population = [val for val, cnt in weighted_choices for i in range(cnt)]
print(population)

print(random.choice(population))

choices, weights = zip(*weighted_choices)
cumdist = list(itertools.accumulate(weights))
print(cumdist)            # [3, 3+2, 3+2+1, 3+2+1+4]
x = random.random() * cumdist[-1]
print(choices[bisect.bisect(cumdist, x)])
