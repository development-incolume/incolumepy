import operator

class Language(object):
    def __init__(self,year,name):
        self.year = year
        self.name = name

    def __repr__(self):
        return ' - '.join(map(str,(self.name,self.year)))

if __name__ == '__main__':
    java = Language(1995,'Java')
    python = Language(1991,'Python')
    ruby = Language(1995,'Ruby')
    haskell = Language(1990,'Haskell')
    javascript = Language(1995,'Javascript')
    self = Language(1986,'Self')
    perl = Language(1987,'Perl')
    matlab = Language(1990,'Matlab')
    ada = Language(1982, 'Ada')
    csharp = Language(2001,'C#') 
    cobol = Language(1959, 'Cobol')
    algol = Language(1958, 'Algol')
    lisp = Language(1958, 'Lisp')
    c = Language(1983,'C')
    cpp = Language(1998,'C++')
    assembly = Language(1950,'Assembly')

    languages = [java,python,ruby,haskell,javascript,self,perl, matlab, ada, cobol, algol, csharp, c, cpp, assembly]
    
    print(languages)
    languages.sort(key=operator.attrgetter('year'))
    print(languages)
    #Apenas Python 2.5 agora!
    languages.sort(key=operator.attrgetter('year','name'))
    print(languages)
    languages.sort(key=operator.attrgetter('name'))
    print(languages)
    languages.sort(key=operator.attrgetter('name', 'year'))
    print(languages)
