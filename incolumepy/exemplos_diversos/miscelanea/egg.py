#python module egg.py in egg

class EggClass(object):

    def cook(self):
        print ('Cooking ...')
#And you use it like this

import egg
my_egg = egg.EggClass()
egg.cook()
#But you want the cook method does cooking differently, why not just inherit EggClass and override the cook method like this

class MySuperEgg(EggClass):

    def cook(self):
        print ('Special cooking manner ...')
#So you can use it like this

egg = MySuperEgg()
egg.cook()
#Instead of inherit the class, you can also use a dirty hack to replace the original method like this

def cook(self, time):
    print ('Special cooking for {} minutes ...'.format(time))

# ah... a dirty hack to override the original cook method
EggClass.cook = cook
egg = EggClass()
egg.cook(10)
