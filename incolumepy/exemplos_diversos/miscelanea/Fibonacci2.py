from functools import lru_cache
from timeit import timeit


def fibonacci_ruim(n, *, DEBUG=True):
    if DEBUG: print('fibonacci_ruim(%s)' % n)
    if n <= 2:
        return 1
    return fibonacci_ruim(n - 1) + fibonacci_ruim(n - 2)


@lru_cache(maxsize=None)
def fibonacci(n, *, DEBUG=True):
    if DEBUG: print('fibonacci(%s)' % n)
    if n <= 2:
        return 1
    return fibonacci(n - 1) + fibonacci(n - 2)


if __name__ == '__main__':
    print(fibonacci_ruim(10, DEBUG=True))
    print(fibonacci(10))

    print('ruim: ', timeit('fibonacci_ruim(10, DEBUG=False)', 'from __main__ import fibonacci_ruim', number=1))
    print('cache: ', timeit('fibonacci(10, DEBUG=False)', 'from __main__ import fibonacci', number=1))
