from collections import defaultdict


def f228645a(n):
    n = int(n)
    notas = [100.00, 50.00, 20.00, 10.00, 5.00, 2.00, 1.00]
    print("R$ %d:" % n)
    for x in notas:
        print("%i nota(s) de R$ %.2f" % ((n / x), x))
        n %= x


def f228645(n):
    n = int(n)
    notas = [100, 50, 20, 10, 5, 2, 1]
    print("R$ %d:" % n)
    for x in notas:
        print("%i nota(s) de R$ %d,00" % ((n / x), x))
        n %= x


def decompor(valor):
    notas = [100, 50, 20, 10, 5, 2, 1]
    valor = int(valor)
    result = {}
    print(f"decompor: R$ {valor:.2f}")
    for nota in notas:
        if nota not in result:
            result[nota] = 0
        result[nota] += valor // nota
        print("{:1.0f} nota(s) de R$ {:3.2f}".format(result[nota], nota))
        valor %= nota
    return result


f228645(576)
print()
d = decompor(188.00)
print(d)
