from pprint import pprint

class Conta:
    def __init__(self, nconta='001', cliente='', senha='123', saldo=50.0):
        self.nconta = nconta
        self.cliente = cliente
        self.senha = senha
        self.saldo = saldo

    def saldoC(self):
        return self.saldo

    def saqueC(self, saque):
        if saque <= self.saldo:
            self.saldo -= saque
            print ("Você Sacou R$ %10.2f seu saldo é R$ %10.2f \n" %(saque, self.saldo))
        else:
            print ("Saldo Insuficiente, Seu saldo atual é R$ %10.2f . Saque não realizado" %self.saldo)

    def depositoC(self, deposito):
        self.saldo += deposito
        print ("Seu Novo Saldo é: R$ %10.2f" %self.saldo)

    def __str__(self):
        return "Conta: %s \nCliente: %s \nSaldo: %10.2f " % (self.nconta, self.cliente, self.saldo)

    def __repr__(self):
        return "{'Conta': '%s', 'Cliente': '%s', 'Saldo': %10.2f}" % (self.nconta, self.cliente, self.saldo)



class Banco:
    def __init__(self, **kwargs):
        self.contas = kwargs['contas']
        self.nome = kwargs['nome']
        self.gerentes = {}


    def add_gerente(self, **kwargs):
        self.gerentes[kwargs['nome_gerente']] = kwargs['senha_gerente']


    def criarconta(self, **kwargs):
        if (kwargs['nconta'] in self.contas):
            print('Conta já exite')
            return False
        else:
            if kwargs:
                self.contas['nconta'] = Conta(kwargs['nconta'], kwargs['cliente'], kwargs['senha'], kwargs['saldo'])
            return True


    def validacao(self, nconta, senha):
        try:
            if senha == (self.contas[nconta]).senha:
                return True
            elif senha != (self.contas[nconta]).senha:
                print ("Conta ou Senha Invalida")
                return False
            else:
                print ("Voce não é cliente. Procure a agencia mais proxima e venha usufruir de nossos serviços\n")
                return False
        except:
            print ("Invalido")

    def informe_conta_senha(self):
        nconta = input('Nº conta: ')
        senha = input('senha: ')
        if self.validacao(nconta,senha):
            return self.contas[nconta]
        return False

    #def __repr__(self):
    #    pass


    @staticmethod
    def menu(this):
        '''

        :return:
        '''
        while 1:
            print ('''
            Digite as opções do Menu para:
            1 - Saldo
            2 - Saque
            3 - Deposito"
            4 - Criar nova conta
            5 - Exibir contas
            6 - Exibir Gerentes
            0 - Sair do aplicativo \n
            ''')
            menu = input("=> ")
            if menu == '1':
                print('Saldo')
                conta = this.informe_conta_senha()
                print(conta.saldoC())
            elif menu == '2':
                print('Saque')
                conta = this.informe_conta_senha()
                conta.saqueC(float(input('Informe o valor: ')))
            elif menu == '3':
                print('Deposito')
                conta = this.informe_conta_senha()
                conta.depositoC(float(input('Informe o valor: ')))
            elif menu == '4':
                print('Nova conta')
                nconta=input('Informe o Nº da conta: ')
                this.criarconta(nconta,
                                cliente=input('Informe o nome do Cliente:'),
                                senha=input('Informe a senha: '),
                                saldo=input('Infome o saldo inicial: '))

            elif menu == '5':
                pprint(this.contas)
            elif menu == '6':
                print('Gerentes: ')
                for item in this.gerentes.keys():
                    print(item,)

            elif menu == '0':
                print('Bye')
                exit(0)



if __name__ == '__main__':
    debug = 0
    conta1 = Conta(cliente='Ana Brito',nconta='1010',senha='0000')

    if debug: print(conta1)
    banco1 = Banco(nome='Incolume', contas={conta1.nconta:conta1})
    if debug: print(banco1.contas)
    if debug: print('senha:',(banco1.contas['1010']).senha)
    if debug: print('senha:','0000' == (banco1.contas['1010']).senha)
    if debug: print('senha:','0001' == (banco1.contas['1010']).senha)
    if debug: print(banco1.validacao(nconta='1010', senha='1010'))

    if debug: banco1.criarconta()
    banco1.criarconta(nconta='1100',cliente='Eliana Brito',senha='321',saldo=450.)
    conta2 = Conta(cliente='Ada',nconta='1011',senha='0000')

    if debug: print(banco1.contas)
    if debug: print(banco1)
    #banco1.menu()
    if debug: print(conta2)
    if debug: conta2.depositoC(100)
    if debug: print(conta2)
    if debug: conta2.saqueC(30)
    if debug: conta2.saqueC(90)
    if debug: conta2.saqueC(50)
    if debug: print(conta2)
    if debug: conta2.saqueC(30)
    if debug: print(conta2)
    if debug: print(conta2.saldoC())
    if debug: print(banco1.contas)
    if debug: print(banco1.nome)

    banco1.add_gerente(nome_gerente='Ricardo Brito', senha_gerente='1234')
    banco1.add_gerente(nome_gerente='Ricardo Nascimento', senha_gerente='1234')
    if debug: print(banco1.gerentes)
    Banco.menu(banco1)
