# https://docs.python.org/3.6/tutorial/controlflow.html

def f(ham: str, eggs: str = 'eggs') -> str:
    print("Annotations:", f.__annotations__)
    print("Arguments:", ham, eggs)
    return ham + ' and ' + eggs


f('teste')


def chest(a: int, b: float) -> float:
    print('Annotations: {}'.format(chest.__annotations__))
    print('Argumentos: ', a, b)


print()
chest('a', 'b')
