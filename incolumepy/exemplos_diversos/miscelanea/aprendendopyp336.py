# -*- coding: utf-8 -*-

class Life:
    def __init__(self, name='desconhecido'):
        print ('Olá %s' % name)
        self.name = name
    def __del__(self):
        print ('tchau %s' % self.name)


if __name__ == '__main__':
    instancia = Life('Brian')
    print (instancia)
    instancia = 'José'
    print (instancia)


