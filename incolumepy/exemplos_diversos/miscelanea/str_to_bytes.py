#Coding: UTF-8
lista = list()
str1 = b'hello world'
str2 = bytes("hello world", encoding="UTF-8")
str3 = u'hello world'
str4 = 'hello world'.encode(encoding='utf-8')
str5 = ('hello world')

mystring = 'alô mundo'

b1 = bytes(mystring, 'utf-8')
b2 = mystring.encode('utf-8')
b3 = bytes(mystring, encoding="UTF-8")
b4 = bytes(mystring, encoding="utf-8")

lista.append(str1)
lista.append(str2)
lista.append(str3)
lista.append(str4)
lista.append(str5)

lista2 = [b1, b2, b3, b4]


for j in lista:
    for i in lista:
        print("%s == %s: %s" % (j,i,(j==i)))
    print('--')

for i in lista2:
    for j in lista2:
        print("%s == %s: %s" % (i,j,(i==j)))
    print('--')
