#! /usr/bin/env python
# -*- coding:utf-8 -*-

import pprint
from platform import python_version
if python_version() >= '3.0':
    xrange = range
    raw_input = input

class Conta:
    def __init__(self, nconta='001', cliente='', senha='123', saldo='50.0'):
        self.nconta = nconta
        self.cliente = cliente
        self.senha = senha
        self.saldo = saldo
    
    def saldoC(saldo):
        print ("\nSeu saldo é R$ %10.2f" %saldo)
    
    def saqueC(saque, saldo):
        if saque <= saldo:
            saldo = saldo - saque
            print ("Você Sacou R$ %10.2f seu saldo é R$ %10.2f \n" %(saque,saldo))
        else:
            print ("Saldo Insuficiente, Seu saldo atual é R$ %10.2f . Insira um novo valor" %saldo)
    
    def depositoC(deposito,saldo):
        saldo=saldo + deposito
        print ("Seu Novo Saldo é: R$ %10.2f" %saldo)

    def __str__(self):
        return "Conta: %s \nCliente: %s \nSaldo: %10.2f " % (self.nconta, self.cliente, self.saldo)
    
    def __repr__(self):
        return "{'Conta': '%s', 'Cliente': '%s', 'Saldo': %10.2f}" % (self.nconta, self.cliente, self.saldo)
        

class Banco:
    contas = []
        

    def criarconta(self, nconta='001',cliente='ana',senha='123',saldo=0):
        self.contas.append(Conta(nconta,cliente,senha,saldo))
        
        
    def validacao(self, nome, senha):
        try:
            if nome == "luan" and senha==1234:
                return True
            elif nome== "luan" and senha != 1234:
                print ("Senha Invalida")
    
            else:
                print ("Voce nao e cliente, procure a agencia mais proxima e venha fazer parte da nossa familia\n")
        except:
            print ("Invalido")

    def validacao(self):
        nconta=raw_input("Digite a sua conta: ")
        senha=int(raw_input("Digite a sua senha (Apenas Numero): "))
        print (nconta, senha)


    @classmethod
    def menu(self):
        #nome=raw_input("Digite seu nome:")
        #senha=int(raw_input("Digite a sua senha (Apenas Numero): "))
        #self.validacao(nome,senha)
        #
        #if self.validacao(nome,senha) == True:
        #    print "Olá %s , Bem Vindo ao Banco \n" %nome
        
        while 1:
            print ('Digite as opções do Menu para: ')
            print ("1 - Saldo")
            print ("2 - Saque")
            print ("3 - Deposito")
            print ("4 - Criar nova conta")
            print ("5 - Exibir contas")
            print ("0 - Sair do aplicativo \n")
        
            menu = raw_input("")
            try:
                if menu == '1':
                    saldoC(saldo)
            
                elif menu == '2':
                    saque=float(raw_input("Digite o Valor do Saque: R$ "))
                    saqueC(saque, saldo)
            
                elif menu == '3':
                    deposito=float(raw_input("Digite o Valor do Deposito: R$"))
                    depositoC(deposito,saldo)
            
                elif menu == '4':
                    print ('Criar nova Conta')
                    self.criarconta(raw_input('Informe o numero da nova conta: '), 
                        raw_input('Informe o nome do cliente: '),
                        raw_input('Informe a senha: '),
                        raw_input('Informe o saldo inicial: '))
                    pass

                elif menu == '5':
                    print ('\n'*1)
                    pprint.pprint(self.contas)
                    print ('\n'*3)
                   
                elif menu == '0':
                    print ("Operação Encerrada")
                    break
                else:
                    print ("\nOperação Invalida\n")
            except ValueError as e:
                pass
            except:
                print ("Operação Invalida")
            
    #Como faço para atribuir o valor da função na variável global? vlw!!!


if __name__ == '__main__':
    debug = 0
    c = Banco()
    if debug: print (c.contas)
    c.criarconta('001','ana','123',150)
    c.criarconta('002','ada','123',100)
    c.criarconta('003','adao','123',100)
    c.criarconta('004','ricardo','123',100)
    if debug: print (c.contas)
    #b = Banco().menu()
    contas = Banco().contas
    print (contas)
    print (contas[0])
    print (contas[0].nconta)
    print (contas[0].cliente)
    print (contas[0].saldo)
    print (contas[0].senha)
    v = Banco().validacao
    print (v)
    v()

    Banco.menu()
