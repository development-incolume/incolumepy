#!/bin/usr/env Python
#--*-- Coding: UTF-8 --*--

from functools import lru_cache


class Fibonacci:
    '''
    Fibonacci
    - recursão
    - memoization
    '''


    def __init__(self):
        self.cache_fibonacci = dict()

    @classmethod
    def fibonacci0(cls, n):
        '''
        metodo recursivo para retorno de elemento fibonacci
        :param n: inteiro
        :return: inteiro
        '''
        if n == 1:
            return 1
        elif n == 2:
            return 1
        elif n > 2:
            return cls.fibonacci0((n - 1)) + cls.fibonacci0((n - 2))

    @staticmethod
    def fibonacci1(n):
        '''
        metodo recursivo com implementação de cache
        :param n: inteiro
        :return: inteiro
        '''
        value = 0
        if n in Fibonacci.cache_fibonacci:
            return Fibonacci.cache_fibonacci[n]
        if n == 1:
            value = 1
        elif n == 2:
            value = 1
        elif n > 2:
            value = Fibonacci.fibonacci1(n - 1) + Fibonacci.fibonacci1(n - 2)

        Fibonacci.cache_fibonacci[n] = value
        return value







    @lru_cache(maxsize = 1000)
    def fibonacci2(self, n):
        '''
        metodo recursivo com implementação de LRU (Least Recently Used)  cache
        :param n: int
        :return: int
        '''
        value = 0
        if n == 1:
            value = 1
        elif n == 2:
            value = 1
        elif n > 2:
            value = self.fibonacci2(n - 1) + self.fibonacci2(n - 2)
        return value

    @lru_cache(maxsize = 1000)
    def fibonacci3(self, n):
        '''
        metodo recursivo com implementação de LRU (Least Recently Used) cache e tratamento de erros
        :param n: int
        :return: int
        '''
        if type(n) != int:
            raise TypeError('n deve ser um "número" inteiro e positivo')
        if n < 0:
            raise ValueError('n deve ser um inteiro positivo')

        value = 0

        if n == 1:
            value = 1
        elif n == 2:
            value = 1
        elif n > 2:
            value = self.fibonacci3((n - 1)) + self.fibonacci3((n - 2))
        return value

    def razao_fibonacci(self,n):
        '''
        Calcula a razão entre elementos fibonacci até o limite de recursão do hardware
        :param n: int
        :return: fload
        '''
        return (self.fibonacci3(n+1)/self.fibonacci3(n))


    def fibonacci4(pos=5):
        '''
        Calcula a sequência fibonacci até a posição (pos) indicada, retorna um primos como os elementos.
        :param pos: int
        :return: list
        '''
        fibonacci = []
        pos = int(pos)

        if pos <= 0:
            raise ValueError('"pos" deverá ser um inteiro positivo.')

        for i in range(pos):
           if (i <= 1 ):
               fibonacci.append(1)
           else:
               fibonacci.append(fibonacci[i-1]+fibonacci[i-2])

        return(fibonacci)


    def fibonacci5(pos=5):
        '''
        Calcula a sequência fibonacci até a posição (pos) indicada, retorna um primos como os elementos.
        :param pos: int
        :return: list
        '''
        fibonacci = []

        if not int(pos):
            raise TypeError('"pos" deverá ser um inteiro positivo.')
        pos = int(pos)

        if pos <= 0:
            raise ValueError('"pos" deverá ser um inteiro positivo.')

        for i in range(pos):
           if (i <= 1 ):
               fibonacci.append(1)
           else:
               fibonacci.append(fibonacci[i-1]+fibonacci[i-2])

        return(fibonacci)


if __name__ == '__main__':
    '''
    #Nesta execução com poucos elementos fica rapido
    for i in range(11):
        #print(i, ':', Fibonacci.fibonacci0(i))
        if i >= 10: print()

    #Nesta execução com muitos elementos a recursão fica lentas
    for i in range(111):
        #print(i, ':', Fibonacci.fibonacci0(i))
        if i >= 111: print()


    #Nesta execução com muitos elementos a recursão utiliza cache e não ficam lentas
    for i in range(1111):
        #print(i, ':', Fibonacci.fibonacci1(i))
        if i >= 1110: print()

#Nesta execução com muitos elementos a recursão utiliza lru_cache e não ficam lentas
    for i in range(1111):
        #print(i, ':', Fibonacci().fibonacci2(i))
        if i >= 1110: print()


    try:
        print(Fibonacci().fibonacci3('i'))
    except:
        print("Fibonacci().fibonacci3('i'): ",sys.exc_info()[1])
    try:
        print(Fibonacci().fibonacci3(2.1))
    except:
        print("Fibonacci().fibonacci3(2.1)", sys.exc_info()[1])

    try:
        print(Fibonacci().fibonacci3(-1))
    except:
        print("Fibonacci().fibonnati3(-1)", sys.exc_info()[1])

    try:
        print(Fibonacci().fibonacci3(0))
    except:
        print("Fibonacci().fibonnati3(0)", sys.exc_info()[1])

    print('*'*50)
    print(Fibonacci().fibonacci3(0))

    print(Fibonacci().fibonacci3(1))

    print(Fibonacci().fibonacci3(2))
    print(Fibonacci().fibonacci3(3))

#Nesta execução com muitos elementos a recursão utiliza lru_cache e não ficam lentas
    for i in range(1,51):
        print(i, ':', Fibonacci().razao_fibonacci(i))
        if i >= 1110: print()
    print(50*'-')

    print(Fibonacci.fibonacci4(10))
    print(Fibonacci.fibonacci5(10))

    try:
        print(Fibonacci.fibonacci5(-10))
    except:
        print('Fibonacci.fibonacci5(-10):',sys.exc_info())

    try:
        print(Fibonacci.fibonacci5(10.))
    except:
        print('Fibonacci.fibonacci5(10.)', sys.exc_info())

    try:
        print(Fibonacci.fibonacci5(10.9))
    except:
        print('Fibonacci.fibonacci5(10.9)', sys.exc_info())

    try:
        print(Fibonacci.fibonacci5('-10'))
    except:
        print("Fibonacci.fibonacci5('-10'): ", sys.exc_info())


    '''
    #for i in range(100):
    #    print(i, Fibonacci.fibonacci0(i))

    #for i in range(100):
    #    print(i, Fibonacci.fibonacci1(i))

    for i in range(100):
        print(i, Fibonacci().fibonacci2(i))
