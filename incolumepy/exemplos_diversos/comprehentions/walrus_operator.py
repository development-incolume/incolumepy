#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random


def get_weather_data():
    return random.randrange(90, 110)


hot_temps = [temp for _ in range(20) if (temp:= get_weather_data()) >= 100]

hot_temperatures = {i: temp for i in range(20) if (temp:= get_weather_data()) >= 100}

if __name__ == '__main__':
    print(hot_temps)
    print(f"{len(hot_temperatures)} itens: {hot_temperatures}")
