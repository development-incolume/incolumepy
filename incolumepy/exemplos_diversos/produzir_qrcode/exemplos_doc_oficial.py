#!/usr/bin/env python
# -*- coding: utf-8 -*-

import qrtools
import pyqrcode
import sys
import os
import io
import inspect
import vobject
from PIL import Image
from datetime import datetime

dir_output = __file__.replace('.py', '')
print(dir_output)


def file_output(s: str):
    os.makedirs(dir_output, exist_ok=True)
    return os.path.join(dir_output, s)


def exemplo0():
    '''Saída do QRCode no formato Scalable Vector Graphics (svg) e Portable Network Graphics (png)'''
    url = pyqrcode.create('http://uca.edu')
    # url.svg(sys.stdout, scale=1)
    url.svg(file_output('uca.svg'), scale=4)
    number = pyqrcode.create(123456789012345)
    number.png(file_output('big-number.png'))


def exemplo1():
    '''Saída do QRCode no formato Scalable Vector Graphics (svg) e Portable Network Graphics (png)'''
    url = pyqrcode.create('http://uca.edu')
    # url.svg(sys.stdout, scale=1)
    url.svg(file_output('uca.svg'), scale=4)
    print(url.terminal())
    number = pyqrcode.create(123456789012345)
    number.png(file_output('big-number.png'))


def exemplo2():
    '''Saída do QRCode no formato Encapsulated PostScript (eps)'''
    qr = pyqrcode.create('Hello world')
    qr.eps(file_output('hello-world.eps'), scale=2.5, module_color='#36C')
    qr.eps(file_output('hello-world2.eps'), background='#eee')
    out = io.StringIO()
    qr.eps(out, module_color=(.4, .4, .4))


def exemplo3():
    code = pyqrcode.QRCode("I don't like spam!")
    print(code.get_png_size(1))
    # 37
    print(code.get_png_size(5))
    # 185


def exemplo4():
    '''Tratamento dos elementos de cor do QRCode'''
    code = pyqrcode.create('Are you suggesting coconuts migrate?')
    code.png(file_output('swallow.png'), scale=5)
    code.png(file_output('swallow.png'), scale=5,
             module_color=(0x66, 0x33, 0x0),  # Dark brown
             background=(0xff, 0xff, 0xff, 0x88))  # 50% transparent white


def exemplo5():
    '''Tratamento dos elementos de cor do QRCode'''
    code = pyqrcode.create('Teste com cor de código para QRCode')
    code.png(file_output('exemplo5.png'), scale=5)
    code.png(file_output('exemplo5.png'), scale=5,
             module_color=(0xAA, 0xBB, 0xCC),  # Dark brown
             background=(0xff, 0xff, 0xff, 0x88))  # 50% transparent white


def exemplo6():
    '''(Notação Format str) png para codificação base64'''
    code = pyqrcode.create('Are you suggesting coconuts migrate?')
    image_as_str = code.png_as_base64_str(scale=5)
    html_img = '<img src="data:image/png;base64,{}">'.format(image_as_str)
    print(html_img)


def exemplo7():
    '''(Notação Fstr) png para codificação base64'''
    code = pyqrcode.create('Are you suggesting coconuts migrate?')
    image_as_str = code.png_as_base64_str(scale=5)
    html_img = f'<img src="data:image/png;base64,{image_as_str}">'
    print(html_img)


def exemplo8():
    '''(Notação %str) png para codificação base64'''
    code = pyqrcode.create('Are you suggesting coconuts migrate?')
    image_as_str = code.png_as_base64_str(scale=5)
    html_img = '<img src="data:image/png;base64,%s">' % image_as_str
    print(html_img)


def exemplo9():
    '''intermediário do exemplo10'''
    code = pyqrcode.create('Hello. Uhh, can we have your liver?')
    code.svg(file_output('live-organ-transplants.svg'), 3.6)


def exemplo10():
    '''fix cor e background'''
    code = pyqrcode.create('Hello. Uhh, can we have your liver?')
    code.svg(file_output('live-organ-transplants1.svg'), 3.6)
    code.svg(file_output('live-organ-transplants1.svg'), scale=4, module_color='brown', background='0xFFFFFF')


def exemplo11():
    '''qrcode no terminal'''
    code = pyqrcode.create('Example')
    text = code.terminal()
    print(text)


def exemplo12():
    '''qrcode para text'''
    code = pyqrcode.create('Example')
    text = code.text()
    print(text)


def exemplo13():
    '''tratativa com modulo grafico tkinter'''
    import tkinter
    # Create and render the QR code
    code = pyqrcode.create('Knights who say ni!')
    code_xbm = code.xbm(scale=5)
    # Create a tk window
    top = tkinter.Tk()
    # Make generate the bitmap image from the redered code
    code_bmp = tkinter.BitmapImage(data=code_xbm)
    # Set the code to have a white background,
    # instead of transparent
    code_bmp.config(background="white")
    # Bitmaps are accepted by lots of Widgets
    label = tkinter.Label(image=code_bmp)
    # The QR code is now visible
    label.pack()


def exemplo14():
    '''Impressão no terminal'''
    url = pyqrcode.create('http://uca.edu')
    url.svg(file_output(f'{inspect.stack()[0][3]}.svg'), scale=8)
    url.eps(file_output(f'{inspect.stack()[0][3]}.eps'), scale=2)
    print(url.terminal(quiet_zone=1))


def exemplo15():
    '''Criação e apresentação do QRCode
    nonexequi'''
    big_code = pyqrcode.create('0987654321', error='L', version=27, mode='binary')
    big_code.png(file_output(f'{inspect.stack()[0][3]}.png'), scale=6, module_color=[0, 0, 0, 128], background=[0xff, 0xff, 0xcc])
    big_code.show()


def exemplo16():
    '''Exemplo de utilização do pyqrcode.terminal'''
    text = pyqrcode.create('Example')
    print('Default\n', text.terminal())
    print('---\nf: red; b: yellow\n', text.terminal(module_color='red', background='yellow'))
    print('---\nquiet_zone', text.terminal(module_color=5, background=123, quiet_zone=1))


def exemplo17():
    '''Gravar byte em QRCode'''
    url = pyqrcode.create('http://uca.edu')
    url.svg(file_output(f'{inspect.stack()[0][3]}.svg'), scale=4)

    # in-memory stream is also supported
    buffer = io.BytesIO()
    url.svg(buffer)

    # do whatever you want with buffer.getvalue()
    print(list(buffer.getvalue()))


def exemplo18():
    '''foreground roxo'''
    url = pyqrcode.create('http://brito.blog.incolume.com.br')
    url.svg(file_output(f'{inspect.stack()[0][3]}.svg'), scale=4, background="white", module_color="#7D007D")


def exemplo19():
    ''''''
    qr = pyqrcode.create('Hello world')
    qr.eps(file_output(f'{inspect.stack()[0][3]}_1.eps'), scale=2.5, module_color='#36C')
    qr.eps(file_output(f'{inspect.stack()[0][3]}_2.eps'), background='#eee')
    out = io.StringIO()
    qr.eps(out, module_color=(.4, .4, .4))


def exemplo20():
    '''Geração do QRCode e gravação com bloco with/modulo pypng/ buffer'''
    url = pyqrcode.create('http://uca.edu')
    with open(file_output(f'{inspect.stack()[0][3]}_0.png'), 'wb') as fstream:
        url.png(fstream, scale=5)

    # same as above
    url.png(file_output(f'{inspect.stack()[0][3]}_1.png'), scale=5)

    # in-memory stream is also supported
    buffer = io.BytesIO()
    url.png(buffer)

    # do whatever you want with buffer.getvalue()
    print(list(buffer.getvalue()))


def exemplo21():
    '''PNG colors'''
    url = pyqrcode.create('Ops')
    url.png(file_output(f'{inspect.stack()[0][3]}.png'),
            scale=6,
            module_color=[0, 0, 0, 128],
            background=[0xff, 0xff, 0xcc]
            )


def exemplo22():
    '''PNG colors'''
    url = pyqrcode.create('Ops')
    url.png(file_output(f'{inspect.stack()[0][3]}.png'),
            scale=6,
            module_color=[0xff, 0xff, 0xcc],
            background=[0, 0, 0, 128],

            )


def exemplo23():
    '''PNG colors'''
    url = pyqrcode.create('Ops')
    url.png(file_output(f'{inspect.stack()[0][3]}.png'),
            scale=6,
            module_color=(0x00, 0x00, 0x00),
            background=(0, 0, 0, 128),

            )


def exemplo24():
    '''criar VCard'''
    j = vobject.vCard()
    print(j)
    j.add('n')
    print(j)


def exemplo25():
    '''ler QRCode Image
    fail'''
    qr = qrtools.QR()
    qr.decode(os.path.expanduser('~/brito/Downloads/photo4994533579673413684.jpeg'))
    print(qr.data)


def exemplo26():
    '''vCard (vobject)'''
    j = vobject.vCard()
    j.add('n')
    j.n.value = vobject.vcard.Name(family='Harris', given='Jeffrey')
    j.add('fn')
    j.fn.value = 'Jeffrey Harris'
    j.add('email')
    j.email.value = 'jeffrey@osafoundation.org'
    j.email.type_param = 'profissional'
    j.prettyPrint()


def exemplo27():
    '''vCard (vobject)'''
    j = vobject.vCard()
    j.add('n')
    j.n.value = vobject.vcard.Name(family='Harris', given='Jeffrey')
    j.add('fn')
    j.fn.value = 'Jeffrey Harris'
    j.add('email')
    j.email.value = 'jeffrey@osafoundation.org'
    j.email.type_param = 'profissional'
    print(j.serialize())
    return j.serialize()


def exemplo28():
    '''Parsing
    vCards'''

    s =exemplo27()
    print(s)
    v = vobject.readOne(s)
    v.prettyPrint()

    v.n.value.family()


def exemplo29():
    d = pyqrcode.Decoder()
    if d.decode('exemplos_doc_oficial/big-number.png'):
        print(d.result)
    else:
        print(d.error)


def exemplo30():
    '''QRCode com logo
    https://stackoverflow.com/questions/45481990/how-to-insert-logo-in-the-center-of-qrcode-in-python
    '''
    url = pyqrcode.QRCode('http://blog.incolume.com.br', error='H')
    url.png(file_output(f'{inspect.stack()[0][3]}.png'), scale=10)
    im = Image.open(file_output(f'{inspect.stack()[0][3]}.png'))
    im = im.convert("RGBA")
    logo = Image.open('files-png-icon.png')
    box = (135, 135, 235, 235)
    im.crop(box)
    region = logo
    region = region.resize((box[2] - box[0], box[3] - box[1]))
    im.paste(region, box)
    im.show()


def exemplo31():
    '''QRCode com logo
    https://stackoverflow.com/questions/45481990/how-to-insert-logo-in-the-center-of-qrcode-in-python
    '''
    # Generate the qr code and save as png
    qrobj = pyqrcode.create('https://stackoverflow.com')
    with open(file_output(f'{inspect.stack()[0][3]}.png'), 'wb') as f:
        qrobj.png(f, scale=10)

    # Now open that png image to put the logo
    img = Image.open(file_output(f'{inspect.stack()[0][3]}.png'))
    width, height = img.size

    # How big the logo we want to put in the qr code png
    logo_size = 50

    # Open the logo image
    logo = Image.open('files-png-icon.png')

    # Calculate xmin, ymin, xmax, ymax to put the logo
    xmin = ymin = int((width / 2) - (logo_size / 2))
    xmax = ymax = int((width / 2) + (logo_size / 2))

    # resize the logo as calculated
    logo = logo.resize((xmax - xmin, ymax - ymin))

    # put the logo in the qr code
    img.paste(logo, (xmin, ymin, xmax, ymax))

    img.show()


def exemplo32():
    '''QRCode com logo
        https://stackoverflow.com/questions/45481990/how-to-insert-logo-in-the-center-of-qrcode-in-python
    '''
    url = pyqrcode.QRCode('http://blog.incolume.com.br', error='H')
    url.png(file_output(f'{inspect.stack()[0][3]}.png'), scale=10)
    im = Image.open(file_output(f'{inspect.stack()[0][3]}.png'))
    im = im.convert("RGBA")
    logo = Image.open('files-png-icon.png')
    box = (135, 135, 275, 275)
    im.crop(box)
    region = logo
    region = region.resize((box[2] - box[0], box[3] - box[1]))
    im.paste(region, box)
    im.show()


def exemplo33():
    '''QRCode com logo
        https://stackoverflow.com/questions/45481990/how-to-insert-logo-in-the-center-of-qrcode-in-python
    '''
    fileout = file_output(f'{inspect.stack()[0][3]}.png')
    url = pyqrcode.QRCode('http://blog.incolume.com.br', error='H')
    url.png(fileout, scale=10)
    im = Image.open(fileout)
    im = im.convert("RGBA")
    logo = Image.open('files-png-icon.png')
    print(im.size, logo.size, abs(-10))
    # box = (*[int(x/2) for x in im.size], *[int(x/2) for x in logo.size])
    print(*[int(abs(y-x)/2) for x in im.size for y in logo.size])
    print(*[int((y - abs(y-x))/2) for x, y in zip(im.size, logo.size)])
    box = (135, 135, 275, 275)
    # box = (*[int((y - abs(y-x)/2)/2) for x, y in zip(im.size, logo.size)], *[int(x/2) for x in logo.size])
    # box = (*[int(x/2) for x in im.size], *[int(x/2) for x in logo.size])
    # box = (*[x//2 for x in im.size], *[x//2 for x in logo.size])
    # box = (*[(y - abs(y - x) // 2) // 2 for x, y in zip(im.size, logo.size)], *[x//2 for x in logo.size])
    # box = (*[x//2 for x in logo.size], *[(y - abs(y - x)) // 2 for x, y in zip(im.size, logo.size)])
    # box = (*[abs((y - x)) for x, y in zip(im.size, logo.size)], *[x//2 for x in logo.size])
    # box = (*[x//2 for x in im.size], *[x//2 for x in logo.size])
    print(box)
    im.crop(box)
    region = logo
    region = region.resize((abs(box[2] - box[0]), abs(box[3] - box[1])))
    im.paste(region, box)
    im.show()


def exemplo34():
    '''QRCode com logo
        https://stackoverflow.com/questions/45481990/how-to-insert-logo-in-the-center-of-qrcode-in-python
    '''
    fileout = file_output(f'{inspect.stack()[0][3]}.png')
    url = pyqrcode.QRCode('http://blog.incolume.com.br', error='H')
    url.png(fileout, scale=10)
    im = Image.open(fileout)
    im = im.convert("RGBA")
    logo = Image.open('apple-touch-icon.png')
    box = (135, 135, 275, 275)
    print(box)
    im.crop(box)
    region = logo
    region = region.resize((abs(box[2] - box[0]), abs(box[3] - box[1])))
    im.paste(region, box)
    im.show()


def exemplo35():
    a = {'a11': 11, 'a22': 22}
    s = '{a11}_{a22}'
    print('{a11}, {a22}'.format(**a))
    print('{a11}, {a11}'.format(**a))
    print('{a22}, {a22}'.format(**a))
    print(s.format(**a))

    def b(**kwargs):
        print(s.format(**kwargs))
    b(a11=111, a22=222)


def exemplo36():
    '''vCard (vobject)'''
    j = vobject.vCard()
    j.add('n')
    j.n.value = vobject.vcard.Name(family='Harris', given='Jeffrey')
    j.add('fn')
    j.fn.value = 'Jeffrey Harris'
    j.add('email')
    j.email.value = 'jeffrey@osafoundation.org'
    j.email.type_param = 'profissional'
    j.prettyPrint()


def exemplo37():
    vcard21 = """
    BEGIN:VCARD
    VERSION:2.1
    FN:Jean Dupont
    N:Dupont;Jean
    ADR;WORK;PREF;QUOTED-PRINTABLE:;Bruxelles 1200=Belgique;6A Rue Th. Decuyper
    LABEL;QUOTED-PRINTABLE;WORK;PREF:Rue Th. Decuyper 6A=Bruxelles 1200=Belgique
    TEL;CELL:+1234 56789
    EMAIL;INTERNET:jean.dupont@example.com
    UID:
    END:VCARD
    """

    vcard30 = """
    BEGIN:VCARD
    VERSION:3.0
    N:Gump;Forrest
    FN:Forrest Gump
    ORG:Bubba Gump Shrimp Co.
    TITLE:Shrimp Man
    PHOTO;VALUE=URL;TYPE=GIF:http://www.example.com/dir_photos/my_photo.gif
    TEL;TYPE=WORK;VOICE:(111) 555-1212
    TEL;TYPE=HOME;VOICE:(404) 555-1212
    ADR;TYPE=WORK:;;100 Waters Edge;Baytown;LA;30314;United States of America
    LABEL;TYPE=WORK:100 Waters Edge\nBaytown, LA 30314\nUnited States of America
    ADR;TYPE=HOME:;;42 Plantation St.;Baytown;LA;30314;United States of America
    LABEL;TYPE=HOME:42 Plantation St.\nBaytown, LA 30314\nUnited States of America
    EMAIL;TYPE=PREF,INTERNET:forrestgump@example.com
    REV:20080424T195243Z
    END:VCARD
    """

    vcard40 = """
    BEGIN:VCARD
    VERSION:4.0
    N:Gump;Forrest;;Mr.;
    FN:Forrest Gump
    ORG:Bubba Gump Shrimp Co.
    TITLE:Shrimp Man
    PHOTO;MEDIATYPE=image/gif:http://www.example.com/dir_photos/my_photo.gif
    TEL;TYPE=work,voice;VALUE=uri:tel:+1-111-555-1212
    TEL;TYPE=home,voice;VALUE=uri:tel:+1-404-555-1212
    ADR;TYPE=WORK;PREF=1;LABEL="100 Waters Edge\nBaytown\n, LA 30314\nUnited States of America":;;100 Waters Edge;Baytown;LA;30314;United States of America
    ADR;TYPE=HOME;LABEL="42 Plantation St.\nBaytown\, LA 30314\nUnited States of America":;;42 Plantation St.;Baytown;LA;30314;United States of America
    EMAIL:forrestgump@example.com
    REV:20080424T195243Z
    x-qq:21588891
    END:VCARD
    """

    result = vcard21, vcard30, vcard40
    print(result)
    return result


def exemplo38():
    '''texto de vCard'''

    VCARD_TEMPLATE = """
    BEGIN:VCARD
    VERSION:2.1
    N:{sname};{name}
    FN:{sname}
    NICKNAME: {nickname}
    PHOTO;VALUE=URL;TYPE=JPEG:https://s.gravatar.com/avatar/f4955d6d76ae1c7ec8a89ace98289d2a?s=80
    ORG:{org}
    TITLE:{title}
    TEL;WORK;VOICE:{telwork}
    TEL;HOME;VOICE:{telhome}
    ADR;WORK:;;{work_addr};{work_city};{work_state};{work_cep};{work_country}
    GEO:geo:{work_lat},{work_long}
    EMAIL;PREF;INTERNET:{emailwork}
    EMAIL;;HOME:{emailhome}
    URL: {url}
    NOTE: {note}
    REV:{rev}
    END:VCARD
    """

    def fill_vcard(**kwargs):
        return VCARD_TEMPLATE.format(**kwargs)
        # return kwargs.get('sname')
        # return kwargs

    print(fill_vcard(
        name='Ricardo',
        sname='Brito',
        nickname='Brito',
        org='Incolume group',
        work_country='Brasil',
        work_state='DF',
        work_city='Brasília',
        work_cep='72.000-000',
        work_addr='',
        work_lat='-15.870077',
        work_long='-48.087377',
        title='Analista de Sistemas',
        telwork='+55 61 3411-5944',
        telhome='+55 61 3585-2485',
        emailwork='britodfbr@gmail.com',
        emailhome='brito@incolume.com.br',
        url='http://brito.blog.incolume.com.br',
        note=r':\ :| :) :D',
        rev=datetime.now().strftime('%Y%m%dT%H%M%SZ')
        )
    )


def exemplo39():
    '''str to VCard'''
    VCARD_TEMPLATE = """BEGIN:VCARD
VERSION:2.1
N:{sname};{name}
FN:{sname}
NICKNAME: {nickname}
PHOTO;VALUE=URL;TYPE=JPEG:https://s.gravatar.com/avatar/f4955d6d76ae1c7ec8a89ace98289d2a?s=80
ORG:{org}
TITLE:{title}
TEL;WORK;VOICE:{telwork}
TEL;HOME;VOICE:{telhome}
ADR;WORK:;;{work_addr};{work_city};{work_state};{work_cep};{work_country}
GEO:geo:{work_lat},{work_long}
EMAIL;PREF;INTERNET:{emailwork}
EMAIL;;HOME:{emailhome}
URL: {url}
NOTE: {note}
REV:{rev}
END:VCARD"""

    def fill_vcard(**kwargs):
        return VCARD_TEMPLATE.format(**kwargs)

    s = fill_vcard(
        name='Ricardo',
        sname='Brito',
        nickname='Brito',
        org='Incolume group',
        work_country='Brasil',
        work_state='DF',
        work_city='Brasília',
        work_cep='72.000-000',
        work_addr='',
        work_lat='-15.870077',
        work_long='-48.087377',
        title='Analista de Sistemas',
        telwork='+55 61 3411-5944',
        telhome='+55 61 3585-2485',
        emailwork='britodfbr@gmail.com',
        emailhome='brito@incolume.com.br',
        url='http://brito.blog.incolume.com.br',
        note=r':\ :| :) :D',
        rev=datetime.now().strftime('%Y%m%dT%H%M%SZ')
        )
    v = vobject.readOne(s)
    print(v.prettyPrint())


def exemplo40():
    '''str to VCard'''
    VCARD_TEMPLATE = """BEGIN:VCARD
VERSION:4.0
N:{sname};{name}
FN:{sname}
NICKNAME: {nickname}
PHOTO;VALUE=URL;TYPE=JPEG:https://s.gravatar.com/avatar/f4955d6d76ae1c7ec8a89ace98289d2a?s=80
ORG:{org}
TITLE:{title}
TEL;WORK;VOICE:{telwork}
TEL;HOME;VOICE:{telhome}
ADR;WORK:;;{work_addr};{work_city};{work_state};{work_cep};{work_country}
GEO:geo:{work_lat},{work_long}
EMAIL;PREF;INTERNET:{emailwork}
EMAIL;;HOME:{emailhome}
URL: {url}
NOTE: {note}
REV:{rev}
END:VCARD"""

    def fill_vcard(**kwargs):
        return VCARD_TEMPLATE.format(**kwargs)

    s = fill_vcard(
        name='Ricardo',
        sname='Brito',
        nickname='Brito',
        org='Incolume group',
        work_country='Brasil',
        work_state='DF',
        work_city='Brasília',
        work_cep='72.000-000',
        work_addr='',
        work_lat='-15.870077',
        work_long='-48.087377',
        title='Analista de Sistemas',
        telwork='+55 61 3411-5944',
        telhome='+55 61 3585-2485',
        emailwork='britodfbr@gmail.com',
        emailhome='brito@incolume.com.br',
        url='http://brito.blog.incolume.com.br',
        note=r':\ :| :) :D',
        rev=datetime.now().strftime('%Y%m%dT%H%M%SZ')
        )
    v = vobject.readOne(s)
    print(v.prettyPrint())
    qr_img = pyqrcode.QRCode(s)
    print(qr_img.data)
    qr_img.png(file_output(f'{inspect.stack()[0][3]}_1.png'), scale=10)
    qr_img.terminal()
    qr_img.show()


def exemplo41():
    '''str to VCard'''
    VCARD_TEMPLATE = """BEGIN:VCARD
VERSION:4.0
N:{sname};{name}
FN:{sname}
NICKNAME: {nickname}
PHOTO;VALUE=URL;TYPE=JPEG:https://s.gravatar.com/avatar/f4955d6d76ae1c7ec8a89ace98289d2a?s=80
ORG:{org}
TITLE:{title}
TEL;WORK;VOICE:{telwork}
TEL;HOME;MOBILE:{telmobile}
ADR;WORK:;;{work_addr};{work_city};{work_state};{work_cep};{work_country}
GEO:geo:{work_lat},{work_long}
EMAIL;PREF;INTERNET:{emailwork}
URL: {url}
NOTE: {note}
REV:{rev}
END:VCARD"""

    def fill_vcard(**kwargs):
        return VCARD_TEMPLATE.format(**kwargs)

    s = fill_vcard(
        name='Ricardo',
        sname='Brito',
        nickname='Brito',
        org='Incolume group',
        work_country='Brasil',
        work_state='DF',
        work_city='Brasília',
        work_cep='72.000-000',
        work_addr='',
        work_lat='-15.870077',
        work_long='-48.087377',
        title='Analista de Sistemas',
        telwork='+55 61 3411-5944',
        telmobile='+55 61 99909-2244',
        emailwork='britodfbr@gmail.com',
        url='http://brito.blog.incolume.com.br',
        note=r':\ :| :) :D',
        rev=datetime.now().strftime('%Y%m%dT%H%M%SZ')
        )
    v = vobject.readOne(s)
    print(v.prettyPrint())
    qr_img = pyqrcode.QRCode(s)
    print(qr_img.data)
    qr_img.png(file_output(f'{inspect.stack()[0][3]}.png'), scale=10)
    qr_img.terminal()
    qr_img.show()


def exemplo42():
    '''str to VCard'''
    VCARD_TEMPLATE = """BEGIN:VCARD
VERSION:4.0
N:{sname};{name}
FN:{name} {sname}
NICKNAME: {nickname}
PHOTO;VALUE=URL;TYPE=JPEG:https://s.gravatar.com/avatar/f4955d6d76ae1c7ec8a89ace98289d2a?s=80
ORG:{org}
TITLE:{title}
TEL;WORK;VOICE:{telwork}
TEL;HOME;MOBILE:{telmobile}
ADR;WORK:;;{work_addr};{work_city};{work_state};{work_cep};{work_country}
GEO:geo:{work_lat},{work_long}
EMAIL;PREF;INTERNET:{emailwork}
URL: {url}
NOTE: {note}
REV:{rev}
END:VCARD"""

    def fill_vcard(**kwargs):
        return VCARD_TEMPLATE.format(**kwargs)

    s = fill_vcard(
        name='Eliana',
        sname='Brito',
        nickname=None,
        org='Empresa Brasileira de Correios e Telegrafos',
        work_country='Brasil',
        work_state='DF',
        work_city='Brasília',
        work_cep='72.000-000',
        work_addr='SBN Q.1 Bl.A Ed. Sede',
        work_lat='-15.870077',
        work_long='-48.087377',
        title='Analista de Correios',
        telwork='+55 61 2141-6575',
        telmobile='+55 61 98285-2474',
        emailwork='elianasb@correios.com.br',
        url='http://www.correios.com.br',
        note=r'Departamento de Postagens',
        rev=datetime.now().strftime('%Y%m%dT%H%M%SZ')
        )
    v = vobject.readOne(s)
    print(v.prettyPrint())
    qr_img = pyqrcode.QRCode(s)
    print(qr_img.data)
    qr_img.png(file_output(f'{inspect.stack()[0][3]}.png'), scale=10)
    qr_img.terminal()
    qr_img.show()


def exemplo43():
    '''str to VCard'''
    VCARD_TEMPLATE = """BEGIN:VCARD
VERSION:4.0
N:{sname};{name}
FN:{name} {sname}
NICKNAME: {nickname}
PHOTO;VALUE=URL;TYPE=JPEG:https://s.gravatar.com/avatar/f4955d6d76ae1c7ec8a89ace98289d2a?s=80
ORG:{org}
TITLE:{title}
TEL;WORK;VOICE:{telwork}
TEL;HOME;MOBILE:{telmobile}
ADR;WORK:;;{work_addr};{work_city};{work_state};{work_cep};{work_country}
GEO:geo:{work_lat},{work_long}
EMAIL;PREF;INTERNET:{emailwork}
URL: {url}
NOTE: {note}
REV:{rev}
END:VCARD"""

    def fill_vcard(**kwargs):
        return VCARD_TEMPLATE.format(**kwargs)

    s = fill_vcard(
        name='Eliana',
        sname='Brito',
        nickname=None,
        org='Empresa Brasileira de Correios e Telegrafos',
        work_country='Brasil',
        work_state='DF',
        work_city='Brasília',
        work_cep='72.000-000',
        work_addr='SBN Q.1 Bl.A Ed. Sede',
        work_lat='-15.870077',
        work_long='-48.087377',
        title='Analista de Correios',
        telwork='+55 61 2141-6575',
        telmobile='+55 61 98285-2474',
        emailwork='elianasb@correios.com.br',
        url='http://www.correios.com.br',
        note=r'Departamento de Postagens',
        rev=datetime.now().strftime('%Y%m%dT%H%M%SZ')
        )
    v = vobject.readOne(s)
    print(v.prettyPrint())
    qr_img = pyqrcode.QRCode(s, error='H')
    print(qr_img.data)
    fileout = file_output(f'{inspect.stack()[0][3]}.png')
    qr_img.png(fileout, scale=10)

    im = Image.open(fileout)
    im = im.convert("RGBA")
    logo = Image.open('apple-touch-icon.png')
    print(im.size, logo.size)
    box = (*[abs(x - y)//2 for x, y in zip(im.size, logo.size)], *[abs(x + y)//2 for x, y in zip(im.size, logo.size)])
    print(box)
    im.crop(box)
    region = logo.copy()
    region = region.resize((abs(box[2] - box[0]), abs(box[3] - box[1])))
    im.paste(region, box)
    im.show()
    im.save(fileout.replace('.png', '_1.png'))


def exemplo44():
    '''str to VCard'''
    VCARD_TEMPLATE = """BEGIN:VCARD
VERSION:4.0
N:{sname};{name}
FN:{name} {sname}
NICKNAME: {nickname}
PHOTO;VALUE=URL;TYPE=JPEG:https://s.gravatar.com/avatar/f4955d6d76ae1c7ec8a89ace98289d2a?s=80
ORG:{org}
TITLE:{title}
TEL;WORK;VOICE:{telwork}
TEL;HOME;MOBILE:{telmobile}
ADR;WORK:;;{work_addr};{work_city};{work_state};{work_cep};{work_country}
GEO:geo:{work_lat},{work_long}
EMAIL;PREF;INTERNET:{emailwork}
URL: {url}
NOTE: {note}
REV:{rev}
END:VCARD"""

    def fill_vcard(**kwargs):
        return VCARD_TEMPLATE.format(**kwargs)

    s = fill_vcard(
        name='Eliana',
        sname='Brito',
        nickname=None,
        org='Empresa Brasileira de Correios e Telegrafos',
        work_country='Brasil',
        work_state='DF',
        work_city='Brasília',
        work_cep='72.000-000',
        work_addr='SBN Q.1 Bl.A Ed. Sede',
        work_lat='-15.870077',
        work_long='-48.087377',
        title='Analista de Correios',
        telwork='+55 61 2141-6575',
        telmobile='+55 61 98285-2474',
        emailwork='elianasb@correios.com.br',
        url='http://www.correios.com.br',
        note=r'Departamento de Postagens',
        rev=datetime.now().strftime('%Y%m%dT%H%M%SZ')
        )
    v = vobject.readOne(s)
    print(v.prettyPrint())
    qr_img = pyqrcode.QRCode(s, error='H')
    print(qr_img.data)
    fileout = file_output(f'{inspect.stack()[0][3]}.png')
    qr_img.png(fileout, scale=10)

    im = Image.open(fileout)
    im = im.convert("RGBA")
    logo = Image.open('apple-touch-icon.png')
    print(im.size, logo.size)
    logo = logo.resize((250, 250))
    box = (*[abs(x - y)//2 for x, y in zip(im.size, logo.size)], *[abs(x + y)//2 for x, y in zip(im.size, logo.size)])
    print(box)
    im.crop(box)
    region = logo.copy()
    region = region.resize((abs(box[2] - box[0]), abs(box[3] - box[1])))
    im.paste(region, box)
    im.show()
    im.save(fileout.replace('.png', '_1.png'))


def exemplo45():
    '''VCard to QRCode dados Eliana'''
    VCARD_TEMPLATE = """BEGIN:VCARD
VERSION:4.0
N:{sname};{name}
FN:{name} {sname}
NICKNAME: {nickname}
PHOTO;VALUE=URL;TYPE=JPEG:https://s.gravatar.com/avatar/f4955d6d76ae1c7ec8a89ace98289d2a?s=80
ORG:{org}
TITLE:{title}
TEL;WORK;VOICE:{telwork}
TEL;CELL;VOICE:{telmobile}
ADR;WORK:;;{work_addr};{work_city};{work_state};{work_cep};{work_country}
GEO:geo:{work_lat},{work_long}
EMAIL;PREF;INTERNET:{emailwork}
URL: {url}
NOTE: {note}
REV:{rev}
END:VCARD"""

    def fill_vcard(**kwargs):
        return VCARD_TEMPLATE.format(**kwargs)

    s = fill_vcard(
        name='Eliana',
        sname='Brito',
        nickname=None,
        org='Empresa Brasileira de Correios e Telegrafos',
        work_country='Brasil',
        work_state='DF',
        work_city='Brasília',
        work_cep='72.000-000',
        work_addr='SBN Q.1 Bl.A Ed. Sede',
        work_lat='-15.870077',
        work_long='-48.087377',
        title='Analista de Correios',
        telwork='+55 61 2141-6575',
        telmobile='+55 61 98285-2474',
        emailwork='elianasb@correios.com.br',
        url='http://www.correios.com.br',
        note=r'Departamento de Postagens',
        rev=datetime.now().strftime('%Y%m%dT%H%M%SZ')
        )
    v = vobject.readOne(s)
    print(v.prettyPrint())
    qr_img = pyqrcode.QRCode(s, error='H')
    print(qr_img.data)
    fileout = file_output(f'{inspect.stack()[0][3]}.png')
    qr_img.png(fileout, scale=10)

    im = Image.open(fileout)
    im = im.convert("RGBA")
    logo = Image.open('eliana.png')
    print(im.size, logo.size)
    logo = logo.resize([int(x*.4) for x in im.size])
    box = (*[abs(x - y)//2 for x, y in zip(im.size, logo.size)], *[abs(x + y)//2 for x, y in zip(im.size, logo.size)])
    print(box)
    im.crop(box)
    region = logo.copy()
    region = region.resize((abs(box[2] - box[0]), abs(box[3] - box[1])))
    im.paste(region, box)
    im.show()
    im.save(fileout.replace('.png', '_1.png'))


def exemplo46():
    '''VCard to QRCode dados Eliana'''
    VCARD_TEMPLATE = """BEGIN:VCARD
VERSION:4.0
N:{sname};{name}
FN:{name} {sname}
NICKNAME: {nickname}
PHOTO;VALUE=URL;TYPE=JPEG:https://s.gravatar.com/avatar/f4955d6d76ae1c7ec8a89ace98289d2a?s=80
ORG:{org}
TITLE:{title}
TEL;TYPE=work,voice;VALUE=uri:{telwork}
TEL;TYPE=CELL,VOICE;VALUE=URI:{telmobile}
ADR;WORK:;;{work_addr};{work_city};{work_state};{work_cep};{work_country}
EMAIL;WORK;INTERNET:{emailwork}
URL;TYPE=WEBSITE: {url}
NOTE: {note}
REV:{rev}
END:VCARD"""

    def fill_vcard(**kwargs):
        return VCARD_TEMPLATE.format(**kwargs)

    s = fill_vcard(
        name='Eliana',
        sname='Brito',
        nickname=None,
        org='Empresa Brasileira de Correios e Telegrafos',
        work_country='Brasil',
        work_state='DF',
        work_city='Brasília',
        work_cep='72.000-000',
        work_addr='SBN Q.1 Bl.A Ed. Sede',
        work_lat='-15.870077',
        work_long='-48.087377',
        title='Analista de Correios',
        telwork='+55 61 2141-6575',
        # telmobile='+55 61 98285-2474',
        telmobile=None,
        emailwork='elianasb@correios.com.br',
        url='http://www.correios.com.br',
        note=r'Departamento de Postagens',
        rev=datetime.now().strftime('%Y%m%dT%H%M%SZ')
        )
    v = vobject.readOne(s)
    print(v.prettyPrint())
    qr_img = pyqrcode.QRCode(s, error='H')
    print(qr_img.data)
    fileout = file_output(f'{inspect.stack()[0][3]}.png')
    qr_img.png(fileout, scale=10)

    im = Image.open(fileout)
    im = im.convert("RGBA")
    logo = Image.open('eliana.png')
    print(im.size, logo.size)
    logo = logo.resize([int(x*.4) for x in im.size])
    box = (*[abs(x - y)//2 for x, y in zip(im.size, logo.size)], *[abs(x + y)//2 for x, y in zip(im.size, logo.size)])
    print(box)
    im.crop(box)
    region = logo.copy()
    region = region.resize((abs(box[2] - box[0]), abs(box[3] - box[1])))
    im.paste(region, box)
    im.show()
    im.save(fileout.replace('.png', '_1.png'))


def fill_vcard(**kwargs):
    VCARD_TEMPLATE = """BEGIN:VCARD
VERSION:4.0
N:{sname};{name}
FN:{name} {sname}
NICKNAME: {nickname}
PHOTO;VALUE=URL;TYPE=JPEG:https://s.gravatar.com/avatar/f4955d6d76ae1c7ec8a89ace98289d2a?s=80
ORG:{org}
TITLE:{title}
TEL;TYPE=work,voice;VALUE=uri:{telwork}
TEL;TYPE=CELL,VOICE;VALUE=URI:{telmobile}
ADR;WORK:;;{work_addr};{work_city};{work_state};{work_cep};{work_country}
EMAIL;TYPE=WORK,INTERNET:{emailwork}
EMAIL;TYPE=HOME,INTERNET:{emailhome}
URL;TYPE=WEBSITE: {url}
NOTE: {note}
REV:{rev}
END:VCARD"""
    return VCARD_TEMPLATE.format(**kwargs)


def exemplo47():
    '''VCard to QRCode dados Eliana'''
    s = fill_vcard(
        name='Ricardo',
        sname='Brito',
        nickname=None,
        org='Incolume group',
        work_country='Brasil',
        work_state='DF',
        work_city='Brasília',
        work_cep='72.000-000',
        work_addr=None,
        work_lat='-15.870077',
        work_long='-48.087377',
        title='Analista de Sistemas',
        telwork='+55 61 3411-5944',
        telmobile='+55 61 99909-2244',
        emailwork='ricardo.brito@presidencia.gov.br',
        emailhome='britodfbr@gmail.com',
        url='http://www4.planalto.gov.br/legislacao',
        note=r'Centro de Estudos Jurídicos',
        rev=datetime.now().strftime('%Y%m%dT%H%M%SZ')
    )
    v = vobject.readOne(s)
    print(v.prettyPrint())
    qr_img = pyqrcode.QRCode(s, error='L')
    print(qr_img.data)
    fileout = file_output(f'{inspect.stack()[0][3]}.png')
    qr_img.png(fileout, scale=10)

    im = Image.open(fileout)
    im = im.convert("RGBA")
    logo = Image.open('apple-touch-icon.png')
    print(im.size, logo.size)
    logo = logo.resize([int(x * .4) for x in im.size])
    box = (
    *[abs(x - y) // 2 for x, y in zip(im.size, logo.size)], *[abs(x + y) // 2 for x, y in zip(im.size, logo.size)])
    print(box)
    im.crop(box)
    region = logo.copy()
    region = region.resize((abs(box[2] - box[0]), abs(box[3] - box[1])))
    im.paste(region, box)
    im.show()
    im.save(fileout.replace('.png', '_1.png'))


def run(func='exemplo'):
    for f in (f'{func}{x}' for x in range(46, 48)):
        try:
            print('===')
            print(f)
            if eval(f).__doc__:
                if 'nonexequi' in eval(f).__doc__.lower():
                    raise NameError('Flag Nonexequi')
                print(eval(f).__doc__)
                print('---')
            eval(f)()
        except NameError:
            print(sys.exc_info())
        except (AttributeError, TypeError) as e:
            print(e)
        else:
            print('---')
        finally:
            print()


if __name__ == '__main__':
    run()
