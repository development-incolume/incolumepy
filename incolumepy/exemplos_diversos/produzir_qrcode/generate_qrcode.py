#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pyqrcode
import sys
import re
import os
from unicodedata import normalize
from functools import wraps


def generate_qr_0():
    link_to_post = "https://medium.com/@ngengesenior/qr-codes-generation-with-python-377735be6c5f"
    url = pyqrcode.create(link_to_post)
    url.png('url.png', scale=8)
    print("Printing QR code")
    print(url.terminal())


def generate_qr_1(link='planalto.gov.br/legislacao'):
    url = pyqrcode.create(link)
    url.png('url.png', scale=8)
    print("Printing QR code")
    print(url.terminal())


def generate_qr(*args, **kwargs):
    name = None
    link = None
    svg = kwargs.get('svg') if isinstance(kwargs.get('svg'), bool) else False
    png = kwargs.get('png') if isinstance(kwargs.get('png'), bool) else False
    dir = kwargs.get('dir') if 'dir' in kwargs else '.'

    if args:
        if isinstance(args.__getitem__(0), tuple):
            name, link = args.__getitem__(0)
            name = normalize('NFKD', name.lower()).encode('ASCII', 'ignore').decode('ASCII')
        else:
            link = args.__getitem__(0)
    if not link:
        link = kwargs.get('link')

    if not os.path.isdir(dir):
        os.makedirs(dir, exist_ok=True)

    if name:
        file_base_output = os.path.join(
            dir,
            "{}_{}".format(os.path.basename(__file__).replace('.py', ''), re.sub(r'[/.\ ]', '_', name))
        )
    else:
        file_base_output = os.path.join(dir, f"{os.path.basename(__file__).replace('.py', '')}_{re.sub(r'[/.]', '', link)}")
    url = pyqrcode.create(link)
    if png:
        url.png(f'{file_base_output}.png', scale=8)
    if svg:
        url.svg(f'{file_base_output}.svg', scale=8)
    print("Printing QR code")
    print(url.terminal())


def generate_qr_2():
    generate_qr('planalto.gov.br/legislacao')


def generate_qr_3():
    generate_qr(link='planalto.gov.br/ccivil_03/css/legis_3.css')


def generate_qr_4():
    generate_qr('planalto.gov.br/ccivil_03/js/includeHTML.js', svg=True)


def generate_qr_5():
    generate_qr('planalto.gov.br/ccivil_03/js/includeHTML.js', svg=True, dir='img_qrcode', png=True)


# def clousure_generate_qr(func):
#     @wraps(func)
#     def wrapper(*args, **kwargs):
#         regex = re.compile(
#             r'^(?:http|ftp)s?://'  # http:// or https://
#             r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
#             r'localhost|'  # localhost...
#             r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
#             r'(?::\d+)?'  # optional port
#             r'(?:/?|[/?]\S+)$', re.IGNORECASE)
#         link = None
#         if args:
#             link = args.__getitem__(0) if re.match(regex, args.__getitem__(0)) else None
#             print(link)
#         if not link:
#             link = kwargs.get('url')
#
#         return func
#     return wrapper


# generate_qr_2 = clousure_generate_qr(generate_qr)


def generate_qr_6():
    codigos ={
        "Código Civil": "http://www.planalto.gov.br/ccivil_03/Leis/2002/L10406.htm",
        "Código de Processo Civil": "http://www.planalto.gov.br/ccivil_03/_ato2015-2018/2015/lei/L13105.htm",
        "Código Penal": "http://www.planalto.gov.br/CCIVIL_03/Decreto-Lei/Del2848.htm",
        "Código de Processo Penal": "http://www.planalto.gov.br/ccivil_03/Decreto-Lei/Del3689.htm",
        "Código Tributário Nacional": "http://www.planalto.gov.br/ccivil_03/Leis/L5172.htm",
        "Consolidação das Leis do Trabalho": "http://www.planalto.gov.br/ccivil_03/Decreto-Lei/Del5452.htm",
        "Código de Defesa do Consumidor": "http://www.planalto.gov.br/ccivil_03/Leis/L8078.htm",
        "Código de Trânsito Brasileiro": "http://www.planalto.gov.br/ccivil_03/Leis/L9503.htm",
        "Código Eleitoral": "http://www.planalto.gov.br/ccivil_03/Leis/L4737.htm",
        "Código Florestal": "http://www.planalto.gov.br/ccivil_03/_Ato2011-2014/2012/Lei/L12651.htm",
        "Código de Águas": "http://www.planalto.gov.br/ccivil_03/decreto/D24643.htm",
        "Código de Minas": "http://www.planalto.gov.br/ccivil_03/Decreto-Lei/Del0227.htm",
        "Código Penal Militar": "http://www.planalto.gov.br/ccivil_03/Decreto-Lei/Del1001.htm",
        "Código de Processo Penal Militar": "http://www.planalto.gov.br/ccivil_03/Decreto-Lei/Del1002.htm",
        "Código Brasileiro de Aeronáutica": "http://www.planalto.gov.br/ccivil_03/Leis/L7565.htm",
        "Código Brasileiro de Telecomunicações": "http://www.planalto.gov.br/ccivil_03/Leis/L4117.htm",
        "Código Comercial": "http://www.planalto.gov.br/ccivil_03/Leis/LIM/LIM556.htm",
    }
    for c in codigos.values():
        generate_qr(c, svg=False, dir='qr_codigos', png=True)


def generate_qr_7():
    codigos ={
        "Código Civil": "http://www.planalto.gov.br/ccivil_03/Leis/2002/L10406.htm",
        "Código de Processo Civil": "http://www.planalto.gov.br/ccivil_03/_ato2015-2018/2015/lei/L13105.htm",
        "Código Penal": "http://www.planalto.gov.br/CCIVIL_03/Decreto-Lei/Del2848.htm",
        "Código de Processo Penal": "http://www.planalto.gov.br/ccivil_03/Decreto-Lei/Del3689.htm",
        "Código Tributário Nacional": "http://www.planalto.gov.br/ccivil_03/Leis/L5172.htm",
        "Consolidação das Leis do Trabalho": "http://www.planalto.gov.br/ccivil_03/Decreto-Lei/Del5452.htm",
        "Código de Defesa do Consumidor": "http://www.planalto.gov.br/ccivil_03/Leis/L8078.htm",
        "Código de Trânsito Brasileiro": "http://www.planalto.gov.br/ccivil_03/Leis/L9503.htm",
        "Código Eleitoral": "http://www.planalto.gov.br/ccivil_03/Leis/L4737.htm",
        "Código Florestal": "http://www.planalto.gov.br/ccivil_03/_Ato2011-2014/2012/Lei/L12651.htm",
        "Código de Águas": "http://www.planalto.gov.br/ccivil_03/decreto/D24643.htm",
        "Código de Minas": "http://www.planalto.gov.br/ccivil_03/Decreto-Lei/Del0227.htm",
        "Código Penal Militar": "http://www.planalto.gov.br/ccivil_03/Decreto-Lei/Del1001.htm",
        "Código de Processo Penal Militar": "http://www.planalto.gov.br/ccivil_03/Decreto-Lei/Del1002.htm",
        "Código Brasileiro de Aeronáutica": "http://www.planalto.gov.br/ccivil_03/Leis/L7565.htm",
        "Código Brasileiro de Telecomunicações": "http://www.planalto.gov.br/ccivil_03/Leis/L4117.htm",
        "Código Comercial": "http://www.planalto.gov.br/ccivil_03/Leis/LIM/LIM556.htm",
    }
    for c in codigos.items():
        print(c)
        generate_qr(c, svg=False, dir='qrcode_codigos', png=True)


def run(func='generate_qr_'):
    for f in (f'{func}{x}' for x in range(8)):
        try:
            print('===')
            print(f)
            if eval(f).__doc__:
                if 'nonexequi' in eval(f).__doc__.lower():
                    raise NameError('Flag Nonexequi')
                print(eval(f).__doc__)
                print('---')
        except NameError:
            print(sys.exc_info())
        else:
            eval(f)()
            print('---')
        finally:
            print()


if __name__ == '__main__':
    run()
