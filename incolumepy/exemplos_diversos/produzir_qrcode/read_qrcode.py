#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pyqrcode
import sys
import re
import os
# import qrcode
# import qrtools
import inspect
from unicodedata import normalize
from functools import wraps
import PIL
from PIL import Image

from pyzbar import pyzbar


dir_output = __file__.replace('.py', '')


def file_output(s: str):
    os.makedirs(dir_output, exist_ok=True)
    return os.path.join(dir_output, s)


files = (os.path.join(p, f) for p, _, files in os.walk(dir_output) for f in files)


def qr_read_0():
    iqr = pyqrcode.create('http://blog.incolume.com.br')
    iqr.png(file_output(f'{inspect.stack()[0][3]}.png'))


def qr_read_1():
    r = pyzbar.decode(Image.open(next(files)))
    print(r)


def qr_read_2():
    '''read qrcode vcard'''
    file = 'britodfbr_qr.jpeg'
    assert os.path.isfile(file), f'Ops: {file}'
    r=pyzbar.decode(Image.open(file))
    print(r)


def qr_read_3():
    '''read all qrcode'''
    outputdir = 'exemplos_doc_oficial'
    files = [os.path.join(p, f) for p, _, files in os.walk(outputdir) for f in files if f.endswith(r'.png')]
    for x in files:
        print(f"{x}: {pyzbar.decode(Image.open(x))}")


def run(func='qr_read_'):
    for f in (f'{func}{x}' for x in range(4)):
        try:
            print('===')
            print(f)
            if eval(f).__doc__:
                if 'nonexequi' in eval(f).__doc__.lower():
                    raise NameError('Flag Nonexequi')
                print(eval(f).__doc__)
                print('---')
            eval(f)()
        except NameError:
            print(sys.exc_info())
        except (AttributeError, TypeError) as e:
            print(e)
        else:
            print('---')
        finally:
            print()


if __name__ == '__main__':
    run()
