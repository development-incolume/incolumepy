def soma_lista0(lista=list()):
    '''

    :param lista:
    :return:
    >>> soma_lista0([1,2,3])
    6
    >>> soma_lista0([1,2,3,4])
    10
    >>> soma_lista0([1,2,3,4,5,6])
    21
    '''
    try:
        return lista.pop() + soma_lista0(lista)
    except IndexError:
        return 0


def soma_lista1(lista=list()):
    '''

    :param lista:
    :return:
    >>> soma_lista1([1,2,3])
    6
    >>> soma_lista1([1,2,3,4])
    10
    >>> soma_lista1([1,2,3,4,5,6])
    21
    '''
    if len(lista) >= 1:
        return lista.pop() + soma_lista1(lista)
    else:
        return 0


def soma_lista(lista):
    '''

    :param lista:
    :return:
    >>> soma_lista([1,2,3])
    6
    >>> soma_lista([1,2,3,4])
    10
    >>> soma_lista([1,2,3,4,5,6])
    21
    '''
    if len(lista) == 1:
        return lista[0]
    else:
        return lista[0] + soma_lista(lista[1:])

def soma0(lista):
    '''
    soma os elementos de uma lista
    :param lista:
    :return: int
    >>> soma0([1,2,3])
    6
    >>> soma0([1,2,3,4])
    10
    >>> soma0([1])
    1
    '''
    try:
        return lista[0] + soma0(lista[1:])
    except:
        return 0


if __name__ == '__main__':
    import doctest

    doctest.testmod()
    print()

    soma_lista([1, 2, 3, 4, 5])

