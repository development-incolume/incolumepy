"""
descompactação de dicionarios
"""

def parrot0(voltage, state='a stiff', action='voom'):
    '''
    String concat method

    :param voltage:
    :param state:
    :param action:
    :return:
    >>> d = {"voltage": "four million", "state": "bleedin' demised", "action": "VOOM"}
    >>> parrot0(**d)
    -- This parrot wouldn't VOOM if you put four million volts through it. E's bleedin' demised !
    '''

    print("-- This parrot wouldn't", action, end=' ')
    print("if you put", voltage, "volts through it.", end=' ')
    print("E's", state, "!")


def parrot1(voltage, state='a stiff', action='voom'):
    '''
    String format method

    :param voltage: string
    :param state: string
    :param action: string
    :return: None
    This function print a string with this frase "-- This parrot wouldn't {} if you put {} volts through it. E's {} !"
    >>> a = {'voltage':'wow', 'state':'yahoo', 'action':'vish'}
    >>> parrot1(**a)
    -- This parrot wouldn't vish if you put wow volts through it. E's yahoo!

    '''
    print("-- This parrot wouldn't {} "
          "if you put {} volts through it. "
          "E's {}!".format(action, voltage, state))


def parrot2(voltage, state='a stiff', action='voom'):
    '''
    String ??? method
    :param voltage: string
    :param state: string
    :param action: string
    :return: None
    This function print a string with this frase "-- This parrot wouldn't {} if you put {} volts through it. E's {} !"
    >>> a = {'voltage':'wow', 'state':'yahoo', 'action':'vish'}
    >>> parrot2(**a)
    -- This parrot wouldn't vish if you put wow volts through it. E's yahoo!
    '''
    print("-- This parrot wouldn't %s "
          "if you put %s volts through it. "
          "E's %s!" % (action, voltage, state))

def parrot3(voltage, state='a stiff', action='voom'):
    '''
    String ??? method

    :param voltage: string
    :param state: string
    :param action: string
    :return: None
    This function print a string with this frase "-- This parrot wouldn't {} if you put {} volts through it. E's {} !"
    >>> a = {'voltage':'wow', 'state':'yahoo', 'action':'vish'}
    >>> parrot3(**a)
    -- This parrot wouldn't vish if you put wow volts through it. E's yahoo!

    '''
    print(F"-- This parrot wouldn't {action} "
          F"if you put {voltage} volts through it. "
          F"E's {state}!")


if __name__ == '__main__':
    import doctest

    doctest.testmod()
    d = {"voltage": "four million", "state": "bleedin' demised", "action": "VOOM"}
    parrot1(**d)
    parrot0(**d)
