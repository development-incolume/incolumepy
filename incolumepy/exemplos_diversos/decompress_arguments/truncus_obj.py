from collections import namedtuple

import names

Obj = namedtuple('Obj', 'code pnome unome')

lobj = []
for i in (range(10)):
    a = Obj(i + 1, names.get_first_name(), names.get_last_name())
    lobj.append(a)

for i, obj in enumerate(lobj):
    print(i, obj)
    print(tuple(obj))

##decompactaçao de argumentos em classe
for obj in lobj:
    print('\n\nCod: {} \nNome: {} \nSobrenome: {}'.format(*tuple(obj)))

