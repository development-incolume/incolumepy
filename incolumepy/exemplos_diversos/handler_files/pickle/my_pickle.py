import sys, gzip, pickle, os

class MyPickle:
    GZIP_MAGIC = b'\x1F\x8B'

    def import_pickle(self, filename):
        fh = None
        try:
            fh = open(filename, 'rb')
            magic = fh.read(len(GZIP_MAGIC))
            if magic == GZIP_MAGIC:
                fh.close()
                fh = gzip.open(filename, 'rb')
            else:
                fh.seek(0)
            self.clear()
            self.update(pickle.loads(fh))
            return True
        except (EnvironmentError, pickle.UnpicklingError) as e:
            print("{0}: import error: {1}".format(os.path.basename(sys.argv[0]), e))
            return False
        except:
            sys.exc_info()
        finally:
            if fh is not None:
                fh.close()

    def export_picle(self, filename, compress=False):
        fh = None
        try:
            if compress:
                fh = gzip.open(filename, "wb")
            else:
                fh = open(filename, 'wb')
            pickle.dump(self, fh, pickle.HIGHEST_PROTOCOL)
            return True
        except (EnvironmentError, pickle.UnpicklingError) as e:
            print("{0}: import error: {1}".format(os.path.basename(sys.argv[0]), e))
            return False
        finally:
            if fh is not None:
                fh.close()

if __name__ == '__main__':
    pass
    MyPickle().export_picle(filename='')
    MyPickle().import_pickle(filename='')
