'''
http://stackoverflow.com/questions/25944410/using-configparser-for-text-read-from-url-using-urllib2
'''
try:
    from configparser import ConfigParser
    from configparser import SafeConfigParser


except:
    from ConfigParser import ConfigParser
    from ConfigParser import SafeConfigParser


import sys
import urllib.request
import io


class Handler_configparser:
    DEBUG = 0
    def __init__(self, DEBUG=False, pathdefault=''):
        Handler_configparser.DEBUG = DEBUG
        Handler_configparser.pathdefault = pathdefault

    def confiparser_read_web(self, url):
        try:
            data = urllib.request.urlopen(url).read().decode()
            if Handler_configparser.DEBUG: print(data)
            if Handler_configparser.DEBUG: print(type(data))
            handler = ConfigParser()

            #http://stackoverflow.com/questions/25944410/using-configparser-for-text-read-from-url-using-urllib2
            stream = io.StringIO(data)
            stream.seek(0)
            handler.read_file(stream)

            if Handler_configparser.DEBUG: print('-')
            if Handler_configparser.DEBUG: print(handler.sections())
            for section in handler.sections():
                print('[%s]' % section)
                for option in handler.options(section):
                    print('%s ='%option, handler.get(section, option))

        except:
            print(sys.exc_info())

    def configparser_read(self, filename):
        try:
            dlist= dict()
            handler = ConfigParser()
            handler.read(filename)

            for section in handler.sections():
                dic = {}
                if Handler_configparser.DEBUG: print(section)
                for option in handler.options(section):
                    if Handler_configparser.DEBUG: print(section, option, handler.get(section, option))
                    dic[option] = handler.get(section, option)
                    dlist[section] = dic
            print(dlist)
            return dlist
            pass
        except:
            print(sys.exc_info())


    def configparser_write(self, filename, values=''):
        try:
            handler = ConfigParser()

            for section in values.keys():
                if Handler_configparser.DEBUG: print(section)
                handler.add_section(section)
                for option in values[section].keys():
                    if Handler_configparser.DEBUG: print(section, option, values[section][option])
                    handler.set(str(section), str(option), str(values[section][option]))

            #file = open(filename,'w')
            #handler.write(file)
            #file.close()

            with open(filename, 'w') as arquivo:
                handler.write(arquivo)
            return True
            pass
        except:
            print(sys.exc_info())

    def teste01(self):
        dlist = {'server01': {'ip': '10.1.1.1', 'port': 8080},
                 'server02': {'ip': '10.1.1.2', 'port': 8080},
                 'server03': {'ip': '10.1.1.3', 'port': 8080},
                 'server04': {'ip': '10.1.2.255', 'port': 80, 'path': '/'},
                 }

        if Handler_configparser.DEBUG:
            Handler_configparser().configparser_write('data/servidores.cfg',dlist)
            Handler_configparser().configparser_write('data/servidores.ini', dlist)
            Handler_configparser().configparser_read('data/servidores.ini')
            Handler_configparser().configparser_read('data/servidores.cfg')
            Handler_configparser().configparser_read('../curso_codecademy/incolumepy/ini/config.ini')
            Handler_configparser(True).configparser_read('data/config.ini')

        Handler_configparser().confiparser_read_web(
            'https://gitlab.com/development-incolume/incolumepy.data/raw/master/config.ini')
        pass

    @classmethod
    def main(cls):
        cls.teste01(cls)

if __name__ == '__main__':
    pass
    Handler_configparser.main()