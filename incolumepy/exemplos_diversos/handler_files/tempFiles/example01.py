# https://stackoverflow.com/questions/10501247/best-way-to-generate-random-file-names-in-python
# https://stackoverflow.com/questions/2961509/python-how-to-create-a-unique-file-name
import tempfile

# tempfile.NamedTemporaryFile(mode='w+b', buffering=-1, encoding=None, newline=None, suffix=None, prefix=None, dir=None, delete=True)

tf = tempfile.NamedTemporaryFile()
print(tf.name)

tf = tempfile.NamedTemporaryFile(mode='w+b', buffering=-1, encoding=None,
                                 newline=None, suffix=None, prefix=None, dir=None, delete=True)
print(tf.name)

tf = tempfile.NamedTemporaryFile(suffix='.tmp')
print(tf.name)

tf = tempfile.NamedTemporaryFile(suffix='.tmp', prefix='file_')
print(tf.name)

tf = tempfile.NamedTemporaryFile(suffix='.tmp', prefix='zz', dir='.', delete=True)
print(tf.name)
