#!/usr/bin/env Python
#-*- coding:utf-8 -*-
'''
Importe de bibliotecas
'''
import os, shutil

def createtree(path):
    '''
    criar estrutura de diretórios
    '''
    #os.system('mkdir -pv /tmp/env/; cd /tmp/env/; for ((i=1;i<=2;i++)); do for ((j=1;j<=2;j++)); do mkdir -p dir$i/subdir$j; touch dir$i/subdir$i/file$j; touch file$i; done; done')
    directory = ''
    try:
        for i in range(1,3):
            for j in range(1,3):
                #print i,j
                directory = path+'/dir'+str(i)+'/subdir'+str(j)
                if not os.path.exists(directory):
                    os.makedirs(directory)
                with open(directory+'/file'+str(j)+".txt","w+") as f:
                    f.write(str(i))
    except BaseException as e:
        return errormsg(e)
    else:
        return 'Estrutura %s criada com sucesso.' % path

def run(cmd):
    os.system(cmd)

def errormsg(erro):
    return "Ops erro: %s" % erro

def checkask(question):
    ask=''
    options=['y','yes','s','sim','n','não','no','nao']
    while True:
        if ask not in options:
            ask = input(question).lower()
        else:
            break
        if ask in options:
            return True
        else:
            return False

def rm(path):
    '''
     Remove arquivos e diretório do sistema de arquivos
    '''
    try:
        if os.path.exists(path) and os.access(path, os.R_OK) and os.access(path, os.W_OK):
            if os.path.isfile(path):
                os.remove(path)
            if os.path.isdir(path):
                os.rmdir(path)
        else:
            raise OSError('Arquivo "%s" inexistente ou sem acesso!' % path)
    except OSError as detalhes:
        if detalhes.errno == 39:
            #print errormsg(detalhes)
            check = checkask('%s é uma arvore de diretórios. Deseja remover todo o conteúdo?(y/n)' % path)
            if check:
                shutil.rmtree(path)
                print ("%s, removido com sucesso!" % path)
            else:
                print ("Operação cancelada.")
        else:
            print (errormsg(detalhes))
    except:
        raise
    else:
        print ("%s, removido com sucesso!" % path)

def show():
    '''
    criar a arvore /tmp/env
    '''
    print (createtree('/tmp/env'))

    '''
    navegar para /tmp/env
    '''
    print ("comando executado >>> os.chdir('/tmp/env')")
    os.chdir('/tmp/env')

    '''
    Exibir conteúdo
    '''
    print ("Saída comando >>> os.system('dir')")
    os.system('dir')

    print ("\nSaída comando >>> os.system('ls')")
    os.system('ls')

    print ("\nSaída comando >>> os.system('tree')")
    os.system('tree')

def rmdir(path):
    '''
    remover diretórios vazios
    '''
    try:
        os.rmdir(path)
    except OSError as detalhes:
        print ("Ops: %s" % detalhes)
    except:
        raise
    else:
        print ("executado com sucesso!")


def rmtree(path):
    '''
    remover arvore de diretórios
    '''
    try:
        os.rmdir(path)
    except OSError as detalhes:
        print ("Ops: %s" % detalhes)
    except:
        raise
    else:
        print ("executado com sucesso!")


def rmtree0():
    print ("\nExecutando comando >>> os.rmdir('dir2/subdir1')")
    try:
        os.rmdir('dir2/subdir1')
    except OSError as detalhes:
        print ("Ops: %s" % detalhes)
    except:
        raise
    else:
        print ("executado com sucesso!")

    print ("\nSaída comando >>> os.system('tree')")
    os.system('tree')

    print ("\nExecutando comando >>> os.rmdir('dir2/subdir2')")
    try:
        os.rmdir('dir2/subdir2')
    except OSError as detalhes:
        print ("Ops: %s" % detalhes)
    except:
        raise
    else:
        print ("executado com sucesso!")

def rmfile(file):
    '''
    remover arquivos
    '''

    print ("\nExecutando comando >>> os.rmdir('dir2/subdir2/file2')")
    try:
        os.remove(file)
    except OSError as detalhes:
        print ("Ops: %s" % detalhes)
    except:
        raise
    else:
        print ("executado com sucesso!")

def deleteall():
    '''
    remover arvore de arquivos
    '''
    print ("\nExecutando comando >>> shutil.rmtree('dir2/')")
    try:
        shutil.rmtree('dir2/')
    except OSError as detalhes:
        print ("Ops: %s" % detalhes)
    except:
        raise
    else:
        print ("executado com sucesso!")

    print ("\nSaída comando >>> os.system('tree')")
    os.system('tree')

    os.system('cd ..')

    print ("\nExecutando comando >>> shutil.rmtree('/tmp/env/')")
    try:
        shutil.rmtree('/tmp/env/')
    except OSError as detalhes:
        print ("Ops: %s" % detalhes)
    except:
        raise
    else:
        print ("executado com sucesso!")

##print checkask('question?')
#rm('env/dir2')
#rm('env/dir1/subdir2/file2.tx')
#rm('env/dir1/subdir2/file2.txt')
#rm('env/dir1/subdir2/')
#rm('env/file3.txt')
#rm('env/')

show()
