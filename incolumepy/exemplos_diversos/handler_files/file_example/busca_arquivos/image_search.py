import os

def getExtension(filename):
   fileName, fileExtension = os.path.splitext(filename)
   return fileExtension

def inArray(array, to_look):
    for x in array:
        if to_look[1:] == x:
            return True
    return False

def isImage(filename):
   extensions = ['jpeg', 'jpg', 'jpe', 'tga', 'gif', 'tif', 'bmp', 'rle', 'pcx', 'png', 'mac', 'pnt', 'pntg', 'pct', 'pic', 'pict', 'qti', 'qtif']
   extension = getExtension(filename)
   if inArray(extensions, extension):
       return True
   return False

def lookupDirectory(directory):
   arquivos = os.listdir(os.path.expanduser(directory))
   for arquivo in arquivos:
       #if isImage(arquivo):
           #print arquivo
       if os.path.isdir(directory + arquivo) == True:
           arquivo = directory + arquivo
           print (arquivo)
           lookupDirectory(arquivo)

if __name__ == '__main__':
    lookupDirectory('/home/brito')
