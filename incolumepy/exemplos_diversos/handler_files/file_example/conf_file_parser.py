import configparser


def write_config(filename='conf/example.ini'):
    config = configparser.ConfigParser()
    config['DEFAULT'] = {'ServerAliveInterval': '45',
                         'Compression': 'yes',
                         'CompressionLevel': '9'}
    config['bitbucket.org'] = {}
    config['bitbucket.org']['User'] = 'hg'
    config['topsecret.server.com'] = {}
    topsecret = config['topsecret.server.com']
    topsecret['Port'] = '50022'     # mutates the parser
    topsecret['ForwardX11'] = 'no'  # same here
    config['DEFAULT']['ForwardX11'] = 'yes'

    with open(filename, 'w') as configfile:
        config.write(configfile)


def read_config(filename='conf/example.ini'):
    config = configparser.ConfigParser()
    config.sections()
    config.read(filename)
    config.sections()
    print('>>','bitbucket.org' in config)
    print('>>', 'bytebong.com' in config)
    config['bitbucket.org']['User']
    config['DEFAULT']['Compression']
    topsecret = config['topsecret.server.com']
    print(topsecret['ForwardX11'])
    print(topsecret['Port'])
    for key in config['bitbucket.org']:
        print(key)
    print(config['bitbucket.org']['ForwardX11'])


def if_present():
    try:
        from ConfigParser import SafeConfigParser
    except:
        from configparser import SafeConfigParser

    parser = SafeConfigParser()
    parser.read('conf/multisection.ini')

    for candidate in ['wiki', 'bug_tracker', 'dvcs']:
        print('%-12s: %s' % (candidate, parser.has_section(candidate)))



def read_config(fileconfig='../config.ini', DEBUG=False):
    try:
        dic = dict()
        handler = configparser.ConfigParser()
        handler.read(fileconfig)
        for section in handler.sections():
            for option in handler.options(section):
                print(section, option, handler.get(section, option))


        print(handler._sections)
        print(handler._sections['bancodedados'])
        print(handler.get('bancodedados', 'type'))
        print(handler.get('bancodedados', 'user'))
        print(handler.get('bancodedados', 'bd'))

    except Exception as e:
        if DEBUG:
            raise e
        print(e)
    pass

if __name__ == '__main__':
    write_config()
    read_config()
    if_present()
    read_config()