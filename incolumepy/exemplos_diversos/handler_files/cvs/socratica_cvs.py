'''

'''
def read_cvs_file(filename):
    path = filename
    file = open(path)
    for line in file:
        print(line)

def read_lines(filename):
    path = filename
    lines = [line for line in open(path)]

    #all lines
    for l in lines:
        print(l)

def read_lines0(filename):
    path = filename
    lines = [line for line in open(path)]

    #all lines
    for line in lines:
        print(line.strip())

def read_lines1(filename):
    path = filename
    lines = [line for line in open(path)]

    #com quebra de linha
    print(lines[0])

    #sem quebra de linha
    print(lines[0].strip())

    #separação de campos
    print(lines[0].strip().split(','))

def read_lines2(filename):
    dataset = [ line.strip().split(',') for line in open(filename)]
    print(dataset[1])

def menu():
    DEBUG = 1
    if DEBUG:
        read_cvs_file('cvsFiles/filmes.cvs')
        read_cvs_file('cvsFiles/medias.cvs')

        read_lines('cvsFiles/medias.cvs')
        read_lines0('cvsFiles/medias.cvs')
        read_lines1('cvsFiles/medias.cvs')
        read_lines2('cvsFiles/filmes.cvs')

if __name__ == '__main__':
    menu()