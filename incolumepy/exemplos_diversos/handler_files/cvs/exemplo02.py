__author__ = '@britodfbr'
'''
https://br.ccm.net/faq/10376-python-ler-e-gravar-arquivos-csv
'''
import csv
meu_arq = "MEUARQUIVO.csv"
def write_csv(file=meu_arq):
    c = csv.writer(open(file, "w"))
    c.writerow(["Nome","Endereço","Telefone","Fax","E-mail","Outros"])

def read_csv(file=meu_arq):
    c = csv.reader(open(file, "r"))
    print(type(c))

    for i in c:
        print(i)

    for row in c:
        print(row[2], row[-2])

if __name__ == '__main__':
    read_csv()
