import subprocess
from datetime import datetime, date, time
import csv


def write_lines13(filename):
    row=[]
    line=[]
    header = ["Pemissões", "link", "user", "group", "size", "date", "fuso", "name"]
    output = subprocess.getoutput('ls --full-time cvsFiles/')
    var = [n for n in output.split('\n')]

    for i in range(len(var)):
        if i > 0:
            row.append(var[i].strip().split())

    for r in row:
        r[6] = r[6].split('.')[0]
        #r[5] = r[5] + " "+ r[6] + " " + r[7]
        r[5] = r[5] + " " + r[6]
        r[6] = r[8]
        del r[8]
        del r[7]




    for r in row:
        s = ','.join(r)
        print(s)

    with open(filename,'w+', newline='') as csvfile:
        #spamwriter = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)
        #spamwriter = csv.writer(csvfile, delimiter=',', quotechar=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter = csv.writer(csvfile)

        spamwriter.writerow(header)
        for l in row:
            spamwriter.writerow(l)
    pass

def read_lines5(filename):
    try:
        file = open(filename, newline='')
        reader = csv.reader(file)
        header = next(reader) #1ª linha header
        data =  [] # demais linhas
        for row in reader:
            permition = row[0]
            link = int(row[1])
            user = row[2]
            group = row[3]
            size = int(row[4])
            date = datetime.strptime(row[5], '%Y-%m-%d %H:%M:%S')
            name =row[6]

            data.append([permition, link, user, group, size, date, name])

        print(header)
        for d in data:
            print(d)

    except Exception as e:
        print(e)
        raise e

def menu():
    DEBUG = 1
    if DEBUG:
        print('write')
        write_lines13('cvsFiles/lista13.cvs')
        print('read')
        read_lines5(('cvsFiles/lista13.cvs'))


if __name__ == '__main__':
    menu()
