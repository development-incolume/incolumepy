__author__ = '@britodfbr'
'''
https://br.ccm.net/faq/10376-python-ler-e-gravar-arquivos-csv
'''
import csv
meu_arq = "arq.csv"
def write_csv(file=meu_arq):
    c = csv.writer(open(file, "w"), delimiter=';')
    c.writerow(["Nome","Endereço","Telefone","Fax","E-mail","Outros, documentos, referência"])

def write_csv0(file='meu_arq.csv'):
    with open(file, 'w') as csvfile:
        content = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL, quotechar='|', delimiter=';')
        content.writerow([1,2,3,4,5,6])

def write_csv1(file='eggs0.csv'):
    with open(file, 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ',
                                quotechar='"', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(['Spam', 'Lovely Spam', 'Wonderful Spam'])


def read_csv(file=meu_arq):
    c = csv.reader(open(file, "r"), delimiter=';')
    print(type(c))
    c = list(c)
    for i in c:
        print(i)

    for row in c:
        print(row[2], row[-2])

def read_csv0(file='eggs.csv', delimiter=','):
    with open(file) as csvfile:
        content = csv.reader(csvfile, delimiter=delimiter)
        c = list(content)
        for i in c:
            print(i)

if __name__ == '__main__':
    write_csv()
    read_csv()
    write_csv0()
    read_csv0(delimiter='|')
    write_csv1()