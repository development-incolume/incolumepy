import json
from collections import namedtuple

def function01():
    jsonData = '{"name": "Frank", "age": 39}'
    jsonToPython = json.loads(jsonData)
    print(jsonToPython)

def function02():
    pythonDictionary = {'name':'Bob', 'age':44, 'isEmployed':True}
    dictionaryToJson = json.dumps(pythonDictionary)
    print(dictionaryToJson)

Emp = namedtuple('Emp', 'name age')
#Emp.tojson = json.dumps(Emp._asdict())
#Emp.__dict__ = Emp._asdict()

class Employee:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def todict(self):
        return self.__dict__

    def totuple(self):
        return (tuple(self.__dict__), tuple(self.__dict__.values()))

    def tojson(self):
        return json.dumps(self.__dict__)


if __name__ == '__main__':
    function01()
    function02()

    e = Employee('Bob', 32)
    print(e.totuple())
    print(e.tojson())
    print(e.todict())

    a = Emp('Luc', 10)

    print(json.dumps(a._asdict()))

