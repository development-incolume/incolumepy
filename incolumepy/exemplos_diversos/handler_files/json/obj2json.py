import json
import names
import random
import os
from incolumepy.utils.files import realfilename
class Employee:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def tojson(self):
        return json.dumps(self.__dict__)


def run():
    d = {}
    m = []
    f = []
    for i in range(15):
        gender = random.choice(['male', 'female'])
        person = Employee(names.get_full_name(gender), random.randint(20, 55))
        if gender == 'male':
            m.append(person.tojson())
        else:
            f.append(person.tojson())
    d['male'] = m
    d['female'] = f
    print(d)
    with open(realfilename(os.path.basename(__file__), ext='json')) as file:
        json.dump(file, d, indent=2)



if __name__ == '__main__':
    run()