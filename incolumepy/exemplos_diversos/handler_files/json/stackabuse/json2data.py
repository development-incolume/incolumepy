import json


def func():
    with open('data.json') as json_file:
        data = json.load(json_file)
        for p in data['people']:
            print('Name: ' + p['name'])
            print('Website: ' + p['website'])
            print('From: ' + p['from'])
            print('')


def func2(filename='data1.json'):
    with open(filename) as jf:
        data = json.load(jf)
        print(data)
        for person in data['people']:
            print(person)
            print(person.values())
            print(*person.values())


if __name__ == '__main__':
    func()
    func2()
