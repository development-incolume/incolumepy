import json
from collections import namedtuple

def other():
    Emp = namedtuple('Emp', ['name', 'age'])('None','None')
    __dict__ = Emp._asdict()
    #Emp.tojson = json.dumps(__dict__)
    a = Emp('Brito', 10)
    b = Emp()
    # print(b)
    print('namedtuple:', __dict__)
    # print('json dump:', a.tojson())

def other01():
    Cal = namedtuple('Calendar', 'date event horario')
    c = Cal('2018-04-08', 'IPB', '19:00')
    print(c)
    print(c._asdict())
    print(json.dumps(c._asdict()))

if __name__ == '__main__':
    #other()
    other01()
