import json
from decimal import Decimal

str_json='''{
    "persons": [
        {
            "city": "Seattle",
            "name": "Brian",
            "telefones": ["5555-5555","9555-5555","3555-5555"],
            "altura": 1.73,
            "data": "12-12-2016"
        },
        {
            "city": "Amsterdam",
            "name": "David",
            "telefones": ["5555-5551","9555-5551","3555-5551"],
            "altura": 1.78,
            "data": "12-01-2016"
        }
    ]
}'''

def exemplo01():
    '''Convert JSON to Python Object (Dict)'''
    data_json = json.loads(str_json)
    print(data_json)

def exemplo02():
    '''Convert JSON to Python Object (List)'''
    array = '{"drinks": ["coffee", "tea", "water"]}'
    data = json.loads(array)

    for element in data['drinks']:
        print(element)

def exemplo03():
    data = json.loads(str_json)
    for element in data['persons'][0]['telefones']:
        print(element)
    pass


def exemplo04():
    jsondata = '{"number": 1.573937639}'

    x = json.loads(jsondata, parse_float=Decimal)
    print (x['number'])

def exemplo05():
    data = json.loads(str_json, parse_float=Decimal)
    print(type(data['persons'][0]['altura']), data['persons'][0]['altura'])

def exemplo06():
    try:
        decoded = json.loads(str_json)

        # Access data
        for x in decoded['persons']:
            print(x['name'])

    except (ValueError, KeyError, TypeError):
        print("JSON format error")

def exemplo07():
    d = {}
    d["Name"] = "Luke"
    d["Country"] = "Canada"

    print(json.dumps(d, ensure_ascii=False))
    # result {"Country": "Canada", "Name": "Luke"}


def exemplo08():
    json_data = '{"name": "Brian", "city": "Seattle"}'
    python_obj = json.loads(json_data)
    print(json.dumps(python_obj, sort_keys=True, indent=4))

def exemplo09():
    obj = json.loads(str_json)
    print(json.dumps(obj, sort_keys=True, indent=4))

def exemplo10():
    obj = json.loads(str_json)
    with open('jsonfiles/exemplo10.json', 'w') as outfile:
        json.dump(obj, outfile)

def exemplo11():
    try:
        f = open('jsonfiles/exemplo10.json')
        obj = json.load(f)
        print(json.dumps(obj, sort_keys=True, indent=2))

        for o in obj['persons']:
            print(o)

        f.close()

    except Exception as e:
        print(e)

def exemplo12():
    obj = json.loads(str_json)
    with open('jsonfiles/exemplo12.json', 'w') as outfile:
        json.dump(obj, outfile, sort_keys=True, indent=2)

def menu():
    exemplo01()
    exemplo02()
    exemplo03()
    exemplo04()
    exemplo05()
    exemplo06()
    exemplo07()
    exemplo08()
    exemplo09()
    exemplo10()
    exemplo11()
    exemplo12()


if __name__ == '__main__':
    menu()
    pass