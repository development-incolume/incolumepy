import json
from collections import namedtuple
import locale

x = namedtuple('_', ['language', 'country', 'locale'])(
            'pt',
            'pt-BR',
            locale.getlocale()[0]
        )

nt = x._asdict()

print('namedtuple:', nt)
# OrderedDict([('language', 'en'), ('country', 'en-GV'), ('locale', 'en_US')])

# Note: if you want to print `nt` as a dict then you'll need to wrap it in a dict(nt) call

z = json.dumps(nt)

print('json dump:', z)