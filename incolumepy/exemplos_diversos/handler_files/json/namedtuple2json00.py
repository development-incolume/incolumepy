import json
from collections import namedtuple
#https://pt.stackoverflow.com/questions/289758/python-serializa%c3%a7%c3%a3o-de-classes-namedtuple-em-json

Emp = namedtuple('Emp', 'name age')
Emp.tojson = lambda self: json.dumps(self._asdict())

a = Emp('Luc', 10)
print(a.tojson())

