def square(n):
    n = int(n)
    return n ** 2


def squarel(list1):
    list2 = []
    for num in list1:
        list2.append(num ** 2)
    return list2


print(squarel([1, 2, 3, 4]))

print('\nMap')
lista = [1, 2, 3, 4]
m = map(lambda x: x ** 2, lista)
print(list(m))

n = map(lambda x: x ** 2, [x for x in range(1, 5)])
print(list(n))

l = [x for x in range(1, 5)]
print(list(map(lambda x: x ** 2, l)))
print(list(map(square, l)))
