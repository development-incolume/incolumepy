import gzip
import os

import names


def gzip_rotator0(source, dest):
    with open(source, 'r') as f_in, gzip.open('{}.gz'.format(dest), 'wb') as f_out:
        compressed = gzip.compress(f_in.read().encode(), 9)
        f_out.write(compressed)
    os.remove(source)


def gzip_rotator1(source, dest):
    '''
    compime origem texto para destino em gzip
    :param source: name txt file
    :param dest: basename destino
    :return: gzip file
    '''
    pass
    with open(source) as fin, open('{}.gz'.format(dest), 'wb') as fout:
        fout.write(gzip.compress(fin.read().encode()))
        return (True, fout.name)
    os.remove(source)


def gzip_rotator2(source, dest):
    pass
    try:
        with gzip.open(source) as fin, gzip.open('{}.gz'.format(dest), 'w') as fout:
            content = fin.read()
            # print(content)
            fout.write(gzip.compress(content))

        # os.remove(source)
    except OSError:
        with open(source) as fin, open('{}.gz'.format(dest), 'wb') as fout:
            fout.write(gzip.compress(fin.read().encode()))
            return (True, fout.name)
        os.remove(source)
    except:
        raise

def gzip_rotator(source, dest):
    pass
    try:
        fin = open(source)
        content = fin.read()
    except:
        print('gzip_rotator')
        raise
    with open('{}.gz'.format(dest), 'wb') as fout:
        fout.write(gzip.compress(content.encode()))


def gen_file(dir='', name='', ext='txt'):
    pass
    try:
        if not name:
            name = os.path.abspath(
                os.path.join(*dir.split(),
                             '{}.{}'.format(os.path.basename(__file__).split('.')[0], ext)))
        with open(name, 'w') as f:
            reg = ''
            for i in range(10):
                reg += 'Cod: {}; Nome: {}; SobreNome: {};\n'.format(i + 1, names.get_first_name(),
                                                                    names.get_last_name())
            f.write(reg)
            return (True, f.name)
    except:
        return (False)


def gzip_read0(filename):
    # fail
    with open(filename) as fin:
        return fin.read()


def gzip_read(filename):
    with gzip.open(filename) as fin:
        return fin.read()

def run():
    pass
    print(gen_file())
    a, source = gen_file()
    zh = gzip_rotator(source, source)
    print(':', zh)
    print(zh[1])
    print(gzip_read(zh[1]))
    print(gzip_read(source))
    #zh1 = gzip_rotator(zh[1], zh[1])


if __name__ == '__main__':
    run()
