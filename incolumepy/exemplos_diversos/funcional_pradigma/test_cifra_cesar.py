import unittest
from cifra_cesar import encrypt, decrypt


class TestCifraCesar(unittest.TestCase):
    def test_encrypt_def_abc(self):
        self.assertEqual('def', encrypt('abc'))

    def test_decrypt_def_abc(self):
        self.assertEqual('abc', decrypt('def'))

    pass


if __name__ == '__main__':
    unittest.main()