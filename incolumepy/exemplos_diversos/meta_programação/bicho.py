#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Bicho:

    def fazer_barulho(self, som, vezes):
        barulho = (som + "!").replace("ar!", "u!")
        return (f"{barulho} " * vezes).strip()

    def latir(self):
        return self.fazer_barulho('au', 2)

    def __getattr__(self, nome):

        def sonar(vezes=1):
            return self.fazer_barulho(nome, vezes)
        return sonar


def run():
    b = Bicho()
    print(b.latir())
    print(b.chilrear())
    print(b.bramir())
    print(b.zumbir())
    print(b.zunzunar())
    print(b.barrir())
    print(b.bramir())
    print(b.crocitar())
    print(b.grasnar())
    print(b.grasnar)


if __name__ == '__main__':
    run()
