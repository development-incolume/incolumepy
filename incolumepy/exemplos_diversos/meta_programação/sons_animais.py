#!/usr/bin/env python
# -*- coding: utf-8 -*-
from functools import wraps


class Bicho:

    def fazer_barulho(self, som, vezes):
        barulho = (som + "!").replace("ar!", "u!")
        return (f"{barulho} " * vezes).strip()

    def ladrar(self):
        return self.latir()

    def latir(self):
        return self.fazer_barulho('au', 2)

    def __getattr__(self, nome):
        @wraps(nome)
        def sonar(vezes=1):
            return self.fazer_barulho(nome, vezes)
        return sonar


def run():
    b = Bicho()
    print(b.latir())
    print(b.chilrear())
    print(b.chilrear)
    print(b.ladrar())
    print(b.coachar())


if __name__ == '__main__':
    run()
