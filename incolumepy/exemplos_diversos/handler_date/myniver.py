from datetime import date, datetime
from time import time


class MyNiver(date):
    @staticmethod
    def data_nascimento():
        return date.fromtimestamp(267159600)

    @staticmethod
    def myniver(year=1978):
        if year < 1978:
            raise ValueError('valor menor que 1978')
        return datetime(int(year), 6, 20, 0, 0).strftime('%s')

    @classmethod
    def epoch(t=''):
        return {
            '': time(),
            '1': date.fromtimestamp(t)
        }.get(t)

    @staticmethod
    def epoch(t=0):
        if t:
            return date.fromtimestamp(t)
        return time()


if __name__ == '__main__':
    print(MyNiver.data_nascimento())
    print(MyNiver.epoch())
    print(MyNiver.epoch(1498139801))
    print(MyNiver.epoch(267181801))

    for i in range(1978, 2018):
        print(i, MyNiver.myniver(i))

    try:
        MyNiver.myniver(1977)
    except Exception as e:
        print(e)
