'''
https://docs.python.org/3/library/functions.html#property
'''


class B:
    '''
    implementação sem o decorador property
    '''

    def __init__(self):
        self._x = None

    def getx(self):
        return self._x

    def setx(self, value):
        self._x = value

    def delx(self):
        del self._x

    x = property(getx, setx, delx, "I'm the 'x' property.")


class C(object):
    '''
        implementação com o decorador property
        '''

    def __init__(self):
        self._x = None

    @property
    def x(self):
        """I'm the 'x' property."""
        print("getter of x called")
        return self._x

    @x.setter
    def x(self, value):
        print("setter of x called")
        self._x = value

    @x.deleter
    def x(self):
        print("deleter of x called")
        del self._x


class D:
    def __init__(self, **kwargs):
        self.__dict__ = kwargs


if __name__ == '__main__':
    b = B()
    print(b.__dict__)
    b.x = 1
    print(b._x)

    print(C)
    c = C()
    print(c.__dict__)
    c.x = 1
    print(c.x)
    del c.x
    print(c.__dict__)

    print(D)
    d = D(a=1)
    print(d.__dict__)

    d1 = D(a=1, b='1', c=True, d={1: 1}, e=[0, 1, 2, 3, 4], f=3.1415, g=(1, 2))
    print(d1.__dict__)
