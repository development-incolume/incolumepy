#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PIL import Image
import inspect
import os
import sys
from functools import wraps


diroutput=os.path.join(__file__.replace('.py', ''))


def file_output(dir_output):
    def inner(s: str):
        os.makedirs(dir_output, exist_ok=True)
        return os.path.join(dir_output, s)
    return inner


fileout = file_output(diroutput)
imgbase = 'apple-touch-icon.png'


def exemplo0(yourImage='apple-touch-icon.png'):
    fileoutput = fileout(f"{inspect.stack()[0][3]}.png")
    yourImage = Image.open(yourImage)
    print(yourImage.size)
    w, h = yourImage.size
    yourImage.crop((0, 30, w, h-30)).save(fileoutput)


def exemplo1():
    fileoutput = fileout(f"{inspect.stack()[0][3]}.png")
    img = Image.open(imgbase)
    img.resize((500, 500)).save(fileoutput)


def exemplo2():
    fileoutput = fileout(f"{inspect.stack()[0][3]}.png")
    img = Image.open(imgbase)
    w, h = img.size
    print(w, h)
    img.resize((int(w*1.6), int(h*1.6))).save(fileoutput)


def exemplo3():
    proportion = 3.5
    fileoutput = fileout(f"{inspect.stack()[0][3]}.png")
    img = Image.open(imgbase)
    w, h = img.size

    print(w, h)
    img.resize((int(w*proportion), int(h*proportion))).save(fileoutput)


def run(func='exemplo'):
    for f in (f'{func}{x}' for x in range(4)):
        try:
            print('===')
            print(f)
            if eval(f).__doc__:
                if 'nonexequi' in eval(f).__doc__.lower():
                    raise NameError('Flag Nonexequi')
                print(eval(f).__doc__)
                print('---')
            eval(f)()
        except NameError:
            print(sys.exc_info())
        except (AttributeError, TypeError) as e:
            print(e)
        else:
            print('---')
        finally:
            print()


if __name__ == '__main__':
    run()