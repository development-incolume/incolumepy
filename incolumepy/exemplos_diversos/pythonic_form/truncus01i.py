from collections import deque

d = {'ricardo': 'blue', 'ana': 'yellow', 'ada': 'green', 'eliana': 'purple'}
names = list(d.keys())
print(names)

#not pythonic
del names[0]
names.pop(0)
names.insert(0, 'brito')
print(names)

names = deque(list(d.keys()))
print(names)
del(names[0])
names.popleft()
names.appendleft('brito')
print(names)