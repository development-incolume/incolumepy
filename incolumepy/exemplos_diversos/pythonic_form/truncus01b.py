d = {'ricardo': 'blue', 'ana': 'yellow', 'ada': 'green', 'eliana': 'purple'}

e = {k: d[k] for k in d if not k.startswith('r')}

nomes = [k for k in d]
cores = [k for k in d.values()]

f = dict(zip(nomes, cores))

g = {x: y for x, y in d.items() if not y.startswith('p')}
h = {x: y for x, y in d.items() if not x.startswith('a')}

print(e)
print(nomes)
print(cores)
print(f)
print(g)
print(h)
