d = {'ricardo': 'blue', 'ana': 'yellow', 'ada': 'green', 'eliana': 'purple'}

while d:
    key, value = d.popitem()
    print('{} --> {}'.format(key, value))