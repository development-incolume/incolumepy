from urllib.request import urlopen
import requests
from functools import lru_cache, wraps


def web_lookup0(url, saved={}):
    if url in saved:
        return saved[url]
    page = urlopen(url).read()
    saved[url] = page
    return page

# modulo cache
@lru_cache(128)
def web_lookup1(url):
    return requests.get(url).content

#decorador personalizado
def cache(func):
    saved={}
    @wraps(func)
    def newfunc(*args):
        if args in saved:
            return newfunc(*args)
        result = func(*args)
        saved[args] = result
        # print(saved)
        return result
    print(saved)
    return newfunc

@cache
def web_lookup(url):
    return requests.get(url).content


print(web_lookup('http://brito.blog.incolume.com.br'))
print()