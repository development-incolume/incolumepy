import unicodedata

import unicodedata_example as ue

frase = u"Klüft skräms inför på fédéral électoral große aço áàãâä! éèêë? íì&#297;îï, óòõôö; úù&#361;ûü."

s = unicodedata.normalize('NFKD', frase).encode('ascii', 'ignore')  # resultado bytes
nfd = unicodedata.normalize('NFD', frase)
nfc = unicodedata.normalize('NFC', frase)
nfkd = unicodedata.normalize('NFKD', frase)
nfkc = unicodedata.normalize('NFKC', frase)

if __name__ == '__main__':
    print(ue.frase)
    print(s.decode('ascii'))  # resultado str

    print('-' * 20)
    # ignore pula caracteres não elegiveis dentro do ascii
    print(nfkd.encode('ascii', 'ignore').decode('ascii'))

    print(nfd.encode('ascii', 'ignore').decode('ascii'))
    print(nfc.encode('ascii', 'ignore').decode('ascii'))
    print(nfkc.encode('ascii', 'ignore').decode('ascii'))
