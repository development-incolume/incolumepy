def encode(palavra):
    result = []
    for i in palavra:
        result.append(chr(ord(i) + 30000))
    return ''.join(result)


def decode(palavra):
    result = []
    for i in palavra:
        result.append(chr(ord(i) - 30000))
    return ''.join(result)


def fencode(palavra):
    return ''.join([chr(ord(x) + 30000) for x in palavra])


def fdecode(palavra):
    return ''.join([chr(ord(x) - 30000) for x in palavra])


if __name__ == '__main__':
    print(encode('teste'))
    print(decode('疤疕疣疤疕'))

    print(fencode('Ricardo'))
    print(fdecode('疂疙疓疑疢疔疟'))
    print(fdecode('疤疕疣疤疕'))
