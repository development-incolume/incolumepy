import unicodedata

'''
https://en.wikipedia.org/wiki/Unicode_equivalence
'''
frase = u"Klüft skräms inför på fédéral électoral große aço áàãâä! éèêë? íì&#297;îï, óòõôö; úù&#361;ûü."

s = unicodedata.normalize('NFKD', frase).encode('ascii', 'ignore')
nfd = unicodedata.normalize('NFD', frase)
nfc = unicodedata.normalize('NFC', frase)
nfkd = unicodedata.normalize('NFKD', frase)
nfkc = unicodedata.normalize('NFKC', frase)

if __name__ == '__main__':
    print(s)
    print(type(s))
    print('-' * 20)
    print(nfd)  # Normalization Form Canonical Decomposition
    print(type(nfd))
    print(nfc)  # Normalization Form Canonical Composition
    print(type(nfc))
    print(nfkd)  # Normalization Form Compatibility Decomposition
    print(type(nfkd))
    print(nfkc)  # Normalization Form Compatibility Composition
    print(type(nfkc))
