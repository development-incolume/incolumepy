import unicodedata
num_ocidentais = ' '.join([str(x) for x in range(11)])
num_arabe = '۰ ۱ ۲ ۳ ٤ ٥ ٦ ۷ ۸ ۹ ۱۰'
num_arabe2 = unicodedata.normalize('NFKD', num_arabe).encode('ascii', 'ignore')

if __name__ == '__main__':
    print(num_ocidentais)
    print(num_arabe, num_arabe2)
    print(num_ocidentais[0] == num_arabe[0])
