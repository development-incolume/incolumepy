def calc(op, *valores):
    subtrair = lambda *valores: valores[0] - (valores[1])
    multiplicar = lambda *valores: valores[0] * valores[1]
    dividir = lambda *valores: valores[0] / valores[1]
    exponenciar = lambda *valores: valores[0] ^ valores[1]
    return {
        '+': sum(valores),
        '-': subtrair(*valores),
        '*': multiplicar(*valores),
        '/': dividir(*valores),
        '^': exponenciar(*valores)
    }.get(op, 'Erro!')


print(calc('+', 1, 2, 3, 4))
print(calc('-', 1, 2))
print(calc('*', 1, 2))
print(calc('/', 1, 2))
print(calc('^', 1, 2))
