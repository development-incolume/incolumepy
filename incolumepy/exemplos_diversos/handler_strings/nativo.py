# encode: utf-8
# Created on: 26/06/18

import sys
import platform


__author__ = '@britodfbr'


string = 'britodfr'
ch = 'b'


def truncus00():
    '''islower()'''
    return string.islower()


def truncus01():
    '''isupper()'''
    return string.isupper()


def truncus02():
    '''isdigit()'''
    return string.isdigit()

def truncus03():
    ''''''
    return string.find(string.lower(), ch) != -1


def truncus04():
    ''''''
    return ch in string.lower()

def truncus05():
    return 'a' <= ch <= 'z'
    pass


def run():
    print('Python {}'.format(platform.python_version()))
    for i in ['truncus{:0>2}'.format(x) for x in range(6)]:
        print('='*20)
        print(i)
        print(eval(i).__doc__)
        print('-'*20)
        try:
            print(eval(i)())
        except:
            print(sys.exc_info())
        print()


if __name__ == '__main__':
    run()