# modifica titulos de arquivos
import glob
import os


# glob.glob
def func0a():
    s = ("-")
    try:
        arq = open("listapdf.txt", "w")
        for file in glob.glob('filespdf/*.pdf'):
            f = str(file.name)
            fr = str(file.readline())
            arq.write(f + s + fr)
            arq.write("\n")
    except (RuntimeError, TypeError, NameError) as e:
        pass


def func0b():
    try:
        for file in glob.glob('/home/brito/Documentos/*.pdf'):
            print(type(file), file)
            with open(file, encoding="latin-1") as fa, \
                    open('listapdf.txt', 'a') as arq:
                name = file
                content = fa.readline()
                arq.write('{}: {}'.format(name, content))
    except Exception as e:
        print(e)


def func0c(path, tipoArq='.pdf'):
    try:
        files = [arq for arq in [os.path.join(path, file) for file in os.listdir(path)] if
                 os.path.isfile(arq) and arq.lower().endswith(tipoArq)]
        for file in files:
            with open(file, encoding='latin-1') as forigem, open('listapdf.txt', 'a') as fdestino:
                name = file
                content = forigem.readline()
                fdestino.write('{}: {}'.format(name, content))
    except Exception as e:
        print(e)


def lc_files_pdf(pasta):
    caminhos = [os.path.join(pasta, nome) for nome in os.listdir(pasta)]
    arquivos = [arq for arq in caminhos if os.path.isfile(arq)]
    pdfs = [arq for arq in arquivos if arq.lower().endswith(".pdf")]
    return pdfs


# os.path
def lc_files0(path, tipoArq='.pdf'):
    return [os.path.join(path, nome) for nome in os.listdir(path)]


def lc_files1(path, tipoArq='.pdf'):
    return [arq for arq in [os.path.join(path, nome) for nome in os.listdir(path)] if os.path.isfile(arq)]


def lc_files2(path, tipoArq='.pdf'):
    return [arq for arq in
            [arq for arq in [os.path.join(path, nome) for nome in os.listdir(path)] if os.path.isfile(arq)] if
            arq.lower().endswith(tipoArq)]


def lc_files3(path, tipoArq='.pdf'):
    return [arq for arq in lc_files1(path, tipoArq) if arq.lower().endswith(tipoArq)]


def lc_files4(path, tipoArq='.pdf'):
    return [arq for arq in
            [arq for arq in [os.path.join(path, nome) for nome in os.listdir(path)] if os.path.isfile(arq)] if
            arq.lower().endswith(tipoArq)]


def lc_filesA0(path):
    return [arq for arq in [os.path.join(path, file) for file in os.listdir(path)] if os.path.isfile(arq)]


def lc_filesA1(path, tipoArq='.pdf'):
    return [arq for arq in [os.path.join(path, file) for file in os.listdir(path)] if
            os.path.isfile(arq) and arq.endswith(tipoArq)]


def lc_files(path, tipoArq='.pdf'):
    return [arq for arq in
            [arq for arq in [os.path.join(path, nome) for nome in os.listdir(path)] if os.path.isfile(arq)] if
            arq.lower().endswith(tipoArq)]


def lc_filesA(path, tipoArq='.pdf'):
    return [arq for arq in [os.path.join(path, file) for file in os.listdir(path)] if
            os.path.isfile(arq) and arq.lower().endswith(tipoArq)]


# os.walk
def lc_filesB0(path):
    return list(os.walk(path))


def lc_filesB(path):
    for item in os.walk(path):
        print(item)


def implementA(path, **kwargs):
    print(os.path.isfile(__file__))
    print(os.path.dirname(__file__))
    print(os.listdir(os.path.dirname(__file__)))
    print(os.path.split(os.path.dirname(__file__)))
    print(os.path.join('./'))
    arquivos = [arq for arq in [os.path.join(path, file)
                                for file in os.listdir(path)]
                if os.path.isfile(arq)]
    print(arquivos)

    def myread():
        pass


# print(myread(*args))

if __name__ == '__main__':
    # func0b()
    print('lc_files_pdf', lc_files_pdf('/home/brito/Documentos/'))
    print('lc_files0', lc_files0('/home/brito/Documentos/'))
    print('lc_files1', lc_files1('/home/brito/Documentos/'))
    print('lc_files', lc_files('/home/brito/Documentos/'))
    print('lc_filesA0', lc_filesA0('/home/brito/Documentos/'))
    print('lc_filesA', lc_filesA('/home/brito/Documentos/'))
    print()
    print(lc_files_pdf('/home/brito/Documentos/') == lc_filesA('/home/brito/Documentos/'))
    print(lc_files_pdf('/home/brito/Documentos/') == lc_files('/home/brito/Documentos/'))
    # implementA('/home/brito/Documentos/')

    # implementA('/home/brito/Documentos/*.pdf'.split('/'))
    func0c('/home/brito/Documentos/')
    print(lc_filesB('/home/brito/Documentos'))
