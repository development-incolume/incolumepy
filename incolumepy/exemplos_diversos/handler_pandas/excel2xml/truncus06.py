# Import `pandas`
import os
import pandas as pd
from incolumepy.utils.files import realfilename

try:
    file = os.path.join('/','home','brito','Downloads','MRE.xlsx')
except:
    file = 'mre.xlsx'



def exemplo06():
    '''sort serie pandas for date'''
    print('\n exemplo6')
    titles = ['data', 'ato', 'situacao', 'ementa']
    xlsx = pd.read_excel(file, header=0, names=titles)
    dates = xlsx.sort_values(by=['data','ato'], ascending=False)
    print(dates.head(10))

def exemplo07():
    '''sort serie pandas for date'''
    print('\n exemplo7')
    titles = ['data', 'ato', 'situacao', 'ementa']
    xlsx = pd.read_excel(file, header=0, names=titles)
    dates = xlsx.sort_values(by=['data','ato'], ascending=False)
    #print(dates.head(10))
    for i in dates.iterrows():
        print(i)
        #item = et.SubElement()

def to_xml0(df, filename=None, mode='w'):
    'https://stackoverflow.com/questions/18574108/\
    how-do-convert-a-pandas-dataframe-to-xml?utm_medium=organic&\
    utm_source=google_rich_qa&utm_campaign=google_rich_qa'
    def row_to_xml(row):
        xml = ['<item>']
        for i, col_name in enumerate(row.index):
            xml.append('  <field name="{0}">{1}</field>'.format(col_name, row.iloc[i]))
        xml.append('</item>')
        return '\n'.join(xml)
    res = '\n'.join(df.apply(row_to_xml, axis=1))

    if filename is None:
        return res
    with open(filename, mode) as f:
        f.write(res)

def to_xml1(df, filename=None, mode='w'):
    def row_to_xml(row):
        xml = ['    <item>']
        for i, col_name in enumerate(row.index):
            xml.append('      <field name="{0}">{1}</field>'.format(col_name, row.iloc[i]))
        xml.append('    </item>')
        return '\n'.join(xml)
    res = '<?xml version="1.0"?>\n<atos>\n'
    res += '\n'.join(df.apply(row_to_xml, axis=1))
    res += '\n</atos>'

    if filename is None:
        return res
    with open(filename, mode) as f:
        f.write(res)

def to_xml(df, filename=None, mode='w'):
    def row_to_xml(row):
        xml = ['    <item>']
        for i, col_name in enumerate(row.index):
            xml.append('      <{0}>{1}</{0}>'.format(col_name, row.iloc[i]))
        xml.append('    </item>')
        return '\n'.join(xml)
    res = '<?xml version="1.0"?>\n<atos>\n'
    res += '\n'.join(df.apply(row_to_xml, axis=1))
    res += '\n</atos>'

    if filename is None:
        return res
    with open(filename, mode) as f:
        f.write(res)

pd.DataFrame.to_xml = to_xml

def exemplo08():
    print('\n exemplo8')
    titles = ['data', 'ato', 'situacao', 'ementa']
    xlsx = pd.read_excel(file, header=0, names=titles)
    dates = xlsx.sort_values(by=['data', 'ato'], ascending=False)
    #for i in dates.iterrows(): print(i)
    dates.to_xml('mre.xml')

def exemplo02():
    print('\n exemplo2')
    titles = ['data', 'ato', 'situacao', 'ementa']
    xlsx = pd.read_excel(file, header=0, names=titles)
    print(xlsx.head())

    #gravar em csv
    xlsx.to_csv(realfilename('dados/mre.csv'), sep=';')

    #gravar em json
    xlsx.to_json(realfilename('dados/mre.json'))

    #gravar em pickle
    xlsx.to_pickle(realfilename('dados/mre.pickle'))

    #gravar em sql
    xlsx.to_json(realfilename('dados/mre.sql'))

    #gravar em dict
    xlsx.to_json(realfilename('dados/mre.dict'))

    #gravar em latex
    xlsx.to_json(realfilename('dados/mre.tex'))

    #gravar em xlsx
    xlsx.to_excel(realfilename('dados/mre.xlsx'))

    #gravar em xml
    xlsx.to_xml(realfilename('dados/mre.xml'))


def run():
    l = []
    #l.append(exemplo06)
    #l.append(exemplo07)
    l.append(exemplo08)
    l.append(exemplo02)

    for i in l:
        i()


if __name__ == '__main__':
    run()
