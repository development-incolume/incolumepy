# Install `XlsxWriter`
#pip install XlsxWriter
import pandas as pd

yourData=[1, 2, 3, 4]
# Specify a writer
writer = pd.ExcelWriter('example.xlsx', engine='xlsxwriter')

# Write your DataFrame to a file
yourData.to_excel(writer, 'Sheet1')

# Save the result
writer.save()