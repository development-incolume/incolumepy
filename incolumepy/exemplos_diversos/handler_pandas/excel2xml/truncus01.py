# Import pandas
import pandas as pd
import os.path


# Assign spreadsheet filename to `file`
file = os.path.join('/','home','brito','Downloads','MRE.xlsx')

def exemplo01():
    print('\n exemplo1')
    # Load spreadsheet
    xl = pd.ExcelFile(file)

    # Print the sheet names
    print(xl.sheet_names)

    # Load a sheet into a DataFrame by name: df1
    df1 = xl.parse('Plan1')
    print(df1.head())



def exemplo02():
    '''Movido para truncus06'''
    print('\n exemplo2')
    titles = ['data', 'ato', 'situacao', 'ementa']
    xlsx = pd.read_excel(file, header=0, names=titles)
    print(xlsx.head())

    #gravar em csv
    #xlsx.to_csv('mre.csv', sep=';')

    #gravar em json
    #xlsx.to_json('mre.json')

    #gravar em pickle
    #xlsx.to_pickle('mre.pickle')

    #gravar em sql
    #xlsx.to_json('mre.sql')

    #gravar em dict
    #xlsx.to_json('mre.dict')

    #gravar em latex
    #xlsx.to_json('mre.tex')

def exemplo03():
    print('\n exemplo3')
    titles = ['data', 'ato', 'situacao', 'ementa']
    xlsx = pd.read_excel(file, header=0, names=titles)
    print(xlsx.head(2))
    print(xlsx.tail(2))
    print('shape: ', xlsx.shape)

def exemplo04():
    '''sort serie pandas for date'''
    print('\n exemplo4')
    titles = ['data', 'ato', 'situacao', 'ementa']
    xlsx = pd.read_excel(file, header=0, names=titles)
    dates = xlsx.data
    print(xlsx.data.sort_values(ascending=True))

def exemplo05():
    '''sort serie pandas for date'''
    print('\n exemplo5')
    titles = ['data', 'ato', 'situacao', 'ementa']
    xlsx = pd.read_excel(file, header=0, names=titles)
    dates = xlsx.sort_values('data', ascending=False)
    print(dates.head())

def exemplo06():
    '''sort serie pandas for date'''
    print('\n exemplo6')
    titles = ['data', 'ato', 'situacao', 'ementa']
    xlsx = pd.read_excel(file, header=0, names=titles)
    dates = xlsx.sort_values(by=['data','ato'], ascending=False)
    print(dates.head(10))


def run():
    l = []
#    l.append(exemplo01)
#    l.append(exemplo02)
#    l.append(exemplo03)
#    l.append(exemplo04)
#    l.append(exemplo05)
    l.append(exemplo06)
    for i in l:
        i()

if __name__ == '__main__':
    pass
#    exemplo01()
    run()