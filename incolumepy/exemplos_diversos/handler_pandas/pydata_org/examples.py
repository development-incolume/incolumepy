import pandas as pd
import numpy as np
#https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.html

d = {'col1': [1, 2], 'col2': [3, 4]}
#Constructing DataFrame from a dictionary.
df = pd.DataFrame(data=d)

print(df)
print('-'*30)
#Notice that the inferred dtype is int64.
print(df.dtypes)
print('-'*30)

#To enforce a single dtype:
df = pd.DataFrame(data=d, dtype=np.int8)
print(df.dtypes)
print('-'*30)

# Constructing DataFrame from numpy ndarray:
df2 = pd.DataFrame(np.random.randint(low=0, high=10, size=(5, 5)), columns=['a', 'b', 'c', 'd', 'e'])
print(df2)