import pandas as pd
'''aplicar multiplos filtros em um dataframe pandas'''

movies = pd.read_csv('http://bit.ly/imdbratings')
ufos = pd.read_csv('http://bit.ly/uforeports')


def pandas01():
    print('pandas01')
    print(movies.head(3))



def pandas02():
    print('\npandas 02')
    print(movies[(movies.duration >= 200) & (movies.genre=='Drama')])


def pandas03():
    print('\npandas 03')
    print(movies[(movies.genre == 'Crime') | (movies.genre == 'Drama') | (movies.genre == 'Action')])


def pandas04():
    print('\npandas 04')
    print(movies.genre.isin(['Crime', 'Drama', 'Action']))


def pandas05():
    print('\npandas 05')
    print(movies[movies.genre.isin(['Crime', 'Drama', 'Action'])])



def pandas06():
    print('\npandas 06')


def pandas07():
    print('\npandas 07')

def pandas08():
    print('\npandas 08')

def pandas09():
    print('\npandas 09')

def run():
    defines = []
    defines.append(pandas01)
    defines.append(pandas02)
    defines.append(pandas03)
    defines.append(pandas04)
    defines.append(pandas05)
    defines.append(pandas06)
    defines.append(pandas07)
    defines.append(pandas08)
    defines.append(pandas09)

    for d in defines:
        d()

if __name__ == '__main__':
    run()