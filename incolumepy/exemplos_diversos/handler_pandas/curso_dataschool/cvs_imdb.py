import sys
import os
import re
import logging
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoSuchWindowException
from selenium.common.exceptions import SessionNotCreatedException



logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class CVS_imdb:
    def __init__(self, driver='../handler_HTML_XML/drivers/geckodriver', **kwargs):
        self.driver = driver
        for k,v in kwargs.items():
            logger.debug((k,v))
            self.__dict__[k] = v
        self.imdb_list = []
        self.locale = 'pt_BR'
        self.firefox_profile = webdriver.FirefoxProfile()
        self.firefox_profile.set_preference("intl.accept_languages", self.locale)
        self.firefoxbin = os.path.abspath(driver)
        self.firefox = webdriver.Firefox(executable_path=self.firefoxbin, firefox_profile=self.firefox_profile)
        logger.debug(self.__dict__)

    def campos0(self):
        self.firefox.get(self.url)
        logger.debug(self.firefox.current_url)
        imdb_list = []
        while True:
            try:
                wait = WebDriverWait(self.firefox, 10)
                wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "NEXT")))
                assert "FILMES NOTA" in self.firefox.title
                imdb_list += self.firefox.find_elements_by_class_name('lister-item-content')
                self.firefox.find_element_by_link_text('NEXT').click()
            except:
                logger.error('.............campos.while.end................')
                break


        print(len(imdb_list))

    def campos(self):
        self.firefox.get(self.url)
        imdb_list = []
        logger.debug(self.firefox.current_url)
        while True:
            try:
                body = self.firefox.find_element_by_tag_name("body")
                body.send_keys(Keys.CONTROL + 't')
                logger.debug('captura do código')
                source = re.sub('\n', '', self.firefox.page_source)
                logger.debug(type(source))
                soup = BeautifulSoup(source, 'html.parser')
                logger.debug(type(soup))
                logger.debug('adicionando o conteudo da pagina na lista')
                self.imdb_list += soup.find_all(class_="lister-item-content")
                logger.debug(len(self.imdb_list))

                logger.debug('verificar se body carregou..')
                body = self.firefox.find_element_by_tag_name("body")
                body.send_keys(Keys.CONTROL + 't')

                logger.debug('aguardando o link next carregar..')
                wait = WebDriverWait(self.firefox, 10)
                wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "Next")))

                logger.debug('clique no link next')
                self.firefox.find_element_by_class_name('flat-button.lister-page-next.next-page').click()


            except Exception as e:
                logger.info('.............while.end................')
                logger.info(sys.exc_info())
                #raise(e)
                break
        logger.debug(len(self.imdb_list))
        logger.debug('..print titulos..')
        for i, item in enumerate(self.imdb_list):
            titulo = item.h3.a.text
            ano = re.split('\(|\)', item.select("[class~=lister-item-year]")[0].text, re.I)[1]
            censura = ''#item.select("[class=certificate]")
            runtime = ''.join([ x for x in item.select("[class~=runtime]")[0].text if x.isdigit()])
            genre = item.select("[class~=genre]")[0].text.strip()
            diretor = item.select(".text-muted.text-small")[-2].text.split('|')[0].split(':')[-1].strip(',')
            atores = item.select(".text-muted.text-small")[-2].text.split('|')[-1].split(':')[-1].strip(',')

            logger.debug(
                "..#{:>3}..titulo: {}..ano: {}..censura: {}..runtime: {}..genre: {}..diretor: {}..atores: {}..".format(
                    i, titulo, ano, censura, runtime, genre, diretor, atores))
            imdb_list.append("{}; {}; {}; {}; {}; {}; {}".format(
                titulo, ano, censura, runtime, genre, diretor, atores))
        self.imdb_list = imdb_list
        logger.debug(len(imdb_list))

        pass

    @staticmethod
    def run():
        logger.debug('>Starting..')
        a = CVS_imdb(
            url='https://www.imdb.com/list/ls003330768/?sort=user_rating,desc&st_dt=&mode=detail&page=1')
        a.url='https://www.imdb.com/list/ls003330768/?st_dt=&mode=detail&page=1&ref_=ttls_vm_dtl&sort=moviemeter,asc'
        try:
            a.campos()
            for i,j in enumerate(a.imdb_list):
                print(i,j)

        except (NoSuchWindowException, SessionNotCreatedException, WebDriverException) as e:
            logger.error(e)
        except:
            logger.error('Ops..{}'.format(sys.exc_info()))
        finally:
            a.firefox.quit()
            logger.debug('>Finish..')


if __name__ == '__main__':
    CVS_imdb.run()


