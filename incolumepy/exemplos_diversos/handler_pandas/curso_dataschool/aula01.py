import pandas as pd
''' Como ler dados tabulares'''
def pandas01():
    print('pandas01')
    print(pd.read_table('http://bit.ly/chiporders'))

def pandas02():
    print('\npandas02')
    print(pd.read_table('http://bit.ly/movieusers'))

def pandas03():
    '''define o separador correto'''
    print('\npandas03')
    print(pd.read_table('http://bit.ly/movieusers', sep='|'))

def pandas04():
    '''define o separador correto e notifica que a 1ª linha não é cabeçalho'''
    print('\npandas04')
    print(pd.read_table('http://bit.ly/movieusers', sep='|', header=None))

def pandas05():
    '''define novo header'''
    print('\npandas05')
    user_cols = ['user_id', 'age', 'gender', 'occupation', 'zip_code']
    print(pd.read_table('http://bit.ly/movieusers', sep='|', header=None, names=user_cols))

def pandas06():
    '''define objeto manipulavel'''
    print('\npandas06')
    user_cols = ['user_id', 'age', 'gender', 'occupation', 'zip_code']
    user = pd.read_table('http://bit.ly/movieusers', sep='|', header=None, names=user_cols)
    print(user.head())

def run():
    defines = []
#    defines.append(pandas01)
#    defines.append(pandas02)
#    defines.append(pandas03)
#    defines.append(pandas04)
#    defines.append(pandas05)
    defines.append(pandas06)

    for d in defines:
        d()

if __name__ == '__main__':
    run()