import pandas as pd
'''ordenar serie ou dataframe pandas'''

movies = pd.read_csv('http://bit.ly/imdbratings')
ufos = pd.read_csv('http://bit.ly/uforeports')


def pandas01():
    print('pandas01')
    print(movies.head(3))



def pandas02():
    print('\npandas 02')
    print(movies.title.sort_values())


def pandas03():
    print('\npandas 03')
    print(movies['title'].sort_values())


def pandas04():
    print('\npandas 04')
    print(type(movies.title.sort_values()))


def pandas05():
    print('\npandas 05')
    print(movies.title.sort_values(ascending=False))



def pandas06():
    print('\npandas 06')
    print(movies.sort_values('title'))


def pandas07():
    print('\npandas07')
    print(movies.sort_values('duration', ascending=False))


def pandas08():
    print('\npandas08')
    print(movies.sort_values(['content_rating', 'duration']).head())


def pandas09():
    print('\npandas09')
    print(movies.sort_values(['content_rating', 'duration'], ascending=False).head())


def run():
    defines = []
    defines.append(pandas01)
    defines.append(pandas02)
    defines.append(pandas03)
    defines.append(pandas04)
    defines.append(pandas05)
    defines.append(pandas06)
    defines.append(pandas07)
    defines.append(pandas08)
    defines.append(pandas09)

    for d in defines:
        d()

if __name__ == '__main__':
    run()