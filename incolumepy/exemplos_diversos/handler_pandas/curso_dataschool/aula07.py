import pandas as pd
'''filtrar dataframe pandas pelo valor da coluna'''

movies = pd.read_csv('http://bit.ly/imdbratings')
ufos = pd.read_csv('http://bit.ly/uforeports')


def pandas01():
    print('pandas01')
    print(movies.head(3))



def pandas02():
    print('\npandas 02')
    booleans = []
    for length in movies.duration:
        if length >= 200:
            booleans.append(True)
        else:
            booleans.append(False)

    print(booleans[:5], len(booleans))




def pandas03():
    print('\npandas 03')
    booleans = []
    for length in movies.duration:
        if length >= 200:
            booleans.append(True)
        else:
            booleans.append(False)

    is_long = pd.Series(booleans)
    print(type(is_long))
    print(is_long.head())



def pandas04():
    print('\npandas 04')
    booleans = []
    for length in movies.duration:
        if length >= 200:
            booleans.append(True)
        else:
            booleans.append(False)

    is_long = pd.Series(booleans)
    print(movies[is_long])


def pandas05():
    print('\npandas 05')
    is_long = movies.duration >= 200
    print(type(is_long))
    print(is_long.head())



def pandas06():
    print('\npandas 06')
    is_long = movies.duration >= 200
    print(movies[is_long])



def pandas07():
    print('\npandas 07')
    print(movies[movies.duration >= 200])


def pandas08():
    print('\npandas 08')
    print('notação por ponto')
    print(movies[movies.duration >= 200].genre)
    print('notação por colchete')
    print(movies[movies.duration >= 200]['genre'])

def pandas09():
    print('\npandas 09')
    result = movies.loc[movies.duration >= 200, 'genre']
    print(result)
    print(type(result))



def run():
    defines = []
    defines.append(pandas01)
    defines.append(pandas02)
    defines.append(pandas03)
    defines.append(pandas04)
    defines.append(pandas05)
    defines.append(pandas06)
    defines.append(pandas07)
    defines.append(pandas08)
    defines.append(pandas09)

    for d in defines:
        d()

if __name__ == '__main__':
    run()