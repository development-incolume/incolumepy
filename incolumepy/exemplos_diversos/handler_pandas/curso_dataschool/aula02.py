import pandas as pd
'''Como selecionar uma serie panda de um dataframe'''

def pandas01():
    '''arquivo tabular separado por vigulas'''
    print('pandas01')
    ufo = pd.read_table('http://bit.ly/uforeports', sep=',')
    print(ufo.head())

def pandas02():
    '''commons separete values'''
    print('\npandas02')
    ufo = pd.read_csv('http://bit.ly/uforeports')
    print(ufo.head())
    print(type(ufo))

def pandas03():
    '''serie panda somente com cidades'''
    print('\npandas03')
    ufo = pd.read_csv('http://bit.ly/uforeports')
    # o campo de consulta é case sensitive
    print(ufo['City'])
    print(type(ufo['City']))

def pandas04():
    '''serie panda somente com cidades notação por ponto
    a notação por ponto não permite espaços no nome de campo
    '''
    print('\npandas04')
    ufo = pd.read_csv('http://bit.ly/uforeports')
    # o campo de consulta é case sensitive
    print(ufo.City)
    print(type(ufo.City))


def pandas05():
    '''serie panda somente com cidades notação por ponto
    a notação por ponto não permite espaços no nome de campo
    '''
    print('\npandas05')
    ufo = pd.read_csv('http://bit.ly/uforeports')
    # o campo de consulta é case sensitive
    print(ufo['Colors Reported'])

def pandas06():
    '''serie panda somente com cidades notação por ponto
    a notação por ponto não permite espaços no nome de campo
    '''
    print('\npandas06')
    ufo = pd.read_csv('http://bit.ly/uforeports')
    print(ufo.head())
    # relatorio linhas x colunas
    print(ufo.shape)


def pandas07():
    '''serie panda somente com cidades notação por ponto
    a notação por ponto não permite espaços no nome de campo
    '''
    print('\npandas07')
    ufo = pd.read_csv('http://bit.ly/uforeports')
    #mescla cidade e estado em mesmo campo
    u = ufo.City + ufo.State
    print(u.head())

def pandas08():
    '''serie panda somente com cidades notação por ponto
    a notação por ponto não permite espaços no nome de campo
    '''
    print('\npandas08')
    ufo = pd.read_csv('http://bit.ly/uforeports')
    #mescla cidade e estado em mesmo campo
    u = ufo.City + ', ' + ufo.State
    print(u.head())
    print(type(u))

def pandas09():
    '''serie panda somente com cidades notação por ponto
    a notação por ponto não permite espaços no nome de campo
    '''
    print('\npandas09')
    ufo = pd.read_csv('http://bit.ly/uforeports')
    #mescla cidade e estado em mesmo campo
    ufo['Location'] = ufo.City + ', ' + ufo.State
    print(ufo.head())


def run():
    defines = []
#    defines.append(pandas01)
#    defines.append(pandas02)
    defines.append(pandas03)
    defines.append(pandas04)
    defines.append(pandas05)
    defines.append(pandas06)
    defines.append(pandas07)
    defines.append(pandas08)
    defines.append(pandas09)

    for d in defines:
        d()

if __name__ == '__main__':
    run()