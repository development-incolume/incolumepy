import pandas as pd
'''remover colunas de um dataframe pandas'''

movies = pd.read_csv('http://bit.ly/imdbratings')
ufos = pd.read_csv('http://bit.ly/uforeports')


def pandas01():
    print('pandas01')
    print(ufos.head(3))



def pandas02():
    print('\npandas 02')
    ufos.drop('Colors Reported', axis=1, inplace=True)
    print(ufos.head(3))

def pandas03():
    '''
    remove colunas
    :return:
    '''
    print('\npandas 03')
    ufos.drop(['City', 'State'], axis=1, inplace=True)
    print(ufos.columns)
    print(ufos.head())

def pandas04():
    '''
    remover linhas
    :return:
    '''
    print('\npandas 04')
    ufos = pd.read_csv('http://bit.ly/uforeports')
    print(ufos.head())
    ufos.drop([1, 2], axis=0, inplace=True)
    print(ufos.head())


def pandas05():
    '''
    renomear modo 3
    :return:
    '''
    print('\npandas 05')



def pandas06():
    '''
    renomear modo 4
    :return:
    '''
    print('\npandas 06')


def pandas07():
    '''
    title caixa baixa
    :return:
    '''
    print('\npandas07')


def pandas08():
    '''
    title caixa alta
    :return:
    '''
    print('\npandas08')


def pandas09():
    '''
    title caixa baixa sem espaço
    :return:
    '''
    print('\npandas09')



def run():
    defines = []
    defines.append(pandas01)
    defines.append(pandas02)
    defines.append(pandas03)
    defines.append(pandas04)
    defines.append(pandas05)
    defines.append(pandas06)
    defines.append(pandas07)
    defines.append(pandas08)
    defines.append(pandas09)

    for d in defines:
        d()

if __name__ == '__main__':
    run()