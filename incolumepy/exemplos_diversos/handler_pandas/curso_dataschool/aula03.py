import pandas as pd
'''atributos e metodos pandas'''

movies = pd.read_csv('http://bit.ly/imdbratings')

def pandas01():
    print('pandas01')
    print(movies.head())
    print(movies.describe())
    print(movies.describe(include=['object']))


def pandas02():
    print('\npandas 02')
    print(movies.shape)
    print(movies.dtypes)


def run():
    defines = []
    defines.append(pandas01)
    defines.append(pandas02)
#    defines.append(pandas03)
#    defines.append(pandas04)
#    defines.append(pandas05)
#    defines.append(pandas06)
#    defines.append(pandas07)
#    defines.append(pandas08)
#    defines.append(pandas09)

    for d in defines:
        d()

if __name__ == '__main__':
    run()