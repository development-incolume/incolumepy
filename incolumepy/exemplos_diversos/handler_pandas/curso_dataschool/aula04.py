import pandas as pd
'''renomear colunas pandas'''

movies = pd.read_csv('http://bit.ly/imdbratings')
ufos = pd.read_csv('http://bit.ly/uforeports')


def pandas01():
    print('pandas01')
    print(ufos.head(3))



def pandas02():
    print('\npandas 02')
    print(ufos.columns)

def pandas03():
    '''
    renomear modo 1
    :return:
    '''
    print('\npandas 03')
    ufos.rename(columns={'Colors Reported':'Colors_Reported', 'Shape Reported':'Shape_Reported'}, inplace=True)
    print(ufos.columns)

def pandas04():
    '''
    renomear modo 2
    :return:
    '''
    print('\npandas 04')
    ufo_cols = ['city', 'colors reported', 'shape reported', 'state', 'time']
    ufos.columns = ufo_cols
    print(ufos.head(2))


def pandas05():
    '''
    renomear modo 3
    :return:
    '''
    print('\npandas 05')
    ufo_cols = ['city', 'colors reported', 'shape reported', 'state', 'time']
    ufos = pd.read_csv('http://bit.ly/uforeports', names=ufo_cols, header=0)
    print(ufos.head(2))


def pandas06():
    '''
    renomear modo 4
    :return:
    '''
    print('\npandas 06')
    ufos = pd.read_csv('http://bit.ly/uforeports')
    print(ufos.columns)
    ufos.columns = ufos.columns.str.replace(' ', '_')
    print(ufos.head(2))

def pandas07():
    '''
    title caixa baixa
    :return:
    '''
    print('\npandas07')
    ufos = pd.read_csv('http://bit.ly/uforeports')
    print(ufos.columns)
    ufos.columns = ufos.columns.str.lower()
    print(ufos.columns)

def pandas08():
    '''
    title caixa alta
    :return:
    '''
    print('\npandas08')
    ufos = pd.read_csv('http://bit.ly/uforeports')
    print(ufos.columns)
    ufos.columns = ufos.columns.str.upper()
    ufos.columns = ufos.columns.str.replace(' ', '_')
    print(ufos.columns)

def pandas09():
    '''
    title caixa baixa sem espaço
    :return:
    '''
    print('\npandas09')
    ufos = pd.read_csv('http://bit.ly/uforeports')
    print(ufos.columns)
    ufos.columns = ufos.columns.str.lower()
    ufos.columns = ufos.columns.str.replace(' ', '_')
    print(ufos.columns)


def run():
    defines = []
    defines.append(pandas01)
    defines.append(pandas02)
    defines.append(pandas03)
    defines.append(pandas04)
    defines.append(pandas05)
    defines.append(pandas06)
    defines.append(pandas07)
    defines.append(pandas08)
    defines.append(pandas09)

    for d in defines:
        d()

if __name__ == '__main__':
    run()