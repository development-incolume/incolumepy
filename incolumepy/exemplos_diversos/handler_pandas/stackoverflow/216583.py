import pandas as pd
import numpy as np
from collections import OrderedDict
data = OrderedDict(
{
'País': ['Bélgica', 'Índia', 'Brasil','Índia'],
'Capital': ['Bruxelas', 'Nova Delhi', 'Brasília', 'Nova Delhi'],
'População': [123465, 456789, 987654, 456789]
})

df = pd.DataFrame(data)
# print dataframe
print(df)
print('-'*20)

# drop duplicates
df_clean = df.drop_duplicates()
print(df_clean)
print('-'*20)

#select duplicates
paises = df.País
df_duplicates = df[paises.isin(paises[paises.duplicated()])]
print(df_duplicates)
print('-'*20)