__author__ = '@britodfbr'

import pandas
data = pandas.DataFrame({
    'Local':['são paulo','rio','bahia'],
    '01/09/2007': [1,2,0],
    '01/10/2007': [0,1,0],
    '01/11/2007': [2,1,1],
    '01/12/2007': [2,0,3],
    '01/01/2008': [0,1,1],
    'contato':['2345-1244','5422-1244','2345-8674']})

'''Aplica o comando melt nos indices, setando Local e contato como ids e adicionando a variável mes com o valor 
quantidade dentro do novo dataFrame, além disto já faço a ordenação pelo Local e quantidade de modo descendente'''
data = pandas.melt(data.reset_index(), id_vars=['Local', 'contato'],
var_name='mes', value_name='quantidade').sort(['Local','quantidade'], ascending=False)



'''O novo dataFrame foi criado, porém ainda temos algumas linhas indesejáveis que contém index no valor, 
então simplesmente faço a remoção destas linhas '''
data = data[data.mes.str.contains("index") == False]