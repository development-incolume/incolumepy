import pandas as pd
import numpy as np
import warnings
warnings.filterwarnings('ignore')


raw_data = {'regiment': ['Nighthawks', 'Nighthawks', 'Nighthawks', 'Nighthawks'],
            'company': ['1st', '1st', '2nd', '2nd'],
            'deaths': ['kkk', 52, '25', 616],
            'battles': [5, 42, 2, 2],
            'size': ['l', 'll', 'l', 'm']}
df = pd.DataFrame(raw_data, columns = ['regiment', 'company', 'deaths', 'battles', 'size'])

print(df)
print('-'*30)

df['com_zeros'] = '0'
print(df)
print('-'*30)


for b in df.itertuples():
    df.com_zeros[b.Index] = '0'+str(b.battles) if b.battles<9 else str(b.battles)
print(df)
print('-'*30)