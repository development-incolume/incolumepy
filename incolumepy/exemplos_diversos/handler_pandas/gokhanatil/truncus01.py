import xml.etree.cElementTree as et
import pandas as pd


def exemplo01():
    parsedXML = et.parse("test.xml")
    for node in parsedXML.getroot():
        name = node.attrib.get('name')
        email = node.find('email')
        phone = node.find('phone')
        street = node.find('address/street')
        print(name, email, phone, street)



def run():
    l = []
    l.append(exemplo01)
    for i in l:
        i()

if __name__ == '__main__':
    run()