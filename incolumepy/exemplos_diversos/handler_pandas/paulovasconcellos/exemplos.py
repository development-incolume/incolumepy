import pandas as pd
import numpy as np
#

#https://paulovasconcellos.com.br/28-comandos-%C3%BAteis-de-pandas-que-talvez-voc%C3%AA-n%C3%A3o-conhe%C3%A7a-6ab64beefa93

def exemplo01():
    s = pd.Series([3, -5, 7, 4], index=['a', 'b', 'c', 'd'])
    return (s)

def exemplo02():
    data = {
        'País': ['Bélgica', 'Índia', 'Brasil'],
        'Capital': ['Bruxelas', 'Nova Delhi', 'Brasília'],
        'População': [123465, 456789, 987654]
    }
    # Criando o DataFrame
    df = pd.DataFrame(data, columns=['País', 'Capital', 'População'])
    return df

def exemplo03():
    df = exemplo02()
    # Para escrever arquivos CSV
    df.to_csv('nome_do_arquivo.csv')

    # Para ler arquivos CSV codificados em ISO
    pd.read_csv('nome_do_arquivo.csv')

    return True

def exemplo04():
    df = exemplo02()
    df.to_excel('seu_arquivo_excel.xlsx')
    #Abrindo arquivos de Excel:
    xlsx = pd.ExcelFile('seu_arquivo_excel.xlsx')
    df = pd.read_excel(xlsx, 'Sheet1')
    return df


def exemplo05():
    # Removendo linhas e colunas:
    # Removendo linhas pelo index
    s = pd.Series([3, -5, 7, 4], index=['a', 'b', 'c', 'd'])
    #s.drop([0, 1])
    #return s

def exemplo06():
    df = exemplo02()
    # Removendo colunas utilizando o argumento axis=1
    df.drop('País', axis=1)
    return df

def exemplo07():
    df = exemplo02()
    # Quantidade de linhas e colunas do DataFrame
    return df.shape


def exemplo08():
    df = exemplo02()
    # Descrição do Index
    return df.index

def exemplo09():
    df = exemplo02()
    # Colunas presentes no DataFrame
    return df.columns


def exemplo10():
    df = exemplo02()
    # Contagem de dados não-nulos
    return  df.count()


def exemplo11():
    df = exemplo02()
    # Criando uma nova coluna em um DataFrame:
    df['Nova Coluna'] = 0
    return df


def exemplo12():
    df = exemplo02()
    # Renomeando colunas de um DataFrame:
    # Se seu DataFrame possui 3 colunas, passe 3 novos valores em uma lista
    df.columns = ['Coluna 1', 'Coluna 2', 'Coluna 3']
    return df

def exemplo13():
    df = exemplo02()
    # Soma dos valores de um DataFrame
    return df.sum()

def exemplo14():
    df = exemplo02()
    # Menor valor de um DataFrame
    return df.min()

def exemplo15():
    df = exemplo02()
    # Maior valor
    return  df.max()

def exemplo16():
    df = exemplo02()
    # Index do menor valor
    #return df.idmin()


def exemplo17():
    df = exemplo02()
    # Index do maior valor
    #return df.idmax()


def exemplo18():
    df = exemplo02()
    # Resumo estatístico do DataFrame, com quartis, mediana, etc.
    return  df.describe()


def exemplo19():
    df = exemplo02()
    # Média dos valores
    return df.mean()

def exemplo20():
    df = exemplo02()
    # Mediana dos valores
    return  df.median()


def exemplo21():
    df = exemplo02()
    # Aplicando uma função que substitui a por b
    df.apply(lambda x: x.replace('a', 'b'))
    return df


def exemplo22():
    df = exemplo02()
    # Ordenando em ordem crescente
    df.sort_values('Capital')
    return df

def exemplo23():
    df = exemplo02()
    # Ordenando em ordem decrescente
    df.sort_values('Capital', ascending=False)
    return df


def exemplo24():
    #Operações aritméticas em Series:
    return pd.Series([1, 2, 3, 4, 5], index=['a', 'b', 'c', 'd', 'e'])

def exemplo25():
    s = exemplo24()
    # Somando todos os valores presentes na Series por 2
    return s.add(2)


def exemplo25():
    s = exemplo24()
    # Subtraindo 2 de todos os valores
    return s.sub(2)


def exemplo26():
    s = exemplo24()
    # Multiplicando todos os valores por 2
    return s.mul(2)


def exemplo27():
    s = exemplo24()
    # Dividindo valores por 2
    return s.div(2)


def exemplo28():
    df = exemplo02()
    #Indexação por Boolean:
    # Filtrando o DataFrame para mostrar apenas valores pares
    return df[df['População'] % 2 == 0]



def exemplo29():
    df = exemplo02()
    # Selecionando a primeira linha da coluna país
    #return df.loc([0], ['País'])


def run():
    l = []
    for i in range(1, 30):
        l.append(('exemplo{:0>2}'.format(i)))
        pass

    for i in l:
        print(eval(i)())
        print('-'*30)

if __name__ == '__main__':
    run()