# http://arrow.readthedocs.io/en/latest/

import arrow

print('Parse from a string:')
print("arrow.get('2013-05-05 12:30:45', 'YYYY-MM-DD HH:mm:ss'): ",
      arrow.get('2013-05-05 12:30:45', 'YYYY-MM-DD HH:mm:ss'))
# <Arrow [2013-05-05T12:30:45+00:00]>

print('\nSearch a date in a string:')
print("arrow.get('June was born in May 1980', 'MMMM YYYY')",
      arrow.get('June was born in May 1980', 'MMMM YYYY'))
# <Arrow [1980-05-01T00:00:00+00:00]>

print('\nSome ISO-8601 compliant strings are recognized and parsed without a format string:')
print("arrow.get('2013-09-30T15:34:00.000-07:00')",
      arrow.get('2013-09-30T15:34:00.000-07:00'))
# <Arrow [2013-09-30T15:34:00-07:00]>

print('\nArrow objects can be instantiated directly too, with the same arguments as a datetime:')
print("arrow.get(2013, 5, 5)",
      arrow.get(2013, 5, 5))
# <Arrow [2013-05-05T00:00:00+00:00]>
print('arrow.Arrow(2013, 5, 5)',
      arrow.Arrow(2013, 5, 5))
# <Arrow [2013-05-05T00:00:00+00:00]>

print("arrow.get('Wed May 30 22:24:34 2012', 'ddd MMM DD HH:mm:ss YYYY'): ",
      arrow.get('Wed May 30 22:24:34 2012', 'ddd MMM DD HH:mm:ss YYYY'))

ts = arrow.get('Wed May 30 22:24:34 2012', 'ddd MMM DD HH:mm:ss YYYY')
print(type(ts))

print(arrow.get('Wed May 30 22:24:34 2012', 'ddd MMM DD HH:mm:ss YYYY').format('YYYY'))
