import random
import time


def registerLog(logfile='file.log'):
    with open(logfile, 'a') as file:
        file.write(str(time.time()) + '\n')


def registerFakeLog(logfile='file.log'):
    with open(logfile, 'a') as file:
        file.write(str(time.time()) + '\n')
    time.sleep(random.random())


def createFakeLogFile(logfile='file.log'):
    while True:
        time.sleep(random.random())
        registerLog(logfile)


if __name__ == '__main__':
    for i in range(100):
        registerLog('aquitava.log')

    for i, j in enumerate(range(101)):
        registerFakeLog('aquitava.log')
