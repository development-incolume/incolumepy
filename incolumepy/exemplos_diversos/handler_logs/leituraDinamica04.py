def monitorar(logfile='file.log'):
    '''
    Não funcionou adequadamente...
    :param logfile:
    :return:
    '''
    with open(logfile) as file:
        while True:
            linha = file.readline().strip()
            yield linha


if __name__ == '__main__':
    logfile = 'file.log'
    for item, linha in enumerate(monitorar(logfile)):
        # print(item, linha)
        # print('%s: %s' % (item, linha))
        print('{:6d}: {}'.format(item, linha))
