import tkinter
from time import strftime

class Application:
    def __init__(self, master=None):
        self.rel = tkinter.Label()
        self.rel['text'] = 'none'
        self.rel.pack()
        self.rel['font'] = 'Helvetica 120 bold'
        self.rel['fg'] = 'red'
        self.rel['fg'] = 'blue'
        self.tac()
        pass

    def tic(self):
        self.rel['text'] = strftime('%H:%M:%S')

    def tac(self):
        self.tic()
        self.rel.after(1000, self.tac)




if __name__ == '__main__':
    root = tkinter.Tk()
    Application(root)
    root.mainloop()
    pass