import threading
from incolumepy.testes.miscelanea.Fibonacci2 import fibonacci

def fibo(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a+b
    data[threading.currentThread().getName()] = a

def get_results():
    #enquanto houverem threads a trabalhar vais esperar, a main thread conta como 1, quando as threads acabarem vais retornar os dados
    while(threading.active_count() > 1):
        continue
    return data

if __name__ == '__main__':
    data = {} # armazenar os dados
    fibos = [10, 15, 20, 30]
    for k, v in enumerate(fibos, 1):
        t = threading.Thread(target=fibo, args=(v,), name='t_{}'.format(k)) # preparar cada thread
        t.start() # começar cada thread

    print(get_results()) # trabalho feito por cada thread