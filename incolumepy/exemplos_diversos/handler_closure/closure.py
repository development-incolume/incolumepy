#!/usr/bin/env python
# -*- coding: utf-8 -*-


def func_oi():
    msg = 'Oi'

    def func_interna():
        print(msg)
    return func_interna


def func_ext(mensagem):
    msg = mensagem

    def func_interna():
        print(msg)
    return func_interna


def func_externa(msg):
    def func_interna():
        print(msg)
    return func_interna


def decorator_function0(original_function):
    def wrapper_function():
        return original_function()
    return wrapper_function


def display():
    print('display function run')


decorator_display = decorator_function0(display)


@decorator_function0
def dis_play():
    print('dis_play')


def decorator_function(original_function):
    def wrapper_function(*args, **kwargs):
        print(F"wrapper executed this before {original_function.__name__}")
        return original_function(*args, **kwargs)
    return wrapper_function


@decorator_function
def display_info(name, age):
    print(F'display_info run with arguments: ({name}, {age})')


class DecoratorClass:
    def __init__(self, original_function):
        self.original_function = original_function

    def __call__(self, *args, **kwargs):
        print(F"wrapper executed this before {self.original_function.__name__}")
        return self.original_function(*args, **kwargs)


@DecoratorClass
def display_onclass(name, age):
    print(F'display_info run with arguments: ({name}, {age})')


def run():
    f1 = func_oi()
    f1()

    f2 = func_ext('Olá!')
    f3 = func_ext('Tchau!')
    f2()
    f3()

    f4 = func_externa('Blbabla')
    f4()

    decorator_display()
    dis_play()

    display_info('john', 25)
    display_onclass('jonny', 30)


if __name__ == '__main__':
    run()
