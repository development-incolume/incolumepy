class int42(int):
    def __init__(self, n):
        int.__init__(n)

    def __add__(self, other):
        return 42

    def __str__(self):
        return '42'


if __name__ == '__main__':
    a = int42(2)
    print(type(a))
    print(a)
    print(a + 5)
