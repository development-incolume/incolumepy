'''
http://recologia.com.br/2015/07/programacao-multi-thread-em-python/
'''

import threading
import time
import random

class MinhaThread (threading.Thread):
    def __init__(self, threadID, nome, contador):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.nome = nome
        self.contador = contador

    def run(self):
        print ("Iniciando thread %s com %d processos" % (self.name, self.contador))
        processo(self.nome, self.contador)
        print ("Finalizando " + self.nome)

def processo(nome, contador):
    while contador:
        print ("Thread %s executando o processo %d" % (nome, contador))

        time.sleep(random.random()) # retardo aleatório na execução do processo, simula carga
        contador -= 1

if __name__ == '__main__':
    # Criando as threads
    thread1 = MinhaThread(1, "Alice", 8)
    thread2 = MinhaThread(2, "Bob", 8)

    # Comecando novas Threads
    thread1.start()
    thread2.start()

    threads = []
    threads.append(thread1)
    threads.append(thread2)

    for t in threads:
        t.join()

    print ("Saindo da main")
