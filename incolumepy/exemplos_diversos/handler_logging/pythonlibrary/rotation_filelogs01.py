import logging
import time
import os
from logging.handlers import TimedRotatingFileHandler


# ----------------------------------------------------------------------
def create_timed_rotating_log(path):
    """"""
    logger = logging.getLogger("Rotating Log")
    logger.setLevel(logging.INFO)

    handler = TimedRotatingFileHandler(path,
                                       when="m",
                                       interval=1,
                                       backupCount=5)
    logger.addHandler(handler)

    for i in range(6):
        logger.info("This is a test!")
        logger.debug('delay..')
        time.sleep(75)
        logger.debug('fim delay.')


# ----------------------------------------------------------------------
if __name__ == "__main__":
    log_file = os.path.abspath(os.path.join('..', 'tmp', '{}.log'.format(os.path.basename(__file__).split('.')[0])))
    print(log_file)
    create_timed_rotating_log(log_file)