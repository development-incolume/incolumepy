from incolumepy.testes.handler_logging import employee
from incolumepy.testes.handler_logging.logger_cfg import *

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s:%(name)s:%(module)s:%(message)s')

file_handler = logging.FileHandler('info.log')
file_handler.setLevel(logging.ERROR)
file_handler.setFormatter(formatter)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)

logger.addHandler(file_handler)
logger.addHandler(stream_handler)



def add(x, y):
    result = x + y
    logging.debug('Add: {} + {} = {}'.format(x, y, result))
    return result


def subtract(x, y):
    result = x - y
    logging.debug('Sub: {} - {} = {}'.format(x, y, result))
    return result


def multiply(x, y):
    result = x * y
    logging.debug('Mul: {} * {} = {}'.format(x, y, result))
    return result


def divide(x, y):
    try:
        result = x / y
        logging.debug('Div: {} / {} = {}'.format(x, y, result))
    except ZeroDivisionError as e:
        # logger.error('Tentativa divisão por zero')
        # 2017-07-05 09:25:14,281:__main__:Tentativa divisão por zero
        logger.exception('Tentativa divisão por zero')
    else:
        return result


if __name__ == '__main__':
    print(add(2, 3))
    print(subtract(2, 3))
    print(multiply(2, 3))
    print(divide(2, 0))
    e = employee.Employee('Ana', 'Brito')
    print(e)
