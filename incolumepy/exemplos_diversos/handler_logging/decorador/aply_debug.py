from incolumepy.testes.handler_logging.decorador.truncus import debug


@debug
def add(x, y):
    return x + y


add(3, 4)
