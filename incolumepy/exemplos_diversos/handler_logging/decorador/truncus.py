import logging
import tempfile
from functools import wraps

tf = tempfile.NamedTemporaryFile(suffix='.log', prefix='debug_', dir='.', delete=True)
print(tf.name)

LOG_FORMAT = '%(asctime)s:%(levelname)s:%(name)s:%(module)s:%(message)s'
logging.basicConfig(filename=tf,
                    filemode='w',
                    level=logging.DEBUG,
                    format=LOG_FORMAT)

'''
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(LOG_FORMAT)

file_handler = logging.FileHandler(tf)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)

logger.addHandler(file_handler)
logger.addHandler(stream_handler)
'''


def debug(func):
    ''' logging para execução'''
    msg = func.__qualname__
    logger = logging.getLogger(func.__module__)

    @wraps(func)
    def wrapped(*args, **kwargs):
        print(msg, )
        logger.debug(msg)
        return func(*args, **kwargs)

    return wrapped
