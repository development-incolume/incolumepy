import logging.handlers
import os
import zlib

def namer(name):
    return name + ".gz"

def rotator(source, dest):
    with open(source, "rb") as sf:
        data = sf.read()
        compressed = zlib.compress(data, 9)
        with open(dest, "wb") as df:
            df.write(compressed)
    os.remove(source)

if __name__ == '__main__':
    my_logger = logging.getLogger('MyLogger')
    my_logger.setLevel(logging.DEBUG)

    file = os.path.abspath(os.path.join('..', 'tmp',
        '{}.log'.format(os.path.basename(__file__).split('.')[0])))
    rh = logging.handlers.RotatingFileHandler(file, maxBytes=10, backupCount=10)
    rh.rotator = rotator
    rh.namer = namer
    formatter = logging.Formatter('%(asctime)s %(name)-15s %(levelname)-8s %(processName)-10s %(message)s')

    rh.setFormatter(formatter)
    my_logger.addHandler(rh)


    for i in range(101):
        my_logger.debug('starting...')
        my_logger.debug(i)
        my_logger.debug('..end')