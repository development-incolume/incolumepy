import logging.handlers
import os
import zlib


def namer(name):
    return name + ".gz"


def rotator(source, dest):
    with open(source, "rb") as sf, open(dest, "wb") as df:
        data = sf.read()
        compressed = zlib.compress(data, 9)
        df.write(compressed)
    os.remove(source)


if __name__ == '__main__':
    my_logger = logging.getLogger('MyLogger')
    my_logger.setLevel(logging.DEBUG)

    filelog = os.path.join('..', 'tmp', '{}.log'.format(os.path.basename(__file__).split('.')[0]))
    rh = logging.handlers.RotatingFileHandler(filelog, maxBytes=10, backupCount=7)
    rh.rotator = rotator
    rh.namer = namer
    my_logger.addHandler(rh)

    for i in range(121):
        my_logger.debug(i)
