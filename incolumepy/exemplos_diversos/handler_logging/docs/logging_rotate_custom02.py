import logging.handlers
import os
import zlib


def namer(name):
    return "{}.log.gz".format(name.split('.')[0])


def rotator(source, dest):
    with open(source, "rb") as sf, open(dest, "wb") as df:
        data = sf.read()
        compressed = zlib.compress(data, 9)
        df.write(compressed)
    os.remove(source)


if __name__ == '__main__':
    my_logger = logging.getLogger('MyLogger')
    my_logger.setLevel(logging.DEBUG)

    formatter = '%(asctime)-15s %(name)-5s %(levelname)-8s IP: %(ip)-15s User: %(user)-8s %(message)s'
    rh = logging.handlers.RotatingFileHandler(
        '{}.log'.format(os.path.basename(__file__).split('.')[0]), maxBytes=10, backupCount=7)
    rh.rotator = rotator
    rh.namer = namer
    my_logger.addHandler(rh)
    my_logger.addHandler(formatter)

    for i in range(121):
        my_logger.debug(i)
