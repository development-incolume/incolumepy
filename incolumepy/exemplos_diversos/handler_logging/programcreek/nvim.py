import logging
import logging.handlers
from logging.handlers import RotatingFileHandler


def _init_logger():
    formatter = _LogFormatter('%(asctime)s %(levelname)-5s %(name)s@%(funcName)s:%(lineno)d %(message)s')

    logger = logging.getLogger('NeoVintageous')
    logger.setLevel(logging.DEBUG)

    # Stream handler
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    # File handler
    log_file = _log_file()
    if log_file:
        file_handler = RotatingFileHandler(
            log_file,
            maxBytes=10000000,  # 10000000 = 10MB
            backupCount=2
        )
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

        logger.debug('debug log file: \'{}\''.format(log_file))
    else:
        console_message('could not create log file \'{}\''.format(log_file))
