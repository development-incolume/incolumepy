import logging

# logger = logging.getLogger(__name__)
logging.basicConfig(filename='employee.log',
                    level=logging.INFO,
                    format='%(levelname)s:%(asctime)s:%(name)s:%(message)s')


class Employee:
    def __init__(self, firstname, lastname, domain='incolume.com.br'):
        self.firstname = firstname
        self.lastname = lastname
        self.domain = domain
        logging.info('Create employee %s - %s' % (self.fullname, self.email))

    @property
    def fullname(self):
        return '{} {}'.format(self.firstname, self.lastname)

    @property
    def email(self):
        return '{}.{}@{}'.format(self.firstname, self.lastname, self.domain).lower()

    def __str__(self):
        return '(%s, %s)' % (self.fullname, self.email)

if __name__ == '__main__':
    emp1 = Employee('John', 'Smith')
    emp2 = Employee('Ricardo', 'Brito')
    emp3 = Employee('Jane', 'Doe')
    emp4 = Employee('Ana', 'Brito')
    emp5 = Employee('Ada', 'Brito')
    emp6 = Employee('Leni', 'Brito')
    emp7 = Employee('Braucileny', 'Brito')
