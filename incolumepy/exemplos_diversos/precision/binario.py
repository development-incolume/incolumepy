def f_bin01():
    return 0b10


def f_bin02():
    return 0b11


def int2bin(i):
    return bin(i)


def bin2int(b):
    return int(b)


if __name__ == '__main__':
    print(f_bin01())
    print(f_bin02())

    print(int2bin(10))
    print(bin2int(0b10))
