def f_octal01():
    return 0o10


def f_octal02():
    return 0o11


def f_octal00(o):
    return ('0o%i' % o)


def int2oct(i):
    return oct(i)


def oct2int(o):
    return int(o)


if __name__ == '__main__':
    print(f_octal01())
    print(f_octal02())
    print(f_octal00(16))

    print(int2oct(9))
    print(oct2int(0o20))
