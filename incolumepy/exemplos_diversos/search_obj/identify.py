class MyClass:
    def search(self):
        print(Search.search(self))


class Mailbox:
    def search(self):
        print(Search.search(self))


class Document:
    def search(self):
        print(Search.search(self))

    pass


class Search:
    @staticmethod
    def search(obj):
        if isinstance(obj, Mailbox):
            return "its a mailbox"

        elif isinstance(obj, Document):
            return "its a document"

        elif isinstance(obj, MyClass):
            return "obj is my object"

        elif isinstance(obj, str):
            return repr(obj), "is an 8-bit string"

        elif isinstance(obj, (int, float, complex)):
            return obj, "is a built-in number type"

        elif isinstance(obj, (tuple, list, dict, set)):
            return "object is a built-in container type"

        else:
            return "unknown object"


def test_a0(capsys):
    a = MyClass()
    assert a.search() == None
    out, err = capsys.readouterr()
    assert out == 'obj is my object\n'


def test_a1(capsys):
    b = Mailbox()
    assert b.search() == None
    out, err = capsys.readouterr()
    assert out == 'its a mailbox\n'


def test_a2(capsys):
    c = Document()
    assert c.search() == None
    out, err = capsys.readouterr()
    assert out == 'its a document\n'
    assert err == ''


def test_b0(capsys):
    s = 'a'
    assert Search.search(s) == ("'a'", 'is an 8-bit string')
    out, err = capsys.readouterr()
    assert out == ''
    assert err == ''


def test_b1(capsys):
    s = 1.0
    assert Search.search(s) == (1.0, 'is a built-in number type')
    out, err = capsys.readouterr()
    assert out == ''
    assert err == ''


def test_b2(capsys):
    s = 1
    assert Search.search(s) == (1, 'is a built-in number type')
    out, err = capsys.readouterr()
    assert out == ''
    assert err == ''


def test_b3(capsys):
    s = 1j
    assert Search.search(s) == (1j, 'is a built-in number type')
    out, err = capsys.readouterr()
    assert out == ''
    assert err == ''


def test_c1(capsys):
    s = set()
    assert Search.search(s) == 'object is a built-in container type'
    out, err = capsys.readouterr()
    assert out == ''
    assert err == ''


def test_c2(capsys):
    s = dict()
    assert Search.search(s) == 'object is a built-in container type'
    out, err = capsys.readouterr()
    assert out == ''
    assert err == ''


def test_c3(capsys):
    s = list()
    assert Search.search(s) == 'object is a built-in container type'
    out, err = capsys.readouterr()
    assert out == ''
    assert err == ''


def test_c4(capsys):
    s = tuple()
    assert Search.search(s) == 'object is a built-in container type'
    out, err = capsys.readouterr()
    assert out == ''
    assert err == ''


if __name__ == '__main__':
    # pytest identify.py
    pass
