#!/usr/bin/env python
# -*- coding: utf-8 -*-
# https://www.youtube.com/watch?v=Ojt9da2E3ao


import getpass
import sys


def autenticate0():
    user = input("informe login: ")
    pw = getpass.fallback_getpass(prompt="informe senha: ", stream=None)
    # pw = getpass.unix_getpass()

    return user, pw


def autenticate():
    pw = getpass.fallback_getpass(stream=sys.stderr)
    return pw


if __name__ == '__main__':
    print(autenticate())
