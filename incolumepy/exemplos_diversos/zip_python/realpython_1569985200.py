#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys

numbers = [1, 2, 3]
letters = ['a', 'b', 'c']
floats = [4.0, 5.0, 6.0]
operators = ['*', '/', '+']
s1 = {2, 3, 1}
s2 = {'b', 'a', 'c'}


def exemplo1():
    '''Passing n Arguments'''
    zipped = zip(numbers, letters)
    print(type(zipped))
    print(list(zipped))


def exemplo2():
    '''Passing n Arguments'''
    print(list(zip(s1, s2)))


def exemplo3():
    '''Passing No Arguments'''
    print(list(zip()))


def exemplo4():
    '''Passing One Argument'''
    print(list(zip(numbers)))


def exemplo5():
    ''''''
    zipped = zip(numbers, letters, floats)  # Three input iterables
    print(list(zipped))


def exemplo6():
    '''Passing Arguments of Unequal Length'''
    print(list(zip(range(5), range(100, 0, -1))))


def exemplo7():
    from itertools import zip_longest
    longest = range(5)
    zipped = zip_longest(numbers, letters, longest, fillvalue='?')
    print(list(zipped))


def exemplo8():
    for l, n in zip(letters, numbers):
        print(f'Letter: {l}', end=', ')
        print(f'Number: {n}')


def exemplo9():
    for l, n, o in zip(letters, numbers, operators):
        print(f'Letter: {l}', end=', ')
        print(f'Number: {n}', end=', ')
        print(f'Operator: {o}')


def exemplo10():
    dict_one = {'name': 'John', 'last_name': 'Doe', 'job': 'Python Consultant'}
    dict_two = {'name': 'Jane', 'last_name': 'Doe', 'job': 'Community Manager'}
    for (k1, v1), (k2, v2) in zip(dict_one.items(), dict_two.items()):
        print(k1, '->', v1)
        print(k2, '->', v2)


def exemplo11():
    '''Unzipping a Sequence'''
    pairs = [(1, 'a'), (2, 'b'), (3, 'c'), (4, 'd')]
    numbers, letters = zip(*pairs)
    print(pairs)
    print(f"unzipped: {numbers} e {letters}")


def exemplo12():
    '''Sorting in Parallel'''
    letters = ['b', 'a', 'd', 'c']
    numbers = [2, 4, 3, 1]
    data1 = list(zip(letters, numbers))
    print(data1)
    # [('b', 2), ('a', 4), ('d', 3), ('c', 1)]
    data1.sort()  # Sort by letters
    print(data1)
    # [('a', 4), ('b', 2), ('c', 1), ('d', 3)]
    data2 = list(zip(numbers, letters))
    print(data2)
    # [(2, 'b'), (4, 'a'), (3, 'd'), (1, 'c')]
    data2.sort()  # Sort by numbers
    print(data2)


def exemplo13():
    letters = ['b', 'a', 'd', 'c']
    numbers = [2, 4, 3, 1]
    print(list(zip(letters, numbers)))
    print(sorted(zip(letters, numbers)))  # Sort by letters
    print(sorted(zip(letters, numbers), key=lambda x: x[1]))  # Sort by letters


def exemplo14():
    total_sales = [52000.00, 51000.00, 48000.00]
    prod_cost = [46800.00, 45900.00, 43200.00]
    for sales, costs in zip(total_sales, prod_cost):
        profit = sales - costs
        print(f'Total profit: {profit}')


def exemplo15():
    fields = ['name', 'last_name', 'age', 'job']
    values = ['John', 'Doe', '45', 'Python Developer']
    a_dict = dict(zip(fields, values))
    print(a_dict)

    new_job = ['Python Consultant']
    field = ['job']
    a_dict.update(zip(field, new_job))
    print(a_dict)


def run():
    for f in range(1, 16):
        try:
            func = eval(f'exemplo{f}')
            print('===')
            print(func.__name__)
            if func.__doc__:
                print(func.__doc__)
            print('---')
            func()
            print('---')
        except NameError:
            print(sys.exc_info())


if __name__ == '__main__':
    run()