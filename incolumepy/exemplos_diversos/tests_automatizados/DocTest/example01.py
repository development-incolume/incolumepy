def fibo(n):
    """
    Neste test somente apresenta o exemplo com erro.
    >>> fibo(0)
    0
    >>> fibo(1)
    1
    >>> fibo(10)
    55
    >>> fibo(11)
    089
    """
    if n < 2:
        return n
    else:
        return fibo(n - 1) + fibo(n - 2)


if __name__ == '__main__':
    import doctest

    doctest.testmod()
