# -*- coding: utf-8 -*-

from unittest import TestCase

from incolumepy.geometria.figura_geometrica import FiguraGeometrica
from incolumepy.geometria.retangulo import Retangulo


class TestRetangulo(TestCase):
    def setUp(self):
        TestCase.setUp(self)
        self.fig = Retangulo()

    def test_herança(self):
        self.assertTrue(isinstance(self.fig, FiguraGeometrica))
        self.assertIsInstance(self.fig, FiguraGeometrica)

    def test_dict(self):
        self.assertIsNotNone(self.fig.__dict__)

    def test_dict_(self):
        self.assertIn('base', self.fig.__dict__)
        self.assertIn('altura', self.fig.__dict__)

    def test_encapsulamento(self):
        with self.assertRaises(AttributeError):
            self.fig.value = 0
        self.assertIsNot('value', self.fig.__dict__)

    def test_get_area(self):
        # Verificamos se o resultado é o esperado
        # de acordo com a formula de area do quadrado
        self.fig.base = 2
        self.fig.altura = 3
        self.assertEqual(self.fig.get_area(), 6)
        self.fig.base = 5
        self.fig.altura = 7
        self.assertEqual(self.fig.get_area(), 35.0)

    def test_get_perimetro(self):
        self.fig.base = 2
        self.fig.altura = 3
        self.assertEqual(self.fig.get_perimetro(), 10)
        self.fig.base = 5
        self.fig.altura = 7
        self.assertEqual(self.fig.get_perimetro(), 24.0)
