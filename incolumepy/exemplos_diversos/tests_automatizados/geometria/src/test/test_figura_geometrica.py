# -*- coding: utf-8 -*-
import os
import sys
from unittest import TestCase

try:
    from incolumepy.geometria.figura_geometrica import FiguraGeometrica
except:
    sys.path.append(os.path.join(os.path.dirname(__file__), *'incolumepy.geometria'.split('.')))
    from figura_geometrica import FiguraGeometrica


# O nome da classe deve iniciar com a palavra Test
class TestFiguraGeometrico(TestCase):
    # Serve para incializar variavei que usaremos
    # globalmente nos testes
    def setUp(self):
        TestCase.setUp(self)
        self.fig = FiguraGeometrica()

    # Retorna uma NotImplementedError
    # O nome do metodo deve comecar com test
    def test_get_area(self):
        self.assertRaises(NotImplementedError, self.fig.get_area)

    # Retorna uma NotImplementedError
    # O nome do metodo deve comecar com test
    def test_get_perimetro(self):
        self.assertRaises(NotImplementedError, self.fig.get_perimetro)

    def test_set_attr(self):
        self.assertTrue(True)
        with self.assertRaises(AttributeError):
            self.fig.value = 0

    def test_get_attr(self):
        with self.assertRaises(AttributeError):
            str(self.fig.value)
