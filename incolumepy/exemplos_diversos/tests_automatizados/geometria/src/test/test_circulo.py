# -*- coding: utf-8 -*-
import math
from unittest import TestCase

from incolumepy.geometria.circulo import Circulo
from incolumepy.geometria.figura_geometrica import FiguraGeometrica


class TestCirculo(TestCase):
    def setUp(self):
        TestCase.setUp(self)
        self.fig = Circulo()

    def test_herança(self):
        self.assertIsInstance(self.fig, FiguraGeometrica)

    def test_dict(self):
        self.assertIsNotNone(self.fig.__dict__)

    def test_dict_(self):
        self.assertIn('raio', self.fig.__dict__)

    def test_encapsulamento(self):
        with self.assertRaises(AttributeError):
            self.fig.value = 0
        self.assertIsNot('value', self.fig.__dict__)

    def test_get_area(self):
        # Utilizamos a formula diretamente por conveniencia
        # já que math.pi e double e sendo assim, possui
        # muitas casas decimais
        self.fig.raio = 2
        area = math.pi * self.fig.raio ** 2
        self.assertEqual(self.fig.get_area(), area)

        self.fig.raio = 7.0
        area = math.pi * self.fig.raio ** 2
        self.assertEqual(self.fig.get_area(), area)

    def test_get_perimetro(self):
        self.fig.raio = 2
        perimetro = 2 * math.pi * self.fig.raio
        self.assertEqual(self.fig.get_perimetro(), perimetro)

        self.fig.raio = 7.0
        perimetro = 2 * math.pi * self.fig.raio
        self.assertEqual(self.fig.get_perimetro(), perimetro)
