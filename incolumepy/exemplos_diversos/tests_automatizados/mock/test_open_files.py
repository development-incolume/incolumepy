from mock import MagicMock, patch
import platform
from distutils.version import LooseVersion

assert LooseVersion(platform.python_version()) >= '3.3.0', "Only for Python 3.3.0+"
import builtins as __builtin__



# mock
my_mock = MagicMock()
with patch(__builtin__.open, my_mock):
    manager = my_mock.return_value.__enter__.return_value
    manager.rsplit = str.rsplit
    manager.read.return_value = 'some data'
    with open('foo') as h:
        data = h.read()



print(data)
# 'some data'
my_mock.assert_called_once_with('foo')
