from collections import namedtuple

Point = namedtuple('Point', ['x', 'y'])
Color = namedtuple('Color', 'red green blue')
Pixel = namedtuple('Pixel', Point._fields + Color._fields)

a = Pixel(11, 22, 128, 255, 0)
b = Pixel(x=11, y=22, red=128, green=255, blue=0)

print(a == b)
print(a is b)

print(a)
print(b)
