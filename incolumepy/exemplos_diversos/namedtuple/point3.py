from collections import namedtuple

Point = namedtuple('Point', 'x y')

Point3d = namedtuple('Point3d', Point._fields + ('z',))