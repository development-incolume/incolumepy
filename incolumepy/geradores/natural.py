def gen_natural():
    x = 0
    while True:
        x += 1
        yield x


if __name__ == '__main__':
    i = gen_natural()
    print(type(i))
    print(i)
    print(next(i))
    for x in range(10):
        print(next(i))
