__verison__ = 1.0


def gen_password(num_caract=8):
    import random

    '''
    gerador de senhas infinito com metodo gerador yield
    '''

    c = "abcdefghijklmnopqrstuvwxyz"
    c1 = c.upper()
    c2 = '1234567890'
    c3 = '!@#$%&*()_-§.+=<>'
    char = c + c1 + c2 + c3

    passwds = []
    passwd = ""
    try:
        num_caract = int(num_caract)
        if num_caract < 0:
            raise ValueError("Erro: (num_caract) número negativo, deve ser um inteiro positivo!")
        elif 0 <= num_caract < 8:
            raise ValueError("Erro: (num_caract) Tem que ter pelo menos 8 caracteres, deve ser um inteiro positivo!.")
        else:

            while True:

                while len(passwd) != num_caract:
                    passwd += random.choice(char)
                passwds.append(passwd)
                yield passwds.pop(0)
    except TypeError as e:
        print(e, 'Erro: (num_caract) quantidade de caracteres da senha, deve ser um inteiro positivo!')
        return False
    except ValueError as e:
        print(e)
        return False


def gen_pw0(minimo: int = 8, nums: bool = True, chars: bool = True):
    from string import ascii_letters, punctuation, digits
    from random import sample

    s = ascii_letters[:]
    if nums:
        s += digits
    if chars:
        s += punctuation

    while True:
        yield ''.join(sample(s, k=minimo))


def gen_pw(minimo: int = 8, nums: bool = True, letters: bool = True, chars: bool = True):
    from string import ascii_letters, punctuation, digits
    from random import sample
    s = ''
    if not (nums & letters):
        nums = True

    if letters:
        s += ascii_letters[:]

    if nums:
        s += digits
    if chars:
        s += punctuation

    while True:
        yield ''.join(sample(s, k=minimo))


def run():
    pw = gen_pw(nums=False, chars=False)
    for i in range(10):
        print(next(pw))


if __name__ == '__main__':
    run()
