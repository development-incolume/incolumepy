from time import time


def gen_epoch(precision=None):
    '''
    Return a epoch, with precision indicate.
    :param precision: int or str, default None
    :return: str
    '''
    try:
        if precision != None:
            precision = int(precision)
        while True:
            yield {
                0: '{:.0f}'.format(time()),
                1: '{:.1f}'.format(time()),
                2: '{:.2f}'.format(time()),
                3: '{:.3f}'.format(time()),
                4: '{:.4f}'.format(time()),
                5: '{:.5f}'.format(time()),
                6: '{:.6f}'.format(time()),
            }.get(precision, '{:}'.format(time()))
    except:
        raise


if __name__ == '__main__':
    a = gen_epoch('1')
    print(type(next(a)))
    b = gen_epoch()
    c = gen_epoch(6)
    for i in range(10):
        print(next(a))
        print(next(b))
        print(next(c))
