import datetime
import random


def gen_random_timestamp():
    while True:
        year = random.randint(1980, 2015)
        month = random.randint(1, 12)
        day = random.randint(1, 28)
        hour = random.randint(1, 23)
        minute = random.randint(1, 59)
        second = random.randint(1, 59)
        microsecond = random.randint(1, 999999)
        date = datetime.datetime(
            year, month, day, hour, minute, second, microsecond).isoformat(" ")

        yield date


def gen_timestamp():
    '''2017-07-11T21:57:00.918574'''
    while 1:
        # yield datetime.datetime.now().isoformat()
        yield datetime.datetime.now()


if __name__ == '__main__':
    t = gen_random_timestamp()
    u = gen_timestamp()
    for i in range(10):
        print(next(t))
    for i in range(10):
        print(next(u))
