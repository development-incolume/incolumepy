from incolumepy.my_debugger.debugger import debug


@debug()
def som(x, y):
    x, y = int(x), int(y)
    return x + y


@debug
def sub(x, y):
    x, y = int(x), int(y)
    return x - y


@debug
def mul(x, y):
    x, y = int(x), int(y)
    return x * y


@debug(prefix='***')
def div(x, y):
    x, y = int(x), int(y)
    return x // y


print(som(2, 1))
print(sub(2, 1))
print(mul(2, 1))
print(div(2, 1))
