from incolumepy.my_debugger.debugger import debugattr


@debugattr
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


p = Point(3, 2)

p.x
p.y
