from functools import wraps

def debug(func):
    msg = func.__qualname__

    @wraps(func)
    def wrapped(*args, **kwargs):
        print(msg)
        return func(*args, **kwargs)

    return wrapped