from functools import wraps
import logging


def debug(func):
    ''' logging para execução'''
    msg = func.__qualname__
    log = logging.getLogger(func.__module__)
    log.debug(msg)

    @wraps(func)
    def wrapped(*args, **kwargs):
        print(msg, )
        return func(*args, **kwargs)

    return wrapped
