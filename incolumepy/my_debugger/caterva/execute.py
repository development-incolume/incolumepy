from os import environ
from truncus import debug as debug0
from truncus1 import debug as debug1
from truncus2 import debug as debug2
from truncus3 import debug as debug3
from truncus4 import debug as debug4


@debug0
def add(x, y):
    return x+y

@debug1
def sub(x, y):
    return x-y

@debug2
def mul(x, y):
    return x*y

@debug3
def div(x, y):
    return x/y

@debug4(prefix='>> ')
def mult(x, y):
    return x*y


add(4, 3)
sub(4, 3)
mul(4, 3)
print(environ, '\n>')
div(4, 3)
environ['DEBUG'] = 'True'
print(environ.get('DEBUG'), '\n>')
div(4, 3)
mult(4, 3)