def debug(func):
    ''' implementação de decorador '''

    # func is function to be wrapped
    def wrapped(*args, **kwargs):
        print(func.__name__)
        return func(*args, **kwargs)

    return wrapped