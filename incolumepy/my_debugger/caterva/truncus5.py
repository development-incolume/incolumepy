from functools import wraps, partial
import os
import logging


def debug(func=None, prefix=''):
    if 'DEBUG' not in os.environ:
        return func

    if func is None:
        return partial(debug, prefix=prefix)

    msg = prefix + func.__qualname__
    logger = logging.getLogger(func.__module__)
    logger.debug(msg)

    @wraps(func)
    def wrapper(*args, **kwargs):
        print(msg)
        return func(*args, *kwargs)

    return wrapper
