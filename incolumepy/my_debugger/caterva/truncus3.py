from functools import wraps
import os


def debug(func):
    ''' variavel de ambiente para debug'''
    if 'DEBUG' not in os.environ:
        return func
    msg = func.__qualname__

    @wraps(func)
    def wrapped(*args, **kwargs):
        print(msg, )
        return func(*args, **kwargs)

    return wrapped