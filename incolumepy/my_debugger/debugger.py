import logging
import os
from functools import wraps, partial


def debug0(func):
    ''' implementação de decorador '''

    # func is function to be wrapped
    def wrapped(*args, **kwargs):
        print(func.__name__)
        return func(*args, **kwargs)

    return wrapped


def debug1(func):
    msg = func.__qualname__

    @wraps(func)
    def wrapped(*args, **kwargs):
        print(msg)
        return func(*args, **kwargs)

    return wrapped


def debug2(func):
    ''' logging para execução'''
    msg = func.__qualname__
    log = logging.getLogger(func.__module__)

    @wraps(func)
    def wrapped(*args, **kwargs):
        print(msg, )
        return func(*args, **kwargs)

    return wrapped


def debug3(func):
    ''' variavel de ambiente para debug'''
    if 'DEBUG' not in os.environ:
        return func
    log = logging.getLogger(func.__module__)
    msg = func.__qualname__

    @wraps(func)
    def wrapped(*args, **kwargs):
        print(msg, )
        return func(*args, **kwargs)

    return wrapped


def debug(func=None, prefix=''):
    if func is None:
        return partial(debug, prefix=prefix)
    msg = prefix + func.__qualname__

    @wraps(func)
    def wrapper(*args, **kwargs):
        print(msg)
        return func(*args, *kwargs)

    return wrapper


def debugattr(cls):
    orig_getattr = cls.__getattribute__

    def __getattribute__(self, name):
        print('Get: ', name)
        return orig_getattr(self, name)

    cls.__getattribute__ = __getattribute__
    return cls
