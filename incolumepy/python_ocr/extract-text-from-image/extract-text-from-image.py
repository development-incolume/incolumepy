# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'
from PIL import Image
import pytesseract
from pathlib import Path

img = next(Path('.').glob('*.png'))
assert img.is_file(), f'{img=}'

print(pytesseract.image_to_string(Image.open(img), lang='por'))


