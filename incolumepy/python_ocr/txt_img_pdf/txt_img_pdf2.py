# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'
from PIL import Image
from pathlib import Path
from pdf2image import convert_from_path, convert_from_bytes
from pdf2image.exceptions import (
    PDFInfoNotInstalledError,
    PDFPageCountError,
    PDFSyntaxError
)
import tempfile
import pytesseract

filepdf = next(Path('').parent.glob('*.pdf'))
assert filepdf.is_file(), f'{filepdf=}'


def pdf_image_extractor0(filepdf: (str, Path, bytes)):
    with tempfile.TemporaryDirectory() as path:
        images_from_path = convert_from_path(filepdf, output_folder=path)
        print(images_from_path)


def pdf_image_extractor1(filename: (str, Path, bytes)):
    """example from https://simply-python.com/tag/pdf2image/"""
    import os
    with tempfile.TemporaryDirectory() as path:
        images_from_path = convert_from_path(filename, output_folder=path, last_page=1, first_page=0)

    base_filename = os.path.splitext(os.path.basename(filename))[0] + '.jpg'
    save_dir = 'your_saved_dir'

    for page in images_from_path:
        page.save(os.path.join(save_dir, base_filename), 'JPEG')


def pdf_image_extractor2(filename: (str, Path, bytes)):
    """Variation from https://simply-python.com/tag/pdf2image/"""
    with tempfile.TemporaryDirectory() as path:
        images_from_path = convert_from_path(filename, output_folder=path, last_page=1, first_page=0)
    outputdir = Path(__file__).parent.joinpath('img_converted')
    outputdir.mkdir(exist_ok=True)
    fout = outputdir/filename.with_suffix('.jpg').name

    for page in images_from_path:
        page.save(fout, 'JPEG')


def pdf_image_extractor3(filename: (str, Path, bytes)):
    """Variation from https://simply-python.com/tag/pdf2image/"""
    with tempfile.TemporaryDirectory() as path:
        images_from_path = convert_from_path(filename, output_folder=path, last_page=38, first_page=36)
    outputdir = Path(__file__).parent.joinpath('img_converted')
    outputdir.mkdir(exist_ok=True)
    for page in images_from_path:
        fout = outputdir / filename.with_suffix('.jpg').name
        count = 0
        bname = fout.stem
        while True:
            if fout.is_file():
                fout = fout.with_name(f'{bname}-{count}{fout.suffix}')
            else:
                break
            count += 1
        page.save(fout, 'JPEG')


def pdf_image_extractor(filename: (str, Path, bytes)):
    """"""
    with tempfile.TemporaryDirectory() as path:
        images_from_path = convert_from_path(filename, output_folder=path, last_page=38, first_page=36)
    outputdir = Path(__file__).parent.joinpath('img_converted')
    outputdir.mkdir(exist_ok=True)
    # fout = outputdir/filename.with_suffix('.txt').name

    for page in images_from_path:
        t = pytesseract.image_to_string(page, lang='por')
        print(t)


def run():
    pdf_image_extractor(filepdf)


if __name__ == '__main__':
    run()
