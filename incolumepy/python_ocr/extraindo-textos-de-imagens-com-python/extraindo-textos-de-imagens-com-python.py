# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'
"""
https://rafaelsanches123.github.io/extraindo-textos-de-imagens-com-python

Dependências do OS
pipenv install -d pillow pytesseract
"""

from PIL import Image
# Módulo para a utilização da tecnologia OCR
import pytesseract

# Extraindo o texto da imagem
print(pytesseract.image_to_string(
    Image.open('extraindo-textos-de-imagens-com-python/imagem-teste-com-ocr.jpg'), lang='por')
)
