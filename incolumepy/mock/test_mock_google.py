import mock
import mock_google
from unittest import TestCase, main


class TestClass(TestCase):
    def test_1(self):
        with mock.patch('mock_google.requests.get') as mocked_get:
            mocked_get.return_value.ok = True
            mocked_get.return_value.text = 'Success'
            mocked_get.assert_called_with('http://google.com')
            self.assertEqual('http://google.com', 'Success')
            self.assertTrue(True)


if __name__ == '__main__':
    main()

