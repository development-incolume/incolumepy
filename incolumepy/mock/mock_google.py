import requests


def my_get_url(url):
    '''get content url'''
    return requests.get(url).content
