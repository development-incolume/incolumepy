'''
O algoritmo mais conhecido para gerar números primos é o Crivo de Eratóstenes,
que ao invés de pegar os números possíveis e checar se são primos, pega os
números primos já encontrados e deleta todos os múltiplos.
'''


def primes(n):
    if n < 2:
        return []
    if n == 2:
        return [2]
    nums = range(3, n + 1, 2)
    nums_length = (n // 2) - 1 + (n % 2)
    idx = 0
    idx_sqrtn = (int(n ** 0.5) - 3) // 2
    while idx <= idx_sqrtn:
        nums_at_idx = (idx << 1) + 3
        for j in range(idx * (nums_at_idx + 3) + 3, nums_length, nums_at_idx):
            nums[j] = 0
        idx += 1
        while idx <= idx_sqrtn:
            if nums[idx] != 0:
                break
            idx += 1
    return [2] + [x for x in nums if x != 0]


def meu_primos(limite):
    lista_de_primos = primes(limite)
    print("Primos:", " ".join(str(x) for x in lista_de_primos))
    print("\n\nForam encontrados %d números primos." % len(lista_de_primos))


if __name__ == '__main__':
    pass
    meu_primos(1000)
