import math
import os
import time
import zipfile
from platform import python_version

if python_version() >= '3.0': xrange = range

DEBUG=True
DEBUG=False
def read(*rnames):
    '''
    return content from file informed in '*rnames'
    '''
    if DEBUG: open(os.path.join(os.path.dirname(__file__), *rnames)).read()
    return open(os.path.join(os.path.dirname(__file__), *rnames)).read()

def namespace(s):
    '''
    return the namespace from to s='incolumepy.packege.module'
    '''
    if DEBUG: print (len(s.split('.')))
    l = []
    w = ''
    if len(s.split('.')) > 1:
        for i in range(len(s.split('.'))):
            if i == 0:
                w = s.split('.')[i]
                l.append(w)
            elif i>0 and i < len(s.split('.')) -1:
                w += '.' + s.split('.')[i]
                l.append(w)
    else:
        l.append(s.split('.')[0])
    return l

def gen_fibonacci():
    '''
    Gerador para o proximo elemento da sequencia fibonacci
    '''
    a,b = 0, 1
    while 1:
        yield b
        a, b = b, a + b

def fatorial():
    '''
    Gerador para sequencia fatorial onde o primeiro elemento é zero
    '''
    a = 0
    while 1:
        yield math.factorial(a)
        a+=1

def gen_impares():
    """
    Gera números impares infinitamente...
    """
    i = 1
    while True:
        i += 2
        yield i

def gen_pares():
    """
    Gerador números pares infinitamente...
    """
    i = 0
    while True:
        i += 2
        yield i

def epoch():
    '''
    Gerador da representação para timestamp computacional em segundos desde 1970-01-01 00:00:00 UTC.
    '''
    while True:
        time.sleep(.01)
        yield "%10.2f" % time.time()

def zeroleft(digits=2):
    '''
    retorna numeros com zero a esquerda nos digitos.
    exemplo:
        01, 02, 03, 04  .. 09, 10 [default]
        001, 002, 003 .. 099, 100 [digits = 3]
        0001, 0002, .. 0999, 1000 [digits = 4]
    '''
    n = 0
    digits = "%0"+str(digits)+"d"
    while 1:
        n +=1
        yield digits % n

        
def newFileName(prefix='file',sufix='txt', digits=3):
    '''
        gera nomes para novos arquivos prefix999.sufix
    '''
    c=zeroleft(digits)
    while 1:
        yield "%s%s.%s" % (prefix,next(c),sufix)

def newUniqFile(prefix='file',sufix='txt', digits=3):
    '''
        gera nomes unicos para novos arquivos prefix999.sufix
        exemplo:

        fname = newUniqFile('arq','log',3)
        with open(fname,'w') as f:
            f.write('')
            print "Arquivo %s criado com sucesso!" % fname
    '''
    filename = newFileName(prefix,sufix, digits)
    name = next(filename)
    while os.path.isfile(name):
        name = next(filename)
    return name

def unzip(self, path=''):
    if not path: path = self.path
    with zipfile.ZipFile(self.filename) as zf:
        zf.extractall(path)
    return (0,'extraido para "%s"' % path)


if __name__ == "__main__":
    if python_version() >= '3.0':
        xrange = range
    os.makedirs('tmp', mode=0o777, exist_ok=True)
    print (namespace('incolumepy.package.subpackage.module'))
    print (namespace('incolumepy.package.module'))
    print (namespace('incolumepy.package'))
    print (namespace('incolumepy'))
    print (namespace('incolumepy.package.subpackage.module') == ['incolumepy','incolumepy.package','incolumepy.package.subpackage'])
    print (namespace('incolumepy') == ['incolumepy'])
    print (namespace('incolumepy') == ["incolumepy"])
    f = gen_fibonacci()
    for i in range(10):
        print (f.__next__())

    print ()
    g = fatorial()
    for i in xrange(10):
        print (next(g)),

    print ()
    t = epoch()
    for i in xrange(10):
        print (next(t)),

    print ()
    l = [x for x in [0,1,2,3,4,5,6,7,8,9] if x % 2]
    print (l)

    print ([x*x for x in xrange(10)])

    print ([x for x in xrange(10) if x % 2])
    print ([x for x in xrange(10) if not x % 2])
    g = fatorial()
    print ([next(g) for x in xrange(10)])
    
    g = fatorial()
    print  ([next(g) for x in xrange(6)])


    a = zeroleft()
    print ("zeroleft: ", next(a))
    print ("zeroleft: ", next(a))
    for i in xrange(5): print ('zeroleft: ', next(a))

    a = newFileName('tmp/file')
    for i in xrange(6): print ('newFileName: ', next(a))

    b = newUniqFile('tmp/arq','log')
    for i in xrange(9): print ('newUniqFile: ', b)

    b = newUniqFile('tmp/registro','log',4)
    for i in xrange(9): print ('newUniqFile: ', b)

    for i in xrange(3):
        name = newUniqFile('tmp/file')
        with open(name,'w') as f:
            f.write('')
            print ("Arquivo %s criado com sucesso!" %name)

    for i in xrange(3): 
        b = newUniqFile('tmp/registro','log',4)
        with open(b,'w') as f:
            f.write('')
            print ("Arquivo %s criado com sucesso!" % b)

    for i in xrange(3): 
        b = newUniqFile('tmp/arq','log')
        with open(b,'w') as f:
            f.write('')
            print ("Arquivo %s criado com sucesso!" % b)

