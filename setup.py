import os, pip
from setuptools import setup, find_packages
from incolumepy.utils.utils import namespace

#pip.main(['install','pytest'])
#pip.main(['install','wheel>=0.21.0'])
#pip.main(['install','twine'])


NAME = 'incolumepy'
DESCRIPTION = "pacote utilitario incolumepy com testes uteis"
KEYWORDS='python incolumepy'
AUTHOR = '@britodfbr'
AUTHOR_EMAIL = 'contato@incolume.com.br'
URL = 'http://www.incolume.com.br'
LICENSE='BSD'
CLASSIFIERS='''
        "Programming Language :: Python",
'''
VERSION =  open(os.path.join(NAME, "version.txt")).read().strip()

LONG_DESCRIPTION = (
    open('README.md').read()
    + '\n' 
    'History\n'
    '=======\n'
    + '\n' +
    open(os.path.join("docs", "HISTORY.rst")).read() + "\n"
    'Contributors\n'
    '============\n'
    + '\n' +
    open(os.path.join('docs','CONTRIBUTORS.rst')).read()
    + '\n'
    'Changes\n'
    '=======\n'
    + '\n' +
    open(os.path.join('docs','CHANGES.rst')).read()
    + '\n')


setup(name=NAME,
      version=VERSION,
      description=DESCRIPTION,
      long_description=LONG_DESCRIPTION,
                      
      # Get more strings from http://www.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        CLASSIFIERS
        ],
      keywords=KEYWORDS,
      author=AUTHOR,
      author_email=AUTHOR_EMAIL,
      url=URL,
      license=LICENSE,
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      test_suite='nose.collector',
      tests_require='nose',
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
          'pytest',
      ],
      entry_points={
        'console_scripts': [
            'checkinterval = incolumepy.checkinterval.Check:Check.main',
            'interval = incolumepy.checkinterval.Check:Check.interval'
        ],
        'gui_scripts': [
            'baz = my_package_gui:start_func',
        ],
      },
  
      #entry_points="""
      ## -*- Entry points: -*-

      #[distutils.setup_keywords]
      ##paster_plugins = setuptools.dist:assert_string_list

      #[egg_info.writers]
      ##paster_plugins.txt = setuptools.command.egg_info:write_arg
      #""",
      #paster_plugins = [''],
)
