import unittest

from incolumepy.utils.utils import namespace


class UtilsTest(unittest.TestCase):
    def test_namespace1(self):
        self.assert_(namespace('incolumepy') == ['incolumepy'])

    def test_namespace2(self):
        self.assert_(namespace('incolumepy.package') == ['incolumepy'])

    def test_namespace3(self):
        self.assert_(namespace('incolumepy.package.subpackage') == ['incolumepy', 'incolumepy.package'])

    def test_namespace4(self):
        self.assert_(namespace('incolumepy.package.subpackage.module') == ['incolumepy','incolumepy.package','incolumepy.package.subpackage'])

if __name__ == '__main__':
    unittest.main()
