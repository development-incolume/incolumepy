import unittest
from platform import python_version

from incolumepy.utils.utils import fatorial
from incolumepy.utils.utils import gen_fibonacci

if python_version() > '3.0':
    xrange = range


class UtilsTest(unittest.TestCase):
    def test_generator_fatorial1(self):
        f = fatorial()
        self.assert_(next(f) == 1)

    def test_generator_fatorial2(self):
        f = fatorial()
        for x in xrange(4): next(f)
        self.assert_(next(f) == 24)

    def test_generator_fatorial3(self):
        f = fatorial()
        for x in xrange(6): next(f)
        self.assert_((next(f) == 720))
        pass

    def test_generator_fatorial4(self):
        f = fatorial()
        self.assert_([next(f) for x in xrange(6)] == [1, 1, 2, 6, 24, 120])

    def test_generator_fatorial5(self):
        f = fatorial()
        self.assert_([next(f) for x in xrange(10)] == \
                     [1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880])

    def test_generator_fibonacci1(self):
        f = gen_fibonacci()
        self.assert_(next(f) == 1)

    def test_generator_fibonacci2(self):
        f = gen_fibonacci()
        for x in xrange(4): next(f)
        self.assert_(next(f) == 5)

    def test_generator_fibonacci3(self):
        f = gen_fibonacci()
        for x in xrange(6): next(f)
        self.assert_(next(f) == 13)
        pass

    def test_generator_fibonacci4(self):
        f = gen_fibonacci()
        self.assert_([next(f) for x in xrange(6)] == [1, 1, 2, 3, 5, 8])

    def test_generator_fibonacci5(self):
        f = gen_fibonacci()
        self.assert_([next(f) for x in xrange(10)] == \
                     [1, 1, 2, 3, 5, 8, 13, 21, 34, 55])


if __name__ == '__main__':
    unittest.main()
