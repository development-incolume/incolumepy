#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import re
import pandas as pd
import sys
from collections import namedtuple
from itertools import combinations
from typing import Iterator
import datetime
from time import time


'''extraído em https://www.todamateria.com.br/estados-do-brasil/'''
s = '''Acre (AC) - Rio Branco - Norte
Alagoas (AL) - Maceió - Nordeste
Amapá (AP) - Macapá - Norte
Amazonas (AM) - Manaus - Norte
Bahia (BA) - Salvador - Nordeste
Ceará (CE) - Fortaleza - Nordeste
Distrito Federal (DF) - Brasília - Centro-oeste
Espírito Santo (ES) - Vitória - Sudeste
Goiás (GO) - Goiânia - Centro-oeste
Maranhão (MA) - São Luís - Nordeste
Mato Grosso (MT) - Cuiabá - Centro-Oeste
Mato Grosso do Sul (MS) - Campo Grande - Centro-Oeste
Minas Gerais (MG) - Belo Horizonte - Sudeste
Pará (PA) - Belém - Norte
Paraíba (PB) - João Pessoa - Nordeste
Paraná (PR) - Curitiba - Sul
Pernambuco (PE) - Recife - Nordeste
Piauí (PI) - Teresina - Nordeste
Rio de Janeiro (RJ) - Rio de Janeiro - Sudeste
Rio Grande do Norte (RN) - Natal - Nordeste
Rio Grande do Sul (RS) - Porto Alegre - Sul
Rondônia (RO) - Porto Velho - Norte
Roraima (RR) - Boa Vista - Norte
Santa Catarina (SC) - Florianópolis - Sul
São Paulo (SP) - São Paulo - Sudeste
Sergipe (SE) - Aracaju - Nordeste
Tocantins (TO) - Palmas - Norte'''


def dict_uf():
    d = {}
    for j, i in enumerate(s.split('\n')):
        # print(j, i, end=' ')
        q = re.match(r'([\w\s]+)\s\((\w{2})\)\s-\s([\w\s]+)\s-\s([\w\s-]+)', i)
        # print(type(q))
        if q:
            #print(q.groups())
            d[q.groups().__getitem__(1)] = {
                'uf': q.groups()[0], 'sigla': q.groups()[1], 'capital': q.groups()[2], 'região': q.groups()[3]
            }
    return d


df = pd.DataFrame.from_dict(dict_uf(), orient='index')
df['região'].str.title()


Question = namedtuple("Question", "question correct choices answer")


def pergunta00():
    q1 = '''Qual destes estados a capital começa com a letra 'A'? '''
    possibilities = df[df.capital.str.startswith('A')].uf
    # random.shuffle(possibilities)
    # print(possibilities)
    options = df[~df.capital.str.startswith('A')].uf
    # random.shuffle(options)
    # print(options)
    # (question. opções(choices), correct)
    correct = random.choices(possibilities)
    choices = random.sample(list(options), k=4) + correct
    random.shuffle(choices)
    result = Question(q1, correct, choices, None)
    return result


def pergunta(q1: str, possibilities: str, options: str, qoptions: int = 4, correct: list = None) -> Question:
    if not correct:
        correct = random.choices(list(possibilities))
        choices = random.sample(list(options), k=qoptions) + correct
        random.shuffle(choices)
        return Question(q1.format(correct.__getitem__(0)), correct, choices, None)
    elif correct:
        choices = random.sample(list(options), k=qoptions) + correct
        random.shuffle(choices)
        return Question(q1.format(random.choices(list(possibilities)).__getitem__(0)), correct, choices, None)
    return None


def pergunta01():
    q1 = '''Qual destas capitais pertencem a região norte? '''
    possibilities = df[df['região'] == 'Norte'].capital
    options = df[~df['região'].str.contains('Norte')].capital
    # print(possibilities, options)
    result = pergunta(q1, possibilities, options)
    return result


def pergunta02():
    q1 = '''Qual destas capitais não pertencem a região norte? '''
    possibilities = df[~df['região'].str.contains('Norte')].capital
    options = df[df['região'] == 'Norte'].capital
    # print(possibilities, options)
    result = pergunta(q1, possibilities, options)
    return result


def pergunta03():
    q1 = '''Qual destas capitais pertencem a região sul? '''
    possibilities = df[df['região'].str.contains('Sul')].capital
    options = df[~df['região'].str.contains('Sul')].capital
    # print(possibilities, options)
    result = pergunta(q1, possibilities, options)
    return result


def pergunta04():
    q1 = '''Qual destas capitais não pertencem a região sul? '''
    possibilities = df[~df['região'].str.contains('Sul')].capital
    options = df[df['região'].str.contains('Sul')].capital
    result = pergunta(q1, possibilities, options, 3)
    return result


def pergunta05():
    q1 = '''Qual destas capitais pertencem a região sudeste? '''
    possibilities = df[df['região'].apply(str.lower) == 'sudeste'].capital
    options = df[~df['região'].apply(str.lower).str.contains('sudeste')].capital
    result = pergunta(q1, possibilities, options)
    return result


def pergunta06():
    q1 = '''Qual destas capitais não pertencem a região sudeste? '''
    possibilities = df[~df['região'].apply(str.lower).str.contains('sudeste')].capital
    options = df[df['região'].apply(str.lower) == 'sudeste'].capital
    # print(possibilities, options)
    result = pergunta(q1, possibilities, options)
    return result


def pergunta07():
    q1 = '''Qual destas capitais não pertencem a região nordeste? '''
    possibilities = df[~df['região'].apply(str.lower).str.contains('nordeste')].capital
    options = df[df['região'].apply(str.lower) == 'nordeste'].capital
    result = pergunta(q1, possibilities, options)
    return result


def pergunta08():
    q1 = '''Qual destas capitais pertencem a região nordeste? '''
    possibilities = df[df['região'].apply(str.lower).str.contains('nordeste')].capital
    options = df[~df['região'].apply(str.lower).str.contains('nordeste')].capital
    result = pergunta(q1, possibilities, options)
    return result


def pergunta09():
    q1 = '''Qual destas capitais pertencem a região centro-oeste? '''
    possibilities = df[df['região'].apply(str.lower).str.contains('centro-oeste')].capital
    options = df[~df['região'].apply(str.lower).str.contains('centro-oeste')].capital
    # print(possibilities)
    result = pergunta(q1, possibilities, options)
    return result


def pergunta10():
    q1 = '''Qual destas capitais não pertencem a região centro-oeste? '''
    possibilities = df[~df['região'].apply(str.lower).str.contains('centro-oeste')].capital
    options = df[df['região'].apply(str.lower).str.contains('centro-oeste')].capital
    result = pergunta(q1, possibilities, options)
    return result


def pergunta11():
    regiao = 'norte'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].uf
    options = df[~df['região'].apply(str.lower).str.contains(regiao)]['região'].unique()
    q = '{}: Qual a região desta Unidade Federativa?'
    result = pergunta(q, possibilities, options, correct=[regiao.title()])
    return result


def pergunta12():
    regiao = 'centro-oeste'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].uf
    options = df[~df['região'].apply(str.lower).str.contains(regiao)]['região'].unique()
    q = '{}: Qual a região desta Unidade Federativa?'
    result = pergunta(q, possibilities, options, correct=[regiao.title()])
    return result


def pergunta13():
    regiao = 'nordeste'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].uf
    options = df[~df['região'].apply(str.lower).str.contains(regiao)]['região'].unique()
    q = '{}: Qual a região desta Unidade Federativa?'
    result = pergunta(q, possibilities, options, correct=[regiao.title()])
    return result


def pergunta14():
    regiao = 'sudeste'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].uf
    options = df[~df['região'].apply(str.lower).str.contains(regiao)]['região'].unique()
    q = '{}: Qual a região desta Unidade Federativa?'
    result = pergunta(q, possibilities, options, correct=[regiao.title()])
    return result


def pergunta15():
    regiao = 'sul'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].uf
    options = df[~df['região'].apply(str.lower).str.contains(regiao)]['região'].unique()
    q = '{}: Qual a região desta Unidade Federativa?'
    result = pergunta(q, possibilities, options, correct=[regiao.title()])
    return result


def pergunta16():
    possibilities = df.sample().uf
    options = df[~df.uf.str.contains(possibilities[0])].capital
    correct = list(df[df.uf.str.contains(possibilities[0])].capital)
    q = 'Qual a capital da Unidade Federativa "{}"?'
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta17():
    regiao = random.sample(list(df['região'].unique()), k=1)[0].lower()
    # print(regiao)
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].sample().uf
    options = df[~df.uf.str.contains(possibilities[0])].capital
    correct = list(df[df.uf.str.contains(possibilities[0])].capital)
    q = 'Qual a capital da Unidade Federativa "{}"?'
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta18():
    regiao = 'norte'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].sample().uf
    options = df[~df.uf.str.contains(possibilities[0])].capital
    correct = list(df[df.uf.str.contains(possibilities[0])].capital)
    q = 'Qual a capital da Unidade Federativa "{}"?'
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta19():
    regiao = 'sul'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].sample().uf
    options = df[~df.uf.str.contains(possibilities[0])].capital
    correct = list(df[df.uf.str.contains(possibilities[0])].capital)
    q = 'Qual a capital da Unidade Federativa "{}"?'
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta20():
    regiao = 'nordeste'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].sample().uf
    options = df[~df.uf.str.contains(possibilities[0])].capital
    correct = list(df[df.uf.str.contains(possibilities[0])].capital)
    q = 'Qual a capital da Unidade Federativa "{}"?'
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta21():
    regiao = 'centro-oeste'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].sample().uf
    options = df[~df.uf.str.contains(possibilities[0])].capital
    correct = list(df[df.uf.str.contains(possibilities[0])].capital)
    q = 'Qual a capital da Unidade Federativa "{}"?'
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta22():
    regiao = 'sudeste'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].sample().uf
    options = df[~df.uf.str.contains(possibilities[0])].capital
    correct = list(df[df.uf.str.contains(possibilities[0])].capital)
    q = 'Qual a capital da Unidade Federativa "{}"?'
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta23():
    regiao = random.sample(list(df['região'].unique()), k=1)[0].lower()
    # print(regiao)
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].sample().capital
    options = df[~df.capital.str.contains(possibilities[0])].uf
    correct = list(df[df.capital.str.contains(possibilities[0])].uf)
    q = 'Qual a Unidade Federativa da capital "{}"?'
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta24():
    regiao = 'norte'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].sample().capital
    options = df[~df.capital.str.contains(possibilities[0])].uf
    correct = list(df[df.capital.str.contains(possibilities[0])].uf)
    q = 'Qual a Unidade Federativa da capital "{}"?'
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta25():
    regiao = 'nordeste'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].sample().capital
    options = df[~df.capital.str.contains(possibilities[0])].uf
    correct = list(df[df.capital.str.contains(possibilities[0])].uf)
    q = 'Qual a Unidade Federativa da capital "{}"?'
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta26():
    regiao = 'centro-oeste'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].sample().capital
    options = df[~df.capital.str.contains(possibilities[0])].uf
    correct = list(df[df.capital.str.contains(possibilities[0])].uf)
    q = 'Qual a Unidade Federativa da capital "{}"?'
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta27():
    regiao = 'sudeste'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].sample().capital
    options = df[~df.capital.str.contains(possibilities[0])].uf
    correct = list(df[df.capital.str.contains(possibilities[0])].uf)
    q = 'Qual a Unidade Federativa da capital "{}"?'
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta28():
    regiao = 'sul'
    possibilities = df[df['região'].apply(str.lower).str.contains(regiao)].sample().capital
    options = df[~df.capital.str.contains(possibilities[0])].uf
    correct = list(df[df.capital.str.contains(possibilities[0])].uf)
    q = 'Qual a Unidade Federativa da capital "{}"?'
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def list_options_combination(combination: Iterator, p: Iterator) -> list:
    return [f"{x}, {y} e {z}" for (x, y), z in zip(combinations(combination, 2), p)]


def pergunta29():
    regiao = 'norte'
    p = df[df['região'].apply(str.lower).str.contains(regiao)].capital
    possibilities = [f"{x}, {y} e {z}" for x, y, z in combinations(p, 3)]
    correct = random.sample(possibilities, k=1)
    o = df[~df.capital.str.contains(p[0])].capital
    options = list_options_combination(o, p)
    q = f'Qual das seguintes capitais pertencem a região "{regiao.title()}"?'
    # print(options)
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta30():
    regiao = 'nordeste'
    p = df[df['região'].apply(str.lower).str.contains(regiao)].capital
    possibilities = [f"{x}, {y} e {z}" for x, y, z in combinations(p, 3)]
    correct = random.sample(possibilities, k=1)
    o = df[~df.capital.str.contains(p[0])].capital
    options = [f"{x}, {y} e {z}" for (x, y), z in zip(combinations(o, 2), p)]
    q = f'Qual das seguintes capitais pertencem a região "{regiao.title()}"?'
    # print(options)
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta31():
    regiao = 'centro-oeste'
    p = df[df['região'].apply(str.lower).str.contains(regiao)].capital
    possibilities = [f"{x}, {y} e {z}" for x, y, z in combinations(p, 3)]
    correct = random.sample(possibilities, k=1)
    o = df[~df.capital.str.contains(p[0])].capital
    options = [f"{x}, {y} e {z}" for (x, y), z in zip(combinations(o, 2), p)]
    q = f'Qual das seguintes capitais pertencem a região "{regiao.title()}"?'
    # print(options)
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta32():
    regiao = 'sudeste'
    p = df[df['região'].apply(str.lower).str.contains(regiao)].capital
    possibilities = [f"{x}, {y} e {z}" for x, y, z in combinations(p, 3)]
    correct = random.sample(possibilities, k=1)
    o = df[~df.capital.str.contains(p[0])].capital
    options = [f"{x}, {y} e {z}" for (x, y), z in zip(combinations(o, 2), p)]
    q = f'Qual das seguintes capitais pertencem a região "{regiao.title()}"?'
    # print(options)
    result = pergunta(q, possibilities, options, correct=correct)
    return result


def pergunta33():
    regiao = 'sul'
    p = df[df['região'].apply(str.lower).str.contains(regiao)].capital
    possibilities = [f"{x}, {y} e {z}" for x, y, z in combinations(p, 3)]
    correct = random.sample(possibilities, k=1)
    o = df[~df.capital.str.contains(p[0])].capital
    options = [f"{x}, {y} e {z}" for (x, y), z in zip(combinations(o, 2), p)]
    q = f'Qual das seguintes capitais pertencem a região "{regiao.title()}"?'
    # print(options)
    result = pergunta(q, possibilities, options, correct=correct, qoptions=3)
    return result


def pergunta34():
    regiao = random.sample(list(df['região'].unique()), k=1)[0].lower()
    p = df[df['região'].apply(str.lower).str.contains(regiao)].capital
    possibilities = [f"{x}, {y} e {z}" for x, y, z in combinations(p, 3)]
    correct = random.sample(possibilities, k=1)
    o = df[~df.capital.str.contains(p[0])].capital
    options = [f"{x}, {y} e {z}" for (x, y), z in zip(combinations(o, 2), p)]
    q = f'Qual das seguintes capitais pertencem a região "{regiao.title()}"?'
    # print(options)
    result = pergunta(q, possibilities, options, correct=correct, qoptions=3)
    return result


def pergunta35():
    regiao = random.sample(list(df['região'].unique()), k=1)[0].lower()
    p = df[df['região'].apply(str.lower).str.contains(regiao)].uf
    possibilities = [f"{x}, {y} e {z}" for x, y, z in combinations(p, 3)]
    correct = random.sample(possibilities, k=1)
    o = df[~df.capital.str.contains(p[0])].uf
    options = [f"{x}, {y} e {z}" for (x, y), z in zip(combinations(o, 2), p)]
    q = f'Qual das seguintes UF pertencem a região "{regiao.title()}"?'
    # print(options)
    result = pergunta(q, possibilities, options, correct=correct, qoptions=3)
    return result


def pergunta_uf():
    serie = df.sample()
    possibilities = list(serie.sigla)
    options = df[~df.uf.str.contains(possibilities[0])].sigla
    correct = list(df[df.sigla.str.contains(possibilities[0])].uf)
    q = f'Qual a sigla da UF "{list(serie.uf).__getitem__(0)}"?'
    result = pergunta(q, possibilities, options)
    return result


def pergunta36():
    result = pergunta_uf()
    return result


def pergunta37():
    result = pergunta_uf()
    return result


def pergunta38():
    result = pergunta_uf()
    return result


def pergunta39():
    result = pergunta_uf()
    return result


def pergunta40():
    result = pergunta_uf()
    return result


Candidato = namedtuple('Candidato', 'nome data lista_perguntas')
NewQuestion = namedtuple('NewQuestion', 'question correct choices answer check')


class Quiz:
    def __init__(self, candidato: str, idate: datetime, fdate: datetime, perguntas: list):
        pass


def quiz(perguntas: list) -> list:
    enunciado = '''O Brasil é composto de 26 estados e um Distrito Federal (DF).
     Os estados e o DF também são denominados unidades da Federação ou
      unidades federativas (UF) e ocupam as regiões brasileiras:
       Norte, Nordeste, Centro-Oeste, Sudeste e Sul.
    '''
    candidato = Candidato(nome=input('Qual o seu nome? '), data=datetime.datetime.now(), lista_perguntas=perguntas)
    print('\n\n=====\nQuiz Incolúme\n=====')
    print(enunciado)
    respostas = []
    pontos = 0
    for i, q in enumerate(candidato.lista_perguntas):
        op = 'ABCDEF'
        while 1:
            print(f'{i+1:>2}) ', q.question)
            d = dict(zip(op, q.choices))
            for x, y in d.items():
                print(f"\t{x}) {y}")
            resp = input('Digite a resposta: ').upper()
            # print(resp)
            respostas.append(d.get(resp))
            if [d.get(resp)] == q.correct:
                print('PARABÉNS')
                pontos += 1
            else:
                print('Terá que melhorar!')
            print()
            if resp in op:
                break
    fim = datetime.datetime.now()

    answers = [NewQuestion(a, b, c, d, b == [d]) for (a, b, c, _), d in zip(candidato.lista_perguntas, respostas)]
    # print(answers)
    dframe = pd.DataFrame().from_dict(answers)
    print(dframe.tail())
    # dframe.to_csv('teste.csv')
    w = input('Deseja gravar as respostas?')
    if w.lower() in ['s', 'y', 'sim', 'yes']:
        with open(f"quiz_{int(time())}.txt", 'w') as fout:
            fout.write(f'{"Nome":<10}: {candidato.nome}\n')
            fout.write(f'{"Data":<10}: {candidato.data.strftime("%Y-%m-%d %H:%M")}\n')
            fout.write(f'{"Duração":<10}: {(fim - candidato.data).total_seconds()//60} Minutos\n')
            fout.write(f'{"Pontuação":<10}: {pontos:0>3}\n')
            # [fout.write(f'{a}\n') for a in answers]
            # for i in dframe.values: print(i)
            # fout.write("{} {} {} {} {}\n".format(*'Pergunta correta opções reposta verificação'.split()))
            # [fout.write(f'{a} {b} {c}, {d}, {e}\n') for a, b, c, d, e in dframe.values]
            [fout.write(f'-----\nPergunta: {a}\nOpções: {c},\nSua resposta: {d},\nCorreta:{b}\n')
             for a, b, c, d, e in dframe.values]


def run():
    perguntas = []
    for i in [f"pergunta{i:0>2}" for i in range(41)]:
        perguntas.append(eval(i)())
    quiz(perguntas)


def run1(f: str = 'pergunta'):
    for f in [f"{f}{i:0>2}" for i in range(41)]:
        try:
            print('===')
            print(f)
            if eval(f).__doc__:
                if 'nonexequi' in eval(f).__doc__.lower():
                    raise NameError('Flag Nonexequi')
                print(eval(f).__doc__)
                print('---')
        except NameError:
            print(sys.exc_info())
        else:
            eval(f)()
            print('---')
        finally:
            print()


def run0():
    # print(dict_uf())
    p = df[~df.capital.str.contains('Brasília')].uf
    o = df[~df.capital.str.contains(p[0])].uf
    print(type(o))
    print(list(o))
    random.shuffle(o)
    r = list_options_combination(o, p)
    print(type(r))
    print(r)
    print(list_options_combination(o, p))


if __name__ == '__main__':
    run()
