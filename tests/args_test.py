import unittest

from incolumepy.testes import epoch


class UtilsTest(unittest.TestCase):
    def test_var_args(self):
        pass

    def test_var_fargs(self):
        pass

    def test_var_kwargs(self):
        pass

    def test_var_args_call(self):
        pass

    def test_epoch(self):
        self.assert_(epoch.epoch())


if __name__ == '__main__':
    unittest.main()
